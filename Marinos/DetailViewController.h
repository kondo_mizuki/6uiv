//
//  DetailViewController.h
//  TimeCapsule
//
//  Created by kyo on 2013/02/20.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

typedef NS_ENUM(NSInteger, ViewControllerHereFrom)
{
    FromCheckInViewController = 0,
    FromCameraViewController,
    FromAlbumViewController
};

@interface DetailViewController : UIViewController
<UIWebViewDelegate, MFMailComposeViewControllerDelegate>

@property(nonatomic, strong) UIWebView *webView;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, assign) ViewControllerHereFrom from;

- (void)setUpDetailPageParts;
- (void)setUpParts;

@end
