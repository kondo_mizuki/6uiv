//
//  ViewController.m
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "ViewController.h"


//#import "AlbumViewController.h"
//#import "BigbrotherCardListViewController.h"
//#import "SFIAdManager.h"
#import "CustomPointAnnotation.h"
#import "DatabaseUtility.h"
#import "LocationUtility.h"

@interface ViewController ()

@property(nonatomic, strong)UIImageView *cinemaAdImageView;
@property(nonatomic, strong)MKMapView *mapView;
@property(nonatomic, strong)UIButton* checkinBtn;
@property(nonatomic, assign)int anikicardID;
@property(nonatomic, strong)NSString* spotName;
@property(nonatomic, assign)double latitude;
@property(nonatomic, assign)double longtitude;

@end

@implementation ViewController

-(void)timer:(NSTimer*)timer{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if ([[ud objectForKey:@"staying"] boolValue]) {
        int staytime = [[ud objectForKey:@"staytime"] intValue];
        [ud setObject:[NSNumber numberWithInt:++staytime] forKey:@"staytime"];
        
        UILabel *stayTimeLabel = (UILabel*)[self.view viewWithTag:STAY_TIME_LABEL_TAG];
        stayTimeLabel.text = @"";
        
        if (staytime > 60*60*24) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d日", staytime/(60*60*24)];
            staytime = staytime%60*60*24;
        }
        if (staytime > 60*60) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d時", staytime/(60*60)];
            staytime = staytime%60*60;
        }
        if (staytime > 59) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d分", staytime/60];
            staytime = staytime%60;
        }
        stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d秒間", staytime];
    }
}

- (void)moveToCheckInViewController
{
    NSLog(@"%d", self.anikicardID);
    if (self.anikicardID != 0) {
//        [[DatabaseUtility sharedManager] updateForCheckin:self.anikicardID];
//        
//        CheckinViewController *viewController = [[CheckinViewController alloc] initWithNibName:nil bundle:nil];
//        
//        viewController.anikicardID = self.anikicardID;
//        viewController.spotName = self.spotName;
//        viewController.latitude = self.latitude;
//        viewController.longtitude = self.longtitude;
//        
//        [self presentViewController:viewController animated:YES completion:nil];
    }
}

- (void)changeCheckinBtnStatus:(NSNotification *)aNotification
{
    self.checkinBtn.hidden = [[aNotification.userInfo objectForKey:@"hidden"] boolValue];
    self.anikicardID = [[aNotification.userInfo objectForKey:@"anikicardID"] intValue];
    self.spotName = [aNotification.userInfo objectForKey:@"name"];
    self.latitude = [[aNotification.userInfo objectForKey:@"latitude"] doubleValue];
    self.longtitude = [[aNotification.userInfo objectForKey:@"longtitude"] doubleValue];
}

- (void)exchangeStayBtnState:(UIButton*)sender
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL staying = [[ud objectForKey:@"staying"] boolValue];
    
    UILabel *stayTimeLabel = (UILabel*)[self.view viewWithTag:STAY_TIME_LABEL_TAG];
    if (!staying) {
        stayTimeLabel.textColor = [UIColor redColor];
    } else {
        stayTimeLabel.textColor = [UIColor blackColor];
    }
    
    [ud setObject:[NSNumber numberWithBool:!staying] forKey:@"staying"];
}

-(void)setAnnotation:(CLLocationCoordinate2D) point mapMove:(BOOL)mapMove animated:(BOOL)animated category:(AnnotationCategory)category name:(NSString*)name
{
    CustomPointAnnotation *annotation = [[CustomPointAnnotation alloc] init];
    annotation.coordinate = point;
    annotation.title = name;

    [self.mapView addAnnotation:annotation];
}

- (void)showPinInMapView:(AnnotationCategory)category
{
    NSArray* array = [[DatabaseUtility sharedManager] getAllLocation];
    
    for (NSDictionary* location in array) {
        [self setAnnotation:CLLocationCoordinate2DMake([[location objectForKey:@"latitude"] doubleValue], [[location objectForKey:@"longtitude"] doubleValue]) mapMove:NO animated:NO category:category name:[location objectForKey:@"name"]];
    }
}

- (void)removeCategoryAllPin:(AnnotationCategory)category
{
    NSMutableArray *annotationsWillRemove = [self.mapView.annotations mutableCopy];
    [annotationsWillRemove removeObject:self.mapView.userLocation];
    
//    for (CustomPointAnnotation *annotation in annotationsWillRemove) {
//        if (annotation.category == category) {
//            [self.mapView removeAnnotation:annotation];
//        }
//    }
}

- (void)tabAction:(UIButton*)sender
{
    switch (sender.tag) {
        case SIGHTS_TAG:
        {
            if (sender.currentImage == [UIImage imageNamed:@"1_tab_off.png"]) {
                [sender setImage:[UIImage imageNamed:@"1_tab_on.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"1_tab_on.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(0, 0, 64, 58);
                [self showPinInMapView:AnnotationCategorySights];
            } else {
                [sender setImage:[UIImage imageNamed:@"1_tab_off.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"1_tab_off.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(0, 0, 64, 40);
                [self removeCategoryAllPin:AnnotationCategorySights];
            }
            break;
        }
        case FOOD_TAG:
        {
            if (sender.currentImage == [UIImage imageNamed:@"2_tab_off.png"]) {
                [sender setImage:[UIImage imageNamed:@"2_tab_on.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"2_tab_on.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64, 0, 64, 58);
                [self showPinInMapView:AnnotationCategoryFood];
            } else {
                [sender setImage:[UIImage imageNamed:@"2_tab_off.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"2_tab_off.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64, 0, 64, 40);
                [self removeCategoryAllPin:AnnotationCategoryFood];
            }
            break;
        }
        case SHOP_TAG:
        {
            if (sender.currentImage == [UIImage imageNamed:@"3_tab_off.png"]) {
                [sender setImage:[UIImage imageNamed:@"3_tab_on.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"3_tab_on.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64*2, 0, 64, 58);
                [self showPinInMapView:AnnotationCategoryShop];
            } else {
                [sender setImage:[UIImage imageNamed:@"3_tab_off.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"3_tab_off.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64*2, 0, 64, 40);
                [self removeCategoryAllPin:AnnotationCategoryShop];
            }
            break;
        }
            
        case HOTEL_TAG:
        {
            if (sender.currentImage == [UIImage imageNamed:@"4_tab_off.png"]) {
                [sender setImage:[UIImage imageNamed:@"4_tab_on.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"4_tab_on.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64*3, 0, 64, 58);
                [self showPinInMapView:AnnotationCategoryHotel];
            } else {
                [sender setImage:[UIImage imageNamed:@"4_tab_off.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"4_tab_off.png"] forState:UIControlStateHighlighted];
                sender.frame = CGRectMake(64*3, 0, 64, 40);
                [self removeCategoryAllPin:AnnotationCategoryHotel];
            }
            break;
        }
        case ACCOUNTING_TAG:
        {
            break;
        }
    }
}

- (void)showAlbumViewController
{
//    AlbumViewController *controller = [[AlbumViewController alloc] init];
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
//    navigationController.navigationBarHidden = YES;
//        
//    [self presentModalViewController:navigationController animated:YES];
}

- (void)showBigbrotherCardListViewController
{
//    BigbrotherCardListViewController *controller = [[BigbrotherCardListViewController alloc] init];
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
//    navigationController.navigationBarHidden = YES;
//    
//    [self presentModalViewController:navigationController animated:YES];
}

#pragma mark - MapView Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation) {
        return nil;
    }
    
    static NSString* Identifier = @"PinAnnotationIdentifier";
    MKPinAnnotationView* pinView;
    pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:Identifier];
    
    if (pinView == nil) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:Identifier];
        
    }
    
    pinView.annotation = annotation;
    pinView.canShowCallout = YES;
    
//    CustomPointAnnotation *anno = (CustomPointAnnotation*)annotation;
//    switch (anno.category) {
//        case AnnotationCategorySights:
//        {
//            pinView.image = [UIImage imageNamed:@"pin1.png"];
//            break;
//        }
//        case AnnotationCategoryFood:
//        {
//            pinView.image = [UIImage imageNamed:@"pin2.png"];
//            break;
//        }
//        case AnnotationCategoryShop:
//        {
//            pinView.image = [UIImage imageNamed:@"pin3.png"];
//            break;
//        }
//        case AnnotationCategoryHotel:
//        {
//            pinView.image = [UIImage imageNamed:@"pin4.png"];
//            break;
//        }
//        default:
//        {
//            break;
//        }
//    }
    
    return pinView;
}

- (void)mapView:(MKMapView*)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (view.annotation == mapView.userLocation) {
        return;
    }
    
    CustomPointAnnotation *annotation = (CustomPointAnnotation*)view.annotation;
    NSLog(@"%@", annotation.title);
    
    UIView *anikiInfoBG = [self.view viewWithTag:ANIKI_INFO_BACKGROUND_TAG];
    UILabel *anikiCommentLabel = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_LABEL_TAG];
    anikiCommentLabel.text = [NSString stringWithFormat:@"ここは%@やで", annotation.title];
    
    UIImageView *compass = (UIImageView*)[anikiInfoBG viewWithTag:COMPASS_TAG];
    if (!self.checkinBtn.hidden && self.latitude == annotation.coordinate.latitude && self.longtitude == annotation.coordinate.longitude) {
        compass.image = [UIImage imageNamed:@"compass_here.png"];
    } else {
        if (annotation.coordinate.latitude < [LocationUtility sharedManager].latitude && annotation.coordinate.longitude < [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_s_w"];
        } else if (annotation.coordinate.latitude < [LocationUtility sharedManager].latitude && annotation.coordinate.longitude > [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_s_e"];
        } else if (annotation.coordinate.latitude > [LocationUtility sharedManager].latitude && annotation.coordinate.longitude < [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_n_w"];
        } else if (annotation.coordinate.latitude > [LocationUtility sharedManager].latitude && annotation.coordinate.longitude > [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_n_e"];
        }
    }
    
    self.cinemaAdImageView.hidden = YES;
}

#pragma mark - set up parts
- (void)setUpBackGroundParts
{
    UIImageView *backGroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    backGroundImageView.image = [UIImage imageNamed:@"bg_1136.png"];
    [self.view addSubview:backGroundImageView];
}

- (void)setUpHeaderTabParts
{
    UIView *backgroundHeaderTabView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    backgroundHeaderTabView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backgroundHeaderTabView];

    UIButton *sightsTab = [UIButton buttonWithType:UIButtonTypeCustom];
    [sightsTab setImage:[UIImage imageNamed:@"1_tab_off.png"] forState:UIControlStateNormal];
    [sightsTab setImage:[UIImage imageNamed:@"1_tab_off.png"] forState:UIControlStateHighlighted];
    [sightsTab addTarget:self action:@selector(tabAction:) forControlEvents:UIControlEventTouchDown];
    sightsTab.frame = CGRectMake(0, 0, 64, 40);
    sightsTab.tag = SIGHTS_TAG;
    [self.view addSubview:sightsTab];

    UIButton *foodTab = [UIButton buttonWithType:UIButtonTypeCustom];
    [foodTab setImage:[UIImage imageNamed:@"2_tab_off.png"] forState:UIControlStateNormal];
    [foodTab setImage:[UIImage imageNamed:@"2_tab_off.png"] forState:UIControlStateHighlighted];
	[foodTab addTarget:self action:@selector(tabAction:) forControlEvents:UIControlEventTouchDown];
    foodTab.frame = CGRectMake(64, 0, 64, 40);
    foodTab.tag = FOOD_TAG;
    [self.view addSubview:foodTab];

    UIButton *hotelTab = [UIButton buttonWithType:UIButtonTypeCustom];
    [hotelTab setImage:[UIImage imageNamed:@"3_tab_off.png"] forState:UIControlStateNormal];
    [hotelTab setImage:[UIImage imageNamed:@"3_tab_off.png"] forState:UIControlStateHighlighted];
    [hotelTab addTarget:self action:@selector(tabAction:) forControlEvents:UIControlEventTouchDown];
    hotelTab.frame = CGRectMake(64*2, 0, 64, 40);
    hotelTab.tag = SHOP_TAG;
    [self.view addSubview:hotelTab];

    UIButton *shopTab = [UIButton buttonWithType:UIButtonTypeCustom];
    [shopTab setImage:[UIImage imageNamed:@"4_tab_off.png"] forState:UIControlStateNormal];
    [shopTab setImage:[UIImage imageNamed:@"4_tab_off.png"] forState:UIControlStateHighlighted];
    [shopTab addTarget:self action:@selector(tabAction:) forControlEvents:UIControlEventTouchDown];
    shopTab.frame = CGRectMake(64*3, 0, 64, 40);
    shopTab.tag = HOTEL_TAG;
    [self.view addSubview:shopTab];

    UIButton *accountingTab = [UIButton buttonWithType:UIButtonTypeCustom];
    [accountingTab setImage:[UIImage imageNamed:@"5_tab.png"] forState:UIControlStateNormal];
    [accountingTab addTarget:self action:@selector(tabAction:) forControlEvents:UIControlEventTouchDown];
    accountingTab.frame = CGRectMake(64*4, 0, 64, 40);
    accountingTab.tag = ACCOUNTING_TAG;
    [self.view addSubview:accountingTab];
}

- (void)setUpMapParts
{
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 40, 320, 320)];
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];

    MKCoordinateRegion zoom = self.mapView.region;
	zoom.center.latitude = 35.368;
    zoom.center.longitude = 136.637;
	zoom.span.latitudeDelta = 0.01;
	zoom.span.longitudeDelta = 0.01;
	[self.mapView setRegion:zoom animated:NO];
    
    [self.view addSubview:self.mapView];
}



- (void)setUpAnikiInfomationParts
{
    UIView *anikiInfoBackground = [[UIView alloc] init];
    anikiInfoBackground.backgroundColor = [UIColor clearColor];
    anikiInfoBackground.tag = ANIKI_INFO_BACKGROUND_TAG;
    if (SCREEN_BOUNDS.size.height == 568) {
        anikiInfoBackground.frame = CGRectMake(0, 40+320, 320, 88);
    } else {
        anikiInfoBackground.frame = CGRectMake(0, 40+320, 320, 50);
    }
    [self.view addSubview:anikiInfoBackground];
    
    UIImageView *anikiPhoto = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aniki_photo.png"]];
    anikiPhoto.frame = CGRectMake(10, 0, 44, 44);
    anikiPhoto.center = CGPointMake(anikiPhoto.center.x, anikiInfoBackground.frame.size.height/2);
    [anikiInfoBackground addSubview:anikiPhoto];
    
    UIImageView *anikiComment = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comment.png"]];
    anikiComment.frame = CGRectMake(10+44, 0, 204, 45);
    anikiComment.center = CGPointMake(anikiComment.center.x, anikiInfoBackground.frame.size.height/2);
    [anikiInfoBackground addSubview:anikiComment];
    
    UILabel *anikiCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10+44+20, 20, 180, 25)];
    anikiCommentLabel.textAlignment = NSTextAlignmentCenter;
    anikiCommentLabel.backgroundColor = [UIColor clearColor];
    anikiCommentLabel.textColor = [UIColor whiteColor];
    anikiCommentLabel.adjustsFontSizeToFitWidth = YES;
    anikiCommentLabel.tag = ANIKI_COMMENT_LABEL_TAG;
    [anikiInfoBackground addSubview:anikiCommentLabel];
    
    UIImageView *compass = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"compass.png"]];
    compass.frame = CGRectMake(10+44+204+5, 0, 48, 47);
    compass.center = CGPointMake(compass.center.x, anikiInfoBackground.frame.size.height/2);
    compass.tag = COMPASS_TAG;
    [anikiInfoBackground addSubview:compass];
    
    self.cinemaAdImageView = [[UIImageView alloc] init];
    if (SCREEN_BOUNDS.size.height == 568) {
        self.cinemaAdImageView.frame = CGRectMake(0, 40+320, 320, 88);
        self.cinemaAdImageView.image = [UIImage imageNamed:@"movie1136.png"];
    } else {
        self.cinemaAdImageView.frame = CGRectMake(0, 40+320, 320, 50);
        self.cinemaAdImageView.image = [UIImage imageNamed:@"movie960.png"];
    }
    
    [self.view addSubview:self.cinemaAdImageView];
}



- (void)setUpButtonBarParts
{
    UIButton *albumBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [albumBtn setImage:[UIImage imageNamed:@"album_btn.png"] forState:UIControlStateNormal];
    [albumBtn setImage:[UIImage imageNamed:@"album_on_btn.png"] forState:UIControlStateHighlighted];
    albumBtn.frame = CGRectMake(0, SCREEN_BOUNDS.size.height-50-20, 105, 50);
    [albumBtn addTarget:self action:@selector(showAlbumViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:albumBtn];
    
    UILabel *albumCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_BOUNDS.size.height-50-20, 105, 40)];
    albumCountLabel.backgroundColor = [UIColor clearColor];
    albumCountLabel.textAlignment = NSTextAlignmentCenter;
    albumCountLabel.font = [UIFont systemFontOfSize:20];
    albumCountLabel.tag = ALBUM_COUNT_LABEL_TAG;
    [self.view addSubview:albumCountLabel];
    
    UIButton *stayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [stayBtn setImage:[UIImage imageNamed:@"stay_btn_normal.png"] forState:UIControlStateNormal];
    stayBtn.frame = CGRectMake(105, SCREEN_BOUNDS.size.height-50-20, 110, 50);
    [stayBtn addTarget:self action:@selector(exchangeStayBtnState:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:stayBtn];
    
    UILabel *stayTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, SCREEN_BOUNDS.size.height-50-20, 110, 40)];
    stayTimeLabel.backgroundColor = [UIColor clearColor];
    stayTimeLabel.textAlignment = NSTextAlignmentCenter;
    stayTimeLabel.font = [UIFont systemFontOfSize:20];
    stayTimeLabel.adjustsFontSizeToFitWidth = YES;
    stayTimeLabel.tag = STAY_TIME_LABEL_TAG;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    int staytime = [[ud objectForKey:@"staytime"] intValue];
    [ud setObject:[NSNumber numberWithInt:++staytime] forKey:@"staytime"];
    
    stayTimeLabel.text = @"";
    
    if (staytime > 60*60*24) {
        stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d日", staytime/(60*60*24)];
        staytime = staytime%60*60*24;
    }
    if (staytime > 60*60) {
        stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d時", staytime/(60*60)];
        staytime = staytime%60*60;
    }
    if (staytime > 59) {
        stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d分", staytime/60];
        staytime = staytime%60;
    }
    stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d秒間", staytime];
    
    if ([[ud objectForKey:@"staying"] boolValue]) {
        stayTimeLabel.textColor = [UIColor redColor];
    } else {
        stayTimeLabel.textColor = [UIColor blackColor];
    }
    [self.view addSubview:stayTimeLabel];
    
    self.checkinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkinBtn setImage:[UIImage imageNamed:@"check_in_btn_1.png"] forState:UIControlStateNormal];
    self.checkinBtn.frame = CGRectMake(105, SCREEN_BOUNDS.size.height-50-20, 110, 50);
    [self.checkinBtn addTarget:self action:@selector(moveToCheckInViewController) forControlEvents:UIControlEventTouchUpInside];
    self.checkinBtn.hidden = YES;
    [self.view addSubview:self.checkinBtn];
    
    UIButton *cardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cardBtn setImage:[UIImage imageNamed:@"aniki_btn.png"] forState:UIControlStateNormal];
    [cardBtn setImage:[UIImage imageNamed:@"aniki_on_btn.png"] forState:UIControlStateHighlighted];
    cardBtn.frame = CGRectMake(105+110, SCREEN_BOUNDS.size.height-50-20, 105, 50);
    [cardBtn addTarget:self action:@selector(showBigbrotherCardListViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cardBtn];
    
    UILabel *gettedAnikicardCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(105+110, SCREEN_BOUNDS.size.height-50-20, 105, 40)];
    gettedAnikicardCountLabel.backgroundColor = [UIColor clearColor];
    gettedAnikicardCountLabel.textAlignment = NSTextAlignmentCenter;
    gettedAnikicardCountLabel.font = [UIFont systemFontOfSize:20];
    gettedAnikicardCountLabel.tag = GETTED_ANIKICARD_COUNT_LABEL_TAG;
    [self.view addSubview:gettedAnikicardCountLabel];
}



- (void)setUpParts
{
    self.view.backgroundColor = [UIColor grayColor];
    [self setUpBackGroundParts];
    [self setUpMapParts];
    [self setUpHeaderTabParts];
    [self setUpAnikiInfomationParts];
    [self setUpButtonBarParts];
}

- (void)setUpTimer {
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timer:) userInfo:nil repeats:YES];
}

- (void)setUpNotification {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(changeCheckinBtnStatus:) name:@"checkinBtnStatus" object:nil];
}

- (void)removeNotification {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:@"checkinBtnStatus" object:nil];
}

#pragma mark - view life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UILabel *albumCountLabel = (UILabel*)[self.view viewWithTag:ALBUM_COUNT_LABEL_TAG];
    albumCountLabel.text = [NSString stringWithFormat:@"%d", [[DatabaseUtility sharedManager] getAlbumCount]];
    
    UILabel *gettedAnikicardCountLabel = (UILabel*)[self.view viewWithTag:GETTED_ANIKICARD_COUNT_LABEL_TAG];
    gettedAnikicardCountLabel.text = [NSString stringWithFormat:@"%d", [[DatabaseUtility sharedManager] getGettedCardCount]];
    
    self.cinemaAdImageView.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setUpNotification];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpParts];
    [self setUpTimer];
    self.anikicardID = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
