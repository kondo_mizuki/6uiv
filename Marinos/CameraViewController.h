//
//  CameraViewController.h
//  TimeCapsule
//
//  Created by kyo on 2013/02/19.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DetailViewController.h"
#import <AWSRuntime/AWSRuntime.h>
#import <AWSS3/AmazonS3Client.h>
#import "ImobileSdkAds/ImobileSdkAds.h"

typedef NS_ENUM(NSInteger, CallingS3APIStatus)
{
    willCall = 0,
    calling,
    called,
    callFailure
};

@protocol CameraViewControllerDelegate <NSObject>
-(void) dismissViewController:(NSString*)url;
@end

@interface CameraViewController : DetailViewController
<AmazonServiceRequestDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate>

@property(nonatomic, retain) AmazonS3Client *s3;
@property(nonatomic, strong) NSMutableData *receivedData;
@property(nonatomic, assign) NSStringEncoding receivedDataEncoding;
@property(nonatomic, strong) UIDatePicker *datePicker;
@property(nonatomic, strong) UITextField *dateTextField;
@property(nonatomic, strong) UIImage *capturedImage;
@property(nonatomic, strong) NSString *postLatitudeStr;
@property(nonatomic, strong) NSString *postLongtitudeStr;

- (BOOL)sendPhotoToServer;
- (void)makeCameraViewParts;
//- (void)setUpCameraPageParts;
//- (void)setUpStillCamera;
- (void)setUpPostPageParts;

- (void) showCamera;
@end
