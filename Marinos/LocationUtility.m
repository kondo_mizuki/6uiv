//
//  LocationUtility.m
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "LocationUtility.h"
#import "DatabaseUtility.h"

@interface LocationUtility()

@property(nonatomic, strong)CLLocationManager *locationManager;

@end

@implementation LocationUtility

static LocationUtility* sharedUtil = nil;

#pragma mark - singleton method
+ (LocationUtility*)sharedManager {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone {
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

#pragma mark - 位置情報の取得
- (void)getLocation {
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    
    // 位置情報サービスが利用できるかどうかをチェック
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager.delegate = self;
        // 測位開始
        [self.locationManager startUpdatingLocation];
    } else {
        NSLog(@"Location services not available.");
    }
}

-(void) onPauseLocation {
    if (nil != self.locationManager && [CLLocationManager locationServicesEnabled])
        [self.locationManager stopUpdatingLocation]; //測位停止
}

// 位置情報更新時
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.isLocationEnabled = YES;
    
    CLLocation* location = [locations lastObject];
        
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        self.latitude = [location coordinate].latitude;
        self.longtitude = [location coordinate].longitude;
        
        BOOL isAllLocationNotCheckin = YES;
        NSArray *array = [[DatabaseUtility sharedManager] getAllLocation];
        for (NSDictionary *location in array) {
            
            double x = self.latitude - [[location objectForKey:@"latitude"] doubleValue];
            double y = self.longtitude - [[location objectForKey:@"longitude"] doubleValue];
            double z = sqrt(x * x + y * y);
                        
            if (z < 0.003) {
                isAllLocationNotCheckin = NO;
                NSDate *now = [NSDate date];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *lastNotification = [formatter dateFromString:[location objectForKey:@"last_notification"]];
                
                NSComparisonResult result = [[lastNotification initWithTimeInterval:24*60*60 sinceDate:lastNotification] compare:now];
                
                if (result == NSOrderedAscending) {
                    UILocalNotification *notification = [[UILocalNotification alloc] init];
                    
                    notification.alertBody = [NSString stringWithFormat:@"%@が近くにあります", [location objectForKey:@"name"]];
                    notification.alertAction = @"Open";
                    notification.soundName = UILocalNotificationDefaultSoundName;
                    
                    // 通知を登録する
                    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    [[DatabaseUtility sharedManager] updateLastNotification:[formatter stringFromDate:now] withID:[[location objectForKey:@"identifier"] intValue]];
                }
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                                    [NSNumber numberWithBool:NO], @"hidden",
                                                    [location objectForKey:@"id"], @"id",
                                                    [location objectForKey:@"place"], @"place",
                                                    [location objectForKey:@"latitude"], @"latitude",
                                                    [location objectForKey:@"longitude"], @"longitude", nil];
                NSNotification *n = [NSNotification notificationWithName:@"checkinBtnStatus" object:self userInfo:dic];
                [[NSNotificationCenter defaultCenter] postNotification:n];
            }
        }
        
        if (isAllLocationNotCheckin) {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], @"hidden",
                                 [NSNumber numberWithInt:0], @"id",
                                 @"", @"place", nil];
            
            NSNotification *n = [NSNotification notificationWithName:@"checkinBtnStatus" object:self userInfo:dic];
            [[NSNotificationCenter defaultCenter] postNotification:n];
        }
    }
}

// CLLocationManager オブジェクトにデリゲートオブジェクトを設定すると初回に呼ばれる
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined) {
        // ユーザが位置情報の使用を許可していない
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            // 位置情報測位の許可を求めるメッセージを表示する
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
        }
    }
}

// 位置情報の取得に失敗した場合にコールされる。
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	if (error) {
		NSString* message = nil;
		switch ([error code]) {
                // アプリでの位置情報サービスが許可されていない場合
			case kCLErrorDenied:
				// 位置情報取得停止
                self.isLocationEnabled = NO;
				[self.locationManager stopUpdatingLocation];
                message = [NSString stringWithFormat:NSLocalizedString(@"notAllowToGetGPS", @"アラート：位置情報取得不許可_メッセージ")];
				break;
			default:
                // 位置情報の取得に失敗しても何も表示しない
                //				message = [NSString stringWithFormat:@"位置情報の取得に失敗しました。"];
                self.isLocationEnabled = NO;
				break;
		}
		if (message) {
			// アラートを表示
			UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
		}
	}
}

@end
