//
//  rocksWebVC.h
//  6uiv
//
//  Created by macbook012 on 2015/03/25.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImobileSdkAds/ImobileSdkAds.h"

@interface rocksWebVC : UIViewController//<UIScrollViewDelegate>

@property (strong, nonatomic)  NSString *urlStr;

@property (weak, nonatomic) IBOutlet UIWebView *WV;

@end
