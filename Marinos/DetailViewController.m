//
//  DetailViewController.m
//  TimeCapsule
//
//  Created by kyo on 2013/02/20.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "DetailViewController.h"
#import "Define.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "Util.h"



@interface DetailViewController ()

@end

@implementation DetailViewController

- (void) dismiss
{
    
    [self.navigationController popViewControllerAnimated:YES];
    /*
     switch (self.from) {
     case FromCheckInViewController:
     {
     [self dismissViewControllerAnimated:YES completion:nil];
     break;
     }
     case FromAlbumViewController:
     {
     [self.navigationController popViewControllerAnimated:YES];
     break;
     }
     case FromCameraViewController:
     {
     [self dismissViewControllerAnimated:YES completion:nil];
     break;
     }
     default:
     break;
     }
     */
}

#pragma mark - ソーシャル連携機能
//- (void)SLComposeViewControllerButtonPressed:(id)sender
//{
//    NSString *iosDevice = [[UIDevice currentDevice] systemVersion];
//    if ([iosDevice intValue] >= 6.0 ){
//        NSString* serviceType;
//        if ([(UIButton*)sender tag] == TWITTER_BTN_TAG) serviceType = SLServiceTypeTwitter;   // Twitter
//        if ([(UIButton*)sender tag] == FACEBOOK_BTN_TAG) serviceType = SLServiceTypeFacebook;  // Facebook
//        
//        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:serviceType];
//        
//        // URLを追加(自分のアプリのストアURLに変更)
//        NSString *htmlTitle = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
//        NSString *userTitle = [htmlTitle substringToIndex:[htmlTitle rangeOfString:@"-timecapsule"].location];//@"-timecapsule("から(を抜いてみた。落ちるから。
//        if (serviceType == SLServiceTypeTwitter)
//        {
//            [composeViewController setInitialText:[NSString stringWithFormat:@"%@ #okinawaorai #timecapsule", userTitle]];
//        }
//        else
//        {
//            [composeViewController setInitialText:[NSString stringWithFormat:@"%@", userTitle]];
//        }
//        [composeViewController addURL:self.url];
//        [self presentViewController:composeViewController animated:NO completion:nil];
//    }
//}

//- (void) sendEmail:(id)sender {
//    Class mail = (NSClassFromString(@"MFMailComposeViewController"));
//    if (mail != nil){
//        //メールの設定がされているかどうかチェック
//        if ([mail canSendMail]){
//            [self showComposerSheet];
//        } else {
//            [self setAlert:NSLocalizedString(@"cannotStartMailTitle", @"アラート：メールが起動できない_タイトル") body:NSLocalizedString(@"cannotStartMailBody", @"アラート：メールが起動できない_本文")];
//        }
//    }
//}

-(void) showComposerSheet {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:[NSString stringWithFormat:@"%@", [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]]];
    
    //メールの本文を設定
    NSString *emailBody = [NSString stringWithFormat:@"%@", [self.webView stringByEvaluatingJavaScriptFromString:@"document.URL"]];
    [picker setMessageBody:emailBody isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:nil];
}

//- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
//    switch (result){
//        case MFMailComposeResultCancelled:
//            //キャンセルした場合
//            break;
//        case MFMailComposeResultSaved:
//            //保存した場合
//            break;
//        case MFMailComposeResultSent:
//            //送信した場合
//            break;
//        case MFMailComposeResultFailed:
//            [self setAlert:NSLocalizedString(@"mailFailureTitle", @"アラート：メール送信失敗_タイトル") body:NSLocalizedString(@"mailFailureBody", @"アラート：メール送信失敗_本文")];
//            break;
//        default:
//            break;
//    }
//    [self dismissViewControllerAnimated:YES completion:nil];
//}


#pragma mark アラート表示
-(void) setAlert:(NSString *)aTitle body:(NSString *)aDescription {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:aTitle message:aDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}



#pragma mark - webViewのdelegateメソッド
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    //キャッシュを全て消去
    //動作検証がしきれない。
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        self.url = [request.URL absoluteURL];
        NSLog(@"webViewのdelegateメソッド : %@", [request.URL path]);
        NSLog(@"webViewのdelegateメソッド : %@", [request.URL absoluteString]);
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView*)webView {
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - setUpParts
- (void)setUpParts {
    [self setUpDetailPageParts];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)setUpDetailPageParts {
    
    // WebViewで詳細閲覧画面を表示
    [self setUpDetailBodyParts];
    [self setUpDetailHeaderParts];
    
    // WebViewでURLの読み込み
//    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"iOSWebView", @"UserAgent", nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    
    // footerにタブバー的なものを表示
   // [self setUpDetailFooterParts];
}

- (void)setUpDetailHeaderParts
{
    //float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
//    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"normal_nav_bar.png"]];
//    headerImageView.frame = CGRectMake(0, 0, 320, 47);
//    [self.view addSubview:headerImageView];
    
        UIImageView *headerTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"normal_nav_bar.png"]];
        headerTitleImageView.frame = CGRectMake(0,0, 320,50);
        [self.view addSubview:headerTitleImageView];
    
    UIButton *closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closebtn.frame = CGRectMake(7, 0, 23, 23);
//    [closebtn setImage:[UIImage imageNamed:@"title-arrow.png"] forState:UIControlStateNormal];
//    [closebtn setImage:[UIImage imageNamed:@"title-arrow.png"] forState:UIControlStateHighlighted];
    
    [closebtn setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateHighlighted];

    
    //ここの処理は考えないとやばそう。特に条件がまずい。
    //    if (self.navigationController) {
    //        [closebtn addTarget:self action:@selector(backToHistoryView) forControlEvents:UIControlEventTouchUpInside];
    //    } else {
    [closebtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    //    }
    
    [self.view addSubview:closebtn];
    //[headerTitleImageView addSubview:closebtn];
}


- (void)backToHistoryView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpDetailBodyParts {
    // float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,44,320, SCREEN_BOUNDS.size.height-44-49)];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}


#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpParts];
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:YES];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
//    self.navigationController.navigationBar.barTintColor = NavBarColor;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//    
//    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
//    title.font = [UIFont boldSystemFontOfSize:16.0];
//    title.textColor = [UIColor whiteColor];
//    title.text = @"タイムカプセル";
//    [title sizeToFit];
//    self.navigationItem.titleView = title;
//    
//    if([[self.navigationController viewControllers] count] > 1){
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
//        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefaultPrompt];
//    }
//}
//
//-(void)backAction{
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
