//
//  gameResultWebVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/19.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "gameResultWebVC.h"

@interface gameResultWebVC ()

@end

@implementation gameResultWebVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:@"http://big6.gr.jp/system/prog/index_appli.php?type=2"];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.wevView loadRequest:req];
    
    [self showImobile];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];

    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"六大学野球ゲーム速報";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];

    }
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"];
    [ImobileSdkAds startBySpotID:@"379689"];
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)];
}

-(void)backAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
