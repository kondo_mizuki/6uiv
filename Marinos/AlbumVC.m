//
//  AlbumVC.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "AlbumVC.h"
#import "AlbumCell.h"

#import "Util.h"
#import "DatabaseUtility.h"
#import "TabButton.h"

typedef NS_ENUM(NSInteger, AlbumNameTag) {
    AlbumName2015       = 0,
    AlbumName2016
};

@interface AlbumVC (){
    NSString *albumName;
    
    UIButton *allCardBtn;
    UIButton *hoseiCardBtn;
    UIButton *tokyoCardBtn;
    UIButton *rikkyoCardBtn;
    UIButton *wasedaCardBtn;
    UIButton *keioCardBtn;
    UIButton *meijiCardBtn;
}

@end

@implementation AlbumVC{
    int btnNum;//検索切り替え
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self allDataBase];
    [self setUpScreen];
    [self createCollectionView];
    self.view.tag = TAB_ALBUM;
    //タブボタン生成
    [self tabButtonsInit];
    [self showImobile];
    
    btnNum=0;
    self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdArray];
    
    [TrackingManager sendScreenTracking:@"アルバム画面"];
    
}

-(void)allDataBase{
    
    _possessionArray = [NSMutableArray array];
    for (int i=0; i < [[DatabaseUtility sharedManager] getCardCount]; i++)
    {
        [_possessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:i+1]]];
    
    }
    
    self.meijiPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"meiji"])
    {
        [self.meijiPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
    
    self.wasedaPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"waseda"])
    {
        [self.wasedaPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
    
    self.rikkyoPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"rikkyo"])
    {
        [self.rikkyoPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
    
    self.keioPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"keio"])
    {
        [self.keioPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
    
    self.hoseiPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"hosei"])
    {
        [self.hoseiPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
    
    self.tokyoPossessionArray = [NSMutableArray array];
    for (NSNumber *i in [[DatabaseUtility sharedManager] getCardIdEachUniv:@"tokyo"])
    {
        [self.tokyoPossessionArray addObject:[NSNumber numberWithInt:[[DatabaseUtility sharedManager] getGettedCardPossession:[i intValue]]]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"アルバム";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    [self.navigationItem setHidesBackButton:YES animated:NO];

    
}


-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-TABBUTTON_HEIGHT-50)]; //広告を表示します
}

#pragma mark - タブボタン生成
-(void)tabButtonsInit
{
    TabButton *tb = [[TabButton alloc]initTabButtons:(int)self.view.tag];
    [tb setDelegate:self];
    tb.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-20);
    [self.view addSubview:tb];
}



#pragma mark - タブボタンどれか押した
- (void)anyButtonPushed:(id)sender
{
    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
    if (vc !=nil)
    {//現在の画面とタブが違う場合のみ遷移
        self.navigationController.navigationBarHidden = YES;
        if ([sender tag] == TAB_TOP)
        {//トップ画面のみポップで遷移
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else
        {//それ以外はプッシュで遷移
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}


-(void)setUpScreen
{
    UIImageView *background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background.png"]];
    if (WIN_SIZE.height==568)
    {
        background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
        background.image = [UIImage imageNamed:@"background.png"];
    }
    background.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    [self.view addSubview:background];
    
    [self albumSwitchBtn];
}

-(void)albumSwitchBtn{
    
    UIButton *yearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [yearBtn setBackgroundImage:[UIImage imageNamed:@"year.png"] forState:UIControlStateNormal];
    [yearBtn setBackgroundImage:[UIImage imageNamed:@"year.png"] forState:UIControlStateHighlighted];

    yearBtn.frame = CGRectMake(0,0,320,40);
    yearBtn.center = CGPointMake(WIN_SIZE.width/2,20+46);//+20statusbar
//    [yearBtn setTitle:@"2015年度" forState:UIControlStateNormal];
//    [yearBtn setTitle:@"2015年度" forState:UIControlStateHighlighted];
//    [yearBtn setBackgroundColor:[UIColor grayColor]];
    //[yearBtn addTarget:self action:@selector(showActionSheet:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:yearBtn];
    
    allCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    allCardBtn.frame = CGRectMake(0,0,46,46);
    allCardBtn.center = CGPointMake(23,60+46);//+20statusbar
    [allCardBtn setImage:[UIImage imageNamed:@"coll_bbl_off.png"] forState:UIControlStateNormal];
    [allCardBtn setImage:[UIImage imageNamed:@"coll_bbl.png"] forState:UIControlStateHighlighted];
    [allCardBtn setImage:[UIImage imageNamed:@"coll_bbl.png"] forState:UIControlStateSelected];
    allCardBtn.selected=YES;
    [allCardBtn addTarget:self action:@selector(allAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:allCardBtn];
    
    hoseiCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    hoseiCardBtn.frame = CGRectMake(0, 0,46, 46);
    hoseiCardBtn.center = CGPointMake(23+46,60+46);//+20statusbar
    [hoseiCardBtn setImage:[UIImage imageNamed:@"coll_hosei_off.png"] forState:UIControlStateNormal];
    [hoseiCardBtn setImage:[UIImage imageNamed:@"coll_hosei.png"] forState:UIControlStateHighlighted];
    [hoseiCardBtn setImage:[UIImage imageNamed:@"coll_hosei.png"] forState:UIControlStateSelected];
    [hoseiCardBtn addTarget:self action:@selector(hoseiAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:hoseiCardBtn];
    
    tokyoCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    tokyoCardBtn.frame = CGRectMake(0, 0,46, 46);
    tokyoCardBtn.center = CGPointMake(23+46*2,60+46);//+20statusbar
    [tokyoCardBtn setImage:[UIImage imageNamed:@"coll_tokyo_off.png"] forState:UIControlStateNormal];
    [tokyoCardBtn setImage:[UIImage imageNamed:@"coll_tokyo.png"] forState:UIControlStateHighlighted];
    [tokyoCardBtn setImage:[UIImage imageNamed:@"coll_tokyo.png"] forState:UIControlStateSelected];
    [tokyoCardBtn addTarget:self action:@selector(tokyoAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:tokyoCardBtn];
    
    rikkyoCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rikkyoCardBtn.frame = CGRectMake(0, 0,46, 46);
    rikkyoCardBtn.center = CGPointMake(23+46*3,60+46);//+20statusbar
    [rikkyoCardBtn setImage:[UIImage imageNamed:@"coll_rikyo_off.png"] forState:UIControlStateNormal];
    [rikkyoCardBtn setImage:[UIImage imageNamed:@"coll_rikyo.png"] forState:UIControlStateHighlighted];
    [rikkyoCardBtn setImage:[UIImage imageNamed:@"coll_rikyo.png"] forState:UIControlStateSelected];
    [rikkyoCardBtn addTarget:self action:@selector(rikkyoAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:rikkyoCardBtn];
    
    wasedaCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    wasedaCardBtn.frame = CGRectMake(0, 0,46, 46);
    wasedaCardBtn.center = CGPointMake(23+46*4,60+46);//+20statusbar
    [wasedaCardBtn setImage:[UIImage imageNamed:@"coll_waseda_off.png"] forState:UIControlStateNormal];
    [wasedaCardBtn setImage:[UIImage imageNamed:@"coll_waseda.png"] forState:UIControlStateHighlighted];
    [wasedaCardBtn setImage:[UIImage imageNamed:@"coll_waseda.png"] forState:UIControlStateSelected];
    [wasedaCardBtn addTarget:self action:@selector(wasedaAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:wasedaCardBtn];
    
    keioCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    keioCardBtn.frame = CGRectMake(0, 0,46, 46);
    keioCardBtn.center = CGPointMake(23+46*5,60+46);//+20statusbar
    [keioCardBtn setImage:[UIImage imageNamed:@"coll_keio_off.png"] forState:UIControlStateNormal];
    [keioCardBtn setImage:[UIImage imageNamed:@"coll_keio.png"] forState:UIControlStateHighlighted];
    [keioCardBtn setImage:[UIImage imageNamed:@"coll_keio.png"] forState:UIControlStateSelected];
    [keioCardBtn addTarget:self action:@selector(keioAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:keioCardBtn];

    meijiCardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    meijiCardBtn.frame = CGRectMake(0,0,46,46);
    meijiCardBtn.center = CGPointMake(23+46*6,60+46);//+20statusbar
    [meijiCardBtn setImage:[UIImage imageNamed:@"coll_meiji_off.png"] forState:UIControlStateNormal];
    [meijiCardBtn setImage:[UIImage imageNamed:@"coll_meiji.png"] forState:UIControlStateHighlighted];
    [meijiCardBtn setImage:[UIImage imageNamed:@"coll_meiji.png"] forState:UIControlStateSelected];
    [meijiCardBtn addTarget:self action:@selector(meijiAlbumPush:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:meijiCardBtn];
    
}

//- (void)showActionSheet:(id)sender{
//    // アクションシートの作成
//    UIActionSheet *actionSheet = [[UIActionSheet alloc]
//                                  initWithTitle:@"アルバム選択"
//                                  delegate:self
//                                  cancelButtonTitle:@"キャンセル"
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"2015シーズン", @"2016オフシーズン",nil];
//    
//    // アクションシートの表示
//    //[actionSheet showInView:self.view];
//    [actionSheet showInView:self.view.window];
//}


//#pragma mark - アクションシートのボタンが押されたとき
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    AlbumNameTag albumTag = buttonIndex;
//    switch (albumTag) {
//        case AlbumName2015:
//            NSLog(@"2013");
//            albumName = ALBUM_2015;
//            break;
//        case AlbumName2016:
//            NSLog(@"2014オフシーズン");
//            albumName = ALBUM_2016;
//            break;
//        default:
//            break;
//    }
//    
//    [self.collectionView reloadData];
//}


-(void)allAlbumPush:(id)sender{
    btnNum=0;
    [self.collectionView reloadData];
    [self allSelectNo];
    allCardBtn.selected=YES;
}
-(void)meijiAlbumPush:(id)sender{
    btnNum=1;
    [self.collectionView reloadData];
    [self allSelectNo];
    meijiCardBtn.selected=YES;
}
-(void)wasedaAlbumPush:(id)sender{
    btnNum=2;
    [self.collectionView reloadData];
    [self allSelectNo];
    wasedaCardBtn.selected=YES;
}
-(void)rikkyoAlbumPush:(id)sender{
    btnNum=3;
    [self.collectionView reloadData];
    [self allSelectNo];
    rikkyoCardBtn.selected=YES;
}
-(void)keioAlbumPush:(id)sender{
    btnNum=4;
    [self.collectionView reloadData];
    [self allSelectNo];
    keioCardBtn.selected=YES;
}
-(void)hoseiAlbumPush:(id)sender{
    btnNum=5;
    [self.collectionView reloadData];
    [self allSelectNo];
    hoseiCardBtn.selected=YES;
}
-(void)tokyoAlbumPush:(id)sender{
    btnNum=6;
    [self.collectionView reloadData];
    [self allSelectNo];
    tokyoCardBtn.selected=YES;
}

-(void)allSelectNo{
    allCardBtn.selected = NO;
    hoseiCardBtn.selected = NO;
    tokyoCardBtn.selected = NO;
    rikkyoCardBtn.selected = NO;
    wasedaCardBtn.selected = NO;
    keioCardBtn.selected = NO;
    meijiCardBtn.selected = NO;
}

#pragma mark - 
-(void)createCollectionView
{
    /*UICollectionViewのコンテンツの設定 UICollectionViewFlowLayout*/
    _flowLayout = [[UICollectionViewFlowLayout alloc]init];
    // セルのサイズ
    _flowLayout.itemSize = CGSizeMake(80,119);//(78,117)
    // セクションごとのヘッダーのサイズ
    _flowLayout.headerReferenceSize = CGSizeMake(0, 0);
    // セクションごとのフッターのサイズ
    _flowLayout.footerReferenceSize = CGSizeMake(0, 0);
    // 行ごとのスペースの最小値
    _flowLayout.minimumLineSpacing = 4.0;
    // アイテムごとのスペースの最小値
    _flowLayout.minimumInteritemSpacing = 0;//2.0
    // セクションの外枠のスペース
    //_flowLayout.sectionInset = UIEdgeInsetsMake(0,1,0,1);
    _flowLayout.sectionInset = UIEdgeInsetsMake(1,0,0,0);//(47,1,0,1)
    

    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,130, WIN_SIZE.width, WIN_SIZE.height-TABBUTTON_HEIGHT-170) collectionViewLayout:_flowLayout];

    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor clearColor];
    //collectionViewにcellのクラスを登録。セルの表示に使う
    [_collectionView registerClass:[AlbumCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    [self.view addSubview:_collectionView];
}



/*セクションの数*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}



/*セクションに応じたセルの数*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //カードDBのレコード数をカウントして返す
    int card_tb_count = [[DatabaseUtility sharedManager] getCardCount];
    
    
    switch (btnNum) {
        case 0://全部
            card_tb_count = [[DatabaseUtility sharedManager] getCardCount];
            break;
        case 1://明治
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"meiji"];
            break;
        case 2://早稲田
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"waseda"];
            break;
        case 3://立教
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"rikkyo"];
            break;
        case 4://慶應
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"keio"];
            break;
        case 5://法政
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"hosei"];
            break;
        case 6://東大
            card_tb_count = [[DatabaseUtility sharedManager] getCardCountEachUniv:@"tokyo"];
            break;
            
        default:
            break;
    }
    return card_tb_count;
}



//タップされた時
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _detailViewController = [[AlbumDetailViewController alloc] init];
    int cardID;
    cardID = [[self.cardIdArray objectAtIndex:indexPath.row]intValue];
    
    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:cardID];
    if (selectedCardDic != nil)//TODO:エラー処理
    {
        int possession = [_possessionArray[indexPath.row] intValue];
        
        switch (btnNum) {
            case 0://全部
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardId];
                possession = [_possessionArray[indexPath.row] intValue];
                break;
            case 1://明治
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"meiji"];
                possession = [self.meijiPossessionArray[indexPath.row] intValue];
                break;
            case 2://早稲田
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"waseda"];
                possession = [self.wasedaPossessionArray[indexPath.row] intValue];
                break;
            case 3://立教
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"rikkyo"];
                possession = [self.rikkyoPossessionArray[indexPath.row] intValue];
                break;
            case 4://慶應
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"keio"];
                possession = [self.keioPossessionArray[indexPath.row] intValue];
                break;
            case 5://法政
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"hosei"];
                possession = [self.hoseiPossessionArray[indexPath.row] intValue];
                break;
            case 6://東大
                self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"tokyo"];
                possession = [self.tokyoPossessionArray[indexPath.row] intValue];
                break;
                
            default:
                break;
        }
        
#warning 強制アルバム詳細遷移　本番は残す
        
        if (possession < 1)
        {
            return;
        }
        
        
    }
    _detailViewController.detailItem = indexPath;
    _detailViewController.cardID = cardID;
    [self.navigationController pushViewController:_detailViewController animated:NO];
    [[Util sharedManager] playSound:SOUND_ALBUM];
}


/*セルの内容を返す*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"UICollectionViewCell";
    AlbumCell *cell = (AlbumCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];

           // サブビューを取り除く
        for (UIView *subview in [cell.contentView subviews])
        {
            [subview removeFromSuperview];
        }
    int possession = [_possessionArray[indexPath.row] intValue];
    
    switch (btnNum) {
        case 0://全部
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardId];
            possession = [_possessionArray[indexPath.row] intValue];
        break;
        case 1://明治
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"meiji"];
            possession = [self.meijiPossessionArray[indexPath.row] intValue];
            break;
        case 2://早稲田
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"waseda"];
            possession = [self.wasedaPossessionArray[indexPath.row] intValue];
            break;
        case 3://立教
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"rikkyo"];
            possession = [self.rikkyoPossessionArray[indexPath.row] intValue];
            break;
        case 4://慶應
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"keio"];
            possession = [self.keioPossessionArray[indexPath.row] intValue];
            break;
        case 5://法政
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"hosei"];
            possession = [self.hoseiPossessionArray[indexPath.row] intValue];
            break;
        case 6://東大
            self.cardIdArray=[[DatabaseUtility sharedManager]getCardIdEachUniv:@"tokyo"];
            possession = [self.tokyoPossessionArray[indexPath.row] intValue];
            break;
            
        default:
            break;
    }
    int cardID = [[self.cardIdArray objectAtIndex:indexPath.row]intValue];
        [cell displayCard:cardID possession:possession];
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end