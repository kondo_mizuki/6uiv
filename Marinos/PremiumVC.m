//
//  PremiumVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "PremiumVC.h"

@interface PremiumVC ()

@end

@implementation PremiumVC{
    int thisMonth;
    int today;
    int beforeMonth;
    int beforeDay;
    
    BOOL hasEntered;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self dayChk];
    
    if(hasEntered){
        [self entryMethod];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
   
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:14.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"春・秋季リーグ戦応援席パスポート";
    [title sizeToFit];
    self.navigationItem.titleView = title;
}

-(void)dayChk
{
    //初期値設定
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@"99" forKey:@"TICKET_MONTH"];
    [dic setObject:@"99" forKey:@"TICKET_DAY"];
    [ud registerDefaults:dic];
    [ud synchronize];
    
    //保存された日付をロード
    beforeMonth = (int)[ud integerForKey:@"TICKET_MONTH"];
    beforeDay = (int)[ud integerForKey:@"TICKET_DAY"];
    NSLog(@"保存されていたチケット日付は%d月%d日",beforeMonth,beforeDay);
    
    //日付を取得
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit |
                                   NSMonthCalendarUnit  |
                                   NSDayCalendarUnit    |
                                   NSWeekdayCalendarUnit|
                                   NSHourCalendarUnit   |
                                   NSMinuteCalendarUnit |
                                   NSSecondCalendarUnit
                                              fromDate:date];
    
    //今日の日付を代入
    thisMonth = (int)dateComps.month;
    today = (int)dateComps.day;
    
    NSLog(@"今日は%d月%d日",thisMonth,today);
    
    //--------------------------
    if (today == beforeDay && thisMonth == beforeMonth)
    {//今日の月日と保存されている月日が一緒だったら
        hasEntered = YES;
        return;
    }
    hasEntered = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)entryMethod{

    self.premiumBG.image=[UIImage imageNamed:@"gatein.png"];
    self.cautionView.hidden=YES;
    //self.cautionLbl.hidden=YES;
    self.entryBtn.hidden=YES;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"入場券";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"ENTRY_FLAG"];
    
    beforeMonth = thisMonth;
    beforeDay=today;
    
    [ud setInteger:beforeMonth forKey:@"TICKET_MONTH"];
    [ud setInteger:beforeDay forKey:@"TICKET_DAY"];
    
    [ud synchronize];
}

- (IBAction)entryBtnPush:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"OKボタンをタップすると応援席フリーパスポート画面が表示されます。OKをタップすると翌日まで入場はできなくなりますのでご注意ください。" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"OK", nil];
        [alert show];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            //１番目のボタンが押されたときの処理を記述する
            return;
            break;
        case 1:
            //２番目のボタンが押されたときの処理を記述する
            [TrackingManager sendEventTracking:@"entry" action:@"entry" label:@"" value:0 screen:@"premiumTicket"];
            [self entryMethod];
            break;
    }
    
}
@end
