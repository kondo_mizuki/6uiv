//
//  CardVC.h
//  Marinos
//
//  Created by KEi on 13/08/06.
//  Copyright (c) 2013年 Keishi Kuwabara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Accounts/Accounts.h>
#import "ImobileSdkAds/ImobileSdkAds.h"


@interface CardVC : UIViewController

@property (nonatomic, assign) int myPoint;
@property (nonatomic, retain) UILabel *myPointLabel;

@property (nonatomic, strong) CAEmitterLayer *emitterLayer;//パーティクル

@end
