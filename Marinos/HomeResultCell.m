//
//  HomeResultCell.m
//  6uiv
//
//  Created by macbook012 on 2015/03/09.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "HomeResultCell.h"

@implementation HomeResultCell{
    
}
@synthesize delegate;


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // write something.
    self.resultWebView.delegate = self;
    self.resultWebView.scrollView.bounces=NO;
    
    NSURL *url = [NSURL URLWithString:@"http://big6.gr.jp/system/prog/appli_todaygame.php"];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.resultWebView loadRequest:req];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapMethod)];
    [tap setNumberOfTapsRequired:1]; // Set your own number here
    [tap setDelegate:self]; // Add the <UIGestureRecognizerDelegate> protocol
    [self.resultWebView addGestureRecognizer:tap];
    
}

-(void)refreshWV{
    //試合結果webviewの表示・非表示切り替え
    NSUserDefaults *ud = ud = [NSUserDefaults standardUserDefaults];
    BOOL b = [ud boolForKey:@"CHARGES_FLAG"];
    if (!b) {
        self.resultWebView.hidden=YES;
    }else{
        self.resultWebView.hidden=NO;
    }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)didTapMethod{
    if ([delegate respondsToSelector:@selector(gameResultPush)]) {
    [delegate gameResultPush];
    }
}

// ページ読込開始時にインジケータをくるくるさせる
-(void)webViewDidStartLoad:(UIWebView*)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

// ページ読込完了時にインジケータを非表示にする
-(void)webViewDidFinishLoad:(UIWebView*)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)noChargePush:(id)sender {
    if ([delegate respondsToSelector:@selector(showChargeAlert)]) {
        [delegate showChargeAlert];
    }
}
@end
