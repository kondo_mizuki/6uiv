//
//  checkInImageVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/27.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "checkInImageVC.h"

@interface checkInImageVC ()

@end

@implementation checkInImageVC{
    NSString *checkInPlaceName;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self checkInCheck];
    [self setUpParameters];
    [self setTrain];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"チェックイン";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    }
}

- (void)setUpParameters
{
    NSLog(@"あ%d",_place_id);
    NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:_place_id];
    _latitude = [pDic[@"latitude"] doubleValue];
    _longtitude = [pDic[@"longitude"] doubleValue];
}

- (void)setTrain
{
    UIImageView *middlePanel = [[UIImageView alloc] initWithFrame:CGRectMake(-10, 340, 340, 110)];
    middlePanel.image = [UIImage imageNamed:@"middle-panel.png"];
    middlePanel.userInteractionEnabled = YES;
    [self.view addSubview:middlePanel];
    
    self.trainWebView.center = CGPointMake(middlePanel.center.x, middlePanel.center.y+2);
    self.trainWebView.delegate = self;
    self.trainWebView.scrollView.bounces = NO;
    [self.trainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://timecapsule-eagle-staging.herokuapp.com/trainsearch?latitude=%@&longtitude=%@", [NSString stringWithFormat:@"%.3f", self.latitude], [NSString stringWithFormat:@"%.3f", self.longtitude]]]]];
    NSLog(@"lon:%f",self.longtitude);
    [self.view addSubview:self.trainWebView];
    
    UILabel *tcInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,300, 13)];
    tcInfoLabel.backgroundColor = [UIColor clearColor];
    tcInfoLabel.textColor = [UIColor blackColor];
    tcInfoLabel.font = [UIFont fontWithName:MY_FONT size:14];
    tcInfoLabel.textAlignment = NSTextAlignmentCenter;
    tcInfoLabel.text = @"〜このスポットの記念写真〜";
    tcInfoLabel.center = CGPointMake(middlePanel.center.x, middlePanel.center.y-47);
    [self.view addSubview:tcInfoLabel];
}


-(void)checkInCheck
{
    [[Util sharedManager] playSound:SOUND_PYORO];
    //_place_idをもとにチェックイン場所の情報を取得
    NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:_place_id];
    
    
    //ヘッダーとトップのチェックイン場所テキストにチェックイン場所を代入。
    checkInPlaceName = pDic[@"name"];
    self.checkInLbl.text = checkInPlaceName;
    self.checkInLbl.text = [NSString stringWithFormat:@"「%@」\nにチェックインしました！",checkInPlaceName];
    self.checkInLbl.adjustsFontSizeToFitWidth = YES;
    
    //保有ポイント数とか読み込み
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    int year = (int)[ud integerForKey:@"KEY_YEAR"];
    int month = (int)[ud integerForKey:@"KEY_MONTH"];
    int day = (int)[ud integerForKey:@"KEY_DAY"];
    NSString *dateStr = [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
    NSLog(@"currentDate:%@",dateStr);
    //チェックインずみかどうかをチェック。
    NSString *last_check_in_date = pDic[@"last_check_in_date"];
    NSLog(@"lastCheckInDate:%@",last_check_in_date);
    if (![dateStr isEqualToString:last_check_in_date])
    {
        //当日チェックインは初めて
        self.getLbl.text = @"10ptゲット！";
        [self performSelector:@selector(checkIn) withObject:nil afterDelay:0.7];
    }
    else
    {//当日チェックインずみ
        self.checkInLbl.text = @"チェックインボーナスは\n１日１回！";
        self.getLbl.text = @"チェックイン済み";
    }
}

-(void)checkIn
{
    [TrackingManager sendEventTracking:@"CheckIn" action:nil label:nil value:nil screen:@"チェックイン画面"];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    int myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
    int year = (int)[ud integerForKey:@"KEY_YEAR"];
    int month = (int)[ud integerForKey:@"KEY_MONTH"];
    int day = (int)[ud integerForKey:@"KEY_DAY"];
    
    NSString *dateStr = [NSString stringWithFormat:@"%d年%d月%d日", year, month, day];
    
    //_place_idをもとに場所を取ってくる
    NSMutableDictionary *dicLocation = [[DatabaseUtility sharedManager] getLocation:_place_id];
    NSString *placeName = dicLocation[@"place"];
    NSLog(@"%@",placeName);
    
    //ポイント保存＆再表示
    int checkInPoint =10;
    myPoint = myPoint+checkInPoint;
    
    [ud setInteger:myPoint forKey:@"KEY_MYPOINT"];
    [ud synchronize];
    
    dateStr = [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
    [[DatabaseUtility sharedManager] updateLastCheckIn:dateStr withID:_place_id];
    
}

#pragma mark - delegate - webView
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSLog(@"%@", [request.URL path]);
        NSLog(@"%@", [request.URL absoluteString]);
        
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.url = [NSURL URLWithString:[request.URL absoluteString]];
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        return NO;
    }
    return YES;
}


-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)photoBtnPush:(id)sender {
    [self moveToCameraViewController];
}

- (void)moveToCameraViewController
{
        if ([LocationUtility sharedManager].isLocationEnabled) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            CameraViewController *controller = [[CameraViewController alloc] init];
            [controller setFrom:FromCameraViewController];
            [self presentViewController:controller animated:YES completion:^{
                [controller showCamera];
            }];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"カメラの使用が許可されていません。設定を確認して下さい。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

@end
