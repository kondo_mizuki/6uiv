//
//  TopVC.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "schoolSongVC.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "gameResultWebVC.h"
#import "ticketVC.h"
#import "cardCouponSelectVC.h"
#import "HomeResultCell.h"
#import "TrackingManager.h"


@interface TopVC : UIViewController
<UIScrollViewDelegate,UIApplicationDelegate, UIWebViewDelegate,HomeResultCellDelegate>//前者いらないかも

//日付チェックメソッド
-(void)dayChk;
- (void)anyButtonPushed:(id)sender;

-(void)tutorialCloseButtonPushed:(id)sender;

@property (nonatomic,retain) UIWebView *topWebView;
//@property (weak, nonatomic) IBOutlet UIWebView *resultWebView;

- (IBAction)meijiPush:(id)sender;
- (IBAction)wasedaPush:(id)sender;
- (IBAction)rikkyoPush:(id)sender;
- (IBAction)keioPush:(id)sender;
- (IBAction)hoseiPush:(id)sender;
- (IBAction)tokyoPush:(id)sender;

//- (IBAction)gameResultBtnPush:(id)sender;
- (IBAction)ticketBtnPush:(id)sender;

- (IBAction)meijiBlogPush:(id)sender;
- (IBAction)wasedaBlogPush:(id)sender;
- (IBAction)rikkyoBlogPush:(id)sender;
- (IBAction)keioBlogPush:(id)sender;
- (IBAction)hoseiBlogPush:(id)sender;
- (IBAction)tokyoBlogPush:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//@property (weak, nonatomic) IBOutlet UIButton *meijiBlogBtn;
//@property (weak, nonatomic) IBOutlet UIButton *wasedaBlogBtn;
//@property (weak, nonatomic) IBOutlet UIButton *rikkyoBlogBtn;
//@property (weak, nonatomic) IBOutlet UIButton *keioBlogBtn;
//@property (weak, nonatomic) IBOutlet UIButton *hoseiBlogBtn;
//@property (weak, nonatomic) IBOutlet UIButton *tokyoBlogBtn;
//
//@property (weak, nonatomic) IBOutlet HomeResultCell *secondCell;
@end
