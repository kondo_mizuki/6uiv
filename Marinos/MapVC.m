//
//  MapVC.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "MapVC.h"

#import "Util.h"
#import "TabButton.h"

#import "CustomPointAnnotation.h"
#import "DatabaseUtility.h"
#import "LocationUtility.h"

@interface MapVC ()
//@property(nonatomic, strong)UIImageView *cinemaAdImageView;
@property(nonatomic, strong)MKMapView *mapView;
@property(nonatomic, strong)UIButton* checkinBtn;
@property(nonatomic, assign)int anikicardID;
@property(nonatomic, strong)NSString* spotName;
@property(nonatomic, assign)double latitude;
@property(nonatomic, assign)double longtitude;
@property(nonatomic, assign)float y568;

@end

@implementation MapVC{
    //UILabel *explanationLabel;
    UIView *anikiInfoBackground;
    UIScrollView *customScrollView;
    UITextView *exTv;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self firstTimeAlert];
        // Custom initialization
    }
    return self;
}


-(void)firstTimeAlert
{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults setObject:@"YES" forKey:@"FIRST_MAP_FLAG"];
    [ud registerDefaults:defaults];
    
    BOOL firstMap = [ud boolForKey:@"FIRST_MAP_FLAG"];
    if (firstMap == YES)
    {
        [ud setBool:NO forKey:@"FIRST_MAP_FLAG"];
        [ud synchronize];
        UIAlertView *alert2;
        alert2 = [[UIAlertView alloc] init];
        alert2.title = @"チェックボーナスについて";
        alert2.message = @"六大学野球に関連するスポットに行って\nチェックインボーナスをゲットしよう！";
        [alert2 addButtonWithTitle:@"OK"];
        if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
        {  //iOS7未満の場合
            ((UILabel *)[[alert2 subviews] objectAtIndex:1]).textAlignment = NSTextAlignmentLeft;
        }
        [alert2 show];
    }
}



-(void)timer:(NSTimer*)timer{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if ([[ud objectForKey:@"staying"] boolValue]) {
        int staytime = [[ud objectForKey:@"staytime"] intValue];
        [ud setObject:[NSNumber numberWithInt:++staytime] forKey:@"staytime"];
        
        UILabel *stayTimeLabel = (UILabel*)[self.view viewWithTag:STAY_TIME_LABEL_TAG];
        stayTimeLabel.text = @"";
        
        if (staytime > 60*60*24) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d日", staytime/(60*60*24)];
            staytime = staytime%(60*60*24);
        }
        if (staytime > 60*60) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d時", staytime/(60*60)];
            staytime = staytime%(60*60);
        }
        if (staytime > 59) {
            stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d分", staytime/60];
            staytime = staytime%60;
        }
        stayTimeLabel.text = [stayTimeLabel.text stringByAppendingFormat:@"%d秒間", staytime];
    }
}


- (void)moveToCheckInViewController
{
    NSLog(@"%d", self.anikicardID);
    if (self.anikicardID != 0) {
    }
}

- (void)changeCheckinBtnStatus:(NSNotification *)aNotification
{
    self.checkinBtn.hidden = [[aNotification.userInfo objectForKey:@"hidden"] boolValue];
    self.anikicardID = [[aNotification.userInfo objectForKey:@"anikicardID"] intValue];
    self.spotName = [aNotification.userInfo objectForKey:@"name"];
    self.latitude = [[aNotification.userInfo objectForKey:@"latitude"] doubleValue];
    self.longtitude = [[aNotification.userInfo objectForKey:@"longitude"] doubleValue];
}

- (void)exchangeStayBtnState:(UIButton*)sender
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL staying = [[ud objectForKey:@"staying"] boolValue];
    
    UILabel *stayTimeLabel = (UILabel*)[self.view viewWithTag:STAY_TIME_LABEL_TAG];
    if (!staying) {
        stayTimeLabel.textColor = [UIColor redColor];
    } else {
        stayTimeLabel.textColor = [UIColor blackColor];
    }
    
    [ud setObject:[NSNumber numberWithBool:!staying] forKey:@"staying"];
}

-(void)setAnnotation:(CLLocationCoordinate2D)co radius:(double)radius placeID:(int)placeID mapMove:(BOOL)mapMove animated:(BOOL)animated detail:(NSString*)detail name:(NSString*)name univName:(NSString*)univName
{
    CustomPointAnnotation *annotation = [[CustomPointAnnotation alloc] init];
    annotation.coordinate = co;
    annotation.radius = radius/(1000.0f * 50.0f);//10だと広すぎ、100を試したら５倍にしましょうということだったので、50に落ち着いた。
    annotation.placeID = placeID;
    annotation.detail = detail;
    annotation.title = name;
    annotation.univName=univName;
    
#pragma mark　長いタイトルをサブタイトルでごまかす
    if (name.length >= 14) {
        annotation.title = [name substringToIndex:13];
        annotation.subtitle=[name  substringFromIndex:13];
    }
    [self.mapView addAnnotation:annotation];
}

- (void)showPinInMapView
{
    NSArray* array = [[DatabaseUtility sharedManager] getAllLocation];
    
    for (NSDictionary* location in array) {
        [self setAnnotation:CLLocationCoordinate2DMake([[location objectForKey:@"latitude"] doubleValue], [[location objectForKey:@"longitude"] doubleValue]) radius:[[location objectForKey:@"radius"] doubleValue] placeID:[[location objectForKey:@"identifier"] intValue] mapMove:NO animated:NO detail:[location objectForKey:@"explanation"] name:[location objectForKey:@"name"] univName:[location objectForKey:@"univ_name"]];
    }
}

- (void)moveCheckIn:(id)sender; {
    
    [TrackingManager sendEventTracking:@"toCheckIn" action:@"tap" label:[NSString stringWithFormat:@"%@",(UIButton*)sender] value:0 screen:@"Map"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    checkInImageVC *ciVC = (checkInImageVC*)[storyboard instantiateViewControllerWithIdentifier:@"checkInImageVC"];
    UIButton *cargo = (UIButton*)sender;
    ciVC.place_id =(int)cargo.tag;
    [self.navigationController pushViewController:ciVC animated:YES];
}


//アノテーションビューが作られたときのデリゲート。addAnotationするときに呼ばれる
- (void)mapView:(MKMapView*)mapView didAddAnnotationViews:(NSArray*)views{
    
    // アノテーションビューを取得する
    for (MKAnnotationView* annotationView in views) {
        
        if ([annotationView.annotation isKindOfClass:[CustomPointAnnotation class]]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            CustomPointAnnotation *po = (CustomPointAnnotation*)annotationView.annotation;
            button.tag = po.placeID;
            [button addTarget:self action:(@selector(moveCheckIn:)) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}


#pragma mark - MapView Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation) {
        return nil;
    }
    
    static NSString* Identifier = @"PinAnnotationIdentifier";
    MKPinAnnotationView* pinView;
    pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:Identifier];
    
    if (pinView == nil) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:Identifier];
    }
    pinView.image=[UIImage imageNamed:@"pin-baseball.png"];
    
    if ([annotation isKindOfClass:[CustomPointAnnotation class]]) {
        CustomPointAnnotation *po = (CustomPointAnnotation*)annotation;
        if (po.univName) {
            pinView.image=[UIImage imageNamed:[self returnPinStr:po.univName]];
        }
    }
    
    pinView.annotation = annotation;
    pinView.canShowCallout = YES;
    pinView.pinColor = MKPinAnnotationColorRed;
    
    return pinView;
}

-(NSString *)returnPinStr:(NSString *)univName{
    NSString *str = nil;
    if ([univName isEqualToString:@"早稲田大学"]) {
        str=@"pin1.png";
    }else if([univName isEqualToString:@"東京大学"]){
        str=@"pin2.png";
    }else if([univName isEqualToString:@"立教大学"]){
        str=@"pin3.png";
    }else if([univName isEqualToString:@"明治大学"]){
        str=@"pin4.png";
    }else if([univName isEqualToString:@"慶應義塾大学"]){
        str=@"pin5.png";
    }else if([univName isEqualToString:@"法政大学"]){
        str=@"pin6.png";
    }else{
        str=@"pin-baseball.png";//神宮だけ
    }
    return str;
}

- (void)mapView:(MKMapView*)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    
    UIView *anikiInfoBG = [self.view viewWithTag:ANIKI_INFO_BACKGROUND_TAG];
    UILabel *titleLabel = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_LABEL_TAG];
    titleLabel.text = @"";
    
    UILabel *explanationLabel = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_DETAIL_TAG];
    explanationLabel.hidden=NO;
    //exTv.hidden=YES;

    //explanationLabel  = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_DETAIL_TAG];
    explanationLabel.frame = CGRectMake(85, 10+20, 235, 40);
    explanationLabel.text = @"六大学野球に関連するスポットに行って、\nチェックインボーナスをもらおう!!";
    explanationLabel.font = [UIFont systemFontOfSize:13];
    [explanationLabel  sizeToFit];
    
}

- (void)mapView:(MKMapView*)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (view.annotation == mapView.userLocation) {
        return;
    }
    
    if ([view.annotation isKindOfClass:[CustomPointAnnotation class]]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        CustomPointAnnotation *po = (CustomPointAnnotation*)view.annotation;
        button.tag = po.placeID;
        [button addTarget:self action:(@selector(moveCheckIn:)) forControlEvents:UIControlEventTouchUpInside];
        
        if ([self checkRadius:mapView.userLocation.coordinate anno:view.annotation]) {
            // コールアウトの左側のアクセサリビューにボタンを追加する
            view.rightCalloutAccessoryView = button;
        }
    }
    
    CustomPointAnnotation *annotation = (CustomPointAnnotation*)view.annotation;
    NSLog(@"%@", annotation.title);
    
    UIView *anikiInfoBG = [self.view viewWithTag:ANIKI_INFO_BACKGROUND_TAG];
   // UILabel *titleLabel = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_LABEL_TAG];
    UILabel *explanationLabel = (UILabel*)[anikiInfoBG viewWithTag:ANIKI_COMMENT_DETAIL_TAG];
    
    
#pragma mark　サブタイトル利用するときに変更するところ２
    if (annotation.subtitle == nil) {
        //titleLabel.text = [NSString stringWithFormat:@"%@ %@",annotation.univName, annotation.title];
        explanationLabel.text = [NSString stringWithFormat:@"%@ %@",annotation.univName, annotation.title];
    }else{
        NSString *longTitle = [annotation.title stringByAppendingString:annotation.subtitle];
        //titleLabel.text = [NSString stringWithFormat:@"%@ %@",annotation.univName, longTitle];
        explanationLabel.text = [NSString stringWithFormat:@"%@ %@",annotation.univName, longTitle];
    }
    
    //[self showScrollView];
    exTv.text=[NSString stringWithFormat:@"%@", annotation.detail];
    [anikiInfoBackground addSubview:customScrollView];
    
    UIImageView *compass = (UIImageView*)[anikiInfoBG viewWithTag:COMPASS_TAG];
    if (!self.checkinBtn.hidden && self.latitude == annotation.coordinate.latitude && self.longtitude == annotation.coordinate.longitude) {
        compass.image = [UIImage imageNamed:@"compass_here.png"];
    } else {
        if (annotation.coordinate.latitude < [LocationUtility sharedManager].latitude && annotation.coordinate.longitude < [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_s_w"];
        } else if (annotation.coordinate.latitude < [LocationUtility sharedManager].latitude && annotation.coordinate.longitude > [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_s_e"];
        } else if (annotation.coordinate.latitude > [LocationUtility sharedManager].latitude && annotation.coordinate.longitude < [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_n_w"];
        } else if (annotation.coordinate.latitude > [LocationUtility sharedManager].latitude && annotation.coordinate.longitude > [LocationUtility sharedManager].longtitude) {
            compass.image = [UIImage imageNamed:@"compass_n_e"];
        }
    }
    
}

- (BOOL)checkRadius:(CLLocationCoordinate2D)co anno:(CustomPointAnnotation*)anno {
    
    BOOL ret;
    
    if((pow(co.latitude-anno.coordinate.latitude, 2)+pow(co.longitude-anno.coordinate.longitude, 2))<=pow(anno.radius+anno.radius, 2)){
        
        ret = YES;
    } else {
        
        ret = FALSE;
    }
    
    return ret;
}


- (void)setUpMapParts
{
#pragma mark 地図の位置大きさ指定
    
    if (SCREEN_BOUNDS.size.height == 568) {
        
        self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0,0, 320, 505)];
    } else {
        
        self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0,0, 320, 390)];
    }
    self.mapView.delegate = self;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow];
    
    MKCoordinateRegion zoom = self.mapView.region;
    // どのくらいの範囲までズームするか。※値が小さいほどズームします
    zoom.span.latitudeDelta = 0.01;
    zoom.span.longitudeDelta = 0.01;
    // ズームする
    [self.mapView setRegion:zoom animated: YES];
    
    
    [self.view addSubview:self.mapView];
}

- (void)setUpAnikiInfomationParts
{
    anikiInfoBackground = [[UIView alloc] init];
    anikiInfoBackground.backgroundColor = [UIColor clearColor];
    anikiInfoBackground.tag = ANIKI_INFO_BACKGROUND_TAG;
    if (SCREEN_BOUNDS.size.height == 568) {
        anikiInfoBackground.frame = CGRectMake(0,380, 320, 88);
    } else {
        anikiInfoBackground.frame = CGRectMake(0, 345, 320, 88);
    }
    [self.view addSubview:anikiInfoBackground];
    
    UIImage *commentImg = [UIImage imageNamed:@"map_pop.png"];
    UIImageView *commentView = [[UIImageView alloc] initWithImage:commentImg];
    if (SCREEN_BOUNDS.size.height == 568) {
        [commentView setFrame:CGRectMake(5, 0, 310, 89)];
    }else{
        [commentView setFrame:CGRectMake(0, 0, 310, 89)];
    }
    [anikiInfoBackground addSubview:commentView];
    
    UILabel *anikiCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(85,0,200, 25)];
    anikiCommentLabel.textAlignment = NSTextAlignmentLeft;
    anikiCommentLabel.backgroundColor = [UIColor clearColor];
    anikiCommentLabel.textColor = [UIColor blackColor];
    anikiCommentLabel.font = [UIFont systemFontOfSize:11];
    anikiCommentLabel.adjustsFontSizeToFitWidth = YES;
    anikiCommentLabel.tag = ANIKI_COMMENT_LABEL_TAG;
    anikiCommentLabel.text = @"";
    [anikiInfoBackground addSubview:anikiCommentLabel];
    
    UILabel *explanationLabel = [[UILabel alloc] initWithFrame:CGRectMake(85, 10+20, 235, 40)];
    explanationLabel.textAlignment = NSTextAlignmentLeft;
    explanationLabel.backgroundColor = [UIColor clearColor];
    explanationLabel.textColor = [UIColor blackColor];
#pragma mark　最初の説明のフォント　試験的に変えてみる
    explanationLabel.font = [UIFont systemFontOfSize:13];
    explanationLabel.numberOfLines = 2;
    explanationLabel.adjustsFontSizeToFitWidth = YES;
    explanationLabel.tag = ANIKI_COMMENT_DETAIL_TAG;
    explanationLabel.text = @"六大学野球に関連するスポットに行って、\nチェックインボーナスをもらおう!!";
    [anikiInfoBackground addSubview:explanationLabel];
    
    
}


- (void)setUpParts
{
    self.view.backgroundColor = [UIColor grayColor];
    //[self setUpBackGroundParts];
    [self setUpMapParts];
    //[self setUpHeaderTabParts];
    [self setUpAnikiInfomationParts];
}

- (void)setUpTimer {
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timer:) userInfo:nil repeats:YES];
}

- (void)setUpNotification {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(changeCheckinBtnStatus:) name:@"checkinBtnStatus" object:nil];
}

- (void)removeNotification {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:@"checkinBtnStatus" object:nil];
}

#pragma mark - view life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    UILabel *albumCountLabel = (UILabel*)[self.view viewWithTag:ALBUM_COUNT_LABEL_TAG];
//    albumCountLabel.text = [NSString stringWithFormat:@"%d", [[DatabaseUtility sharedManager] getAlbumCount]];
    
    //self.cinemaAdImageView.hidden = NO;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setUpNotification];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeNotification];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if([[UIScreen mainScreen]bounds].size.height==568){
        //iPhone5の場合の処理
        _y568 = 44;
    }else{
        //それ以外の場合の処理
        _y568 = 0;
    }
    
    [self setUpParts];
    //[self setUpTimer];
    [self showPinInMapView];
    self.anikicardID = 0;
    self.view.tag = TAB_MAP;
    //タブボタン生成
    [self tabButtonsInit];
    [self showImobile];
    
    [TrackingManager sendScreenTracking:@"マップ画面"];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-TABBUTTON_HEIGHT-50)]; //広告を表示します
}

#pragma mark - タブボタン生成
-(void)tabButtonsInit
{
    TabButton *tb = [[TabButton alloc]initTabButtons:(int)self.view.tag];
    [tb setDelegate:self];
    tb.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-20);
    [self.view addSubview:tb];
}



#pragma mark - タブボタンどれか押した
- (void)anyButtonPushed:(id)sender
{
    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
    if (vc !=nil)
    {//現在の画面とタブが違う場合のみ遷移
        self.navigationController.navigationBarHidden = YES;
        if ([sender tag] == TAB_TOP)
        {//トップ画面のみポップで遷移
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else
        {//それ以外はプッシュで遷移
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
