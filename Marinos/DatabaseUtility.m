//
//  DatabaseUtility.m
//  BigbrotherMap
//
//  Created by kyo on 2013/07/18.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "DatabaseUtility.h"
#import "FMDatabase.h"

@interface DatabaseUtility ()

@property(nonatomic, strong) FMDatabase *db;

@end

@implementation DatabaseUtility

static DatabaseUtility* sharedUtil = nil;

#pragma mark - singleton method
+ (DatabaseUtility*)sharedManager
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];            
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

#pragma mark - データベース操作のためのopen,closeメソッド
- (BOOL)openDatabase
{
    //DBファイルへのパスを取得
    //パスは~/Documents/配下に格納される。
    NSString *dbPath = nil;
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得データ数を確認
    if ([documentsPath count] >= 1) {
        //固定で0番目を取得でOK
        dbPath = [documentsPath objectAtIndex:0];
        //パスの最後にファイル名をアペンドし、DBファイルへのフルパスを生成。
        dbPath = [dbPath stringByAppendingPathComponent:DB_FILE];
    } else {
        //error
        NSLog(@"search Document path error. database file open error.");
        return false;
    }
    
    //DBファイルがDocument配下に存在するか判定
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dbPath]) {
        //存在しない
        //デフォルトのDBファイルをコピー(初回のみ)
        //ファイルはアプリケーションディレクトリ配下に格納されている。
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *orgPath = [bundle bundlePath];
        //初期ファイルのパス。(~/XXX.app/sample.db)
        orgPath = [orgPath stringByAppendingPathComponent:DB_FILE];
        
        //デフォルトのDBファイルをDocument配下へコピー
        if (![fileManager copyItemAtPath:orgPath toPath:dbPath error:nil]) {
            //error
            NSLog(@"db file copy error. : %@ to %@.", orgPath, dbPath);
            return false;
        }
    }
    
    //open database with FMDB.
    self.db = [FMDatabase databaseWithPath:dbPath];
    return [self.db open];
}

- (void)closeDatabase
{
    if (self.db) {
        [self.db close];
    }
}

#pragma mark - データベース操作
- (BOOL) tablesCreater
{
    if ([self openDatabase]) {
        BOOL result = TRUE;

        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS historys(id INTEGER PRIMARY KEY, thumbnail BLOB, url TEXT, record_date TEXT, latitude REAL, longtitude REAL, title TEXT, body TEXT, datetime TEXT, like_count INTEGER, tweet_count INTEGER)"];
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS place(id INTEGER PRIMARY KEY AUTOINCREMENT, place TEXT, address TEXT, detail TEXT, place_category_id INTEGER, latitude REAL, longitude REAL, radius REAL, point INTEGER, check_in_count INTEGER, sp_check_in_count INTEGER, last_check_in_date TEXT, last_sp_check_in_date TEXT, last_notification_date TEXT)"];
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS sp_place(id INTEGER PRIMARY KEY AUTOINCREMENT, place_id INTEGER,sp_check_in_date TEXT, card_id INTEGER)"];
        
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS card(id INTEGER PRIMARY KEY AUTOINCREMENT, rare_level_id INTEGER, char_category_id INTEGER, card_year INTEGER, file_name TEXT, possession INTEGER, invalid_flag INTEGER)"];
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS rare_level(id INTEGER PRIMARY KEY AUTOINCREMENT, rare_level_name TEXT)"];
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS char_category(id INTEGER PRIMARY KEY AUTOINCREMENT, char_category_name TEXT)"];

        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS place_category_id(id INTEGER PRIMARY KEY AUTOINCREMENT, place_category_name TEXT)"];

        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
     
        return result;
    } else {
        NSLog(@"Can't open Database");
    }
    return NO;
}

- (NSMutableArray*) showTables {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sqlite_master WHERE type='table'"];
        while ([rs next]) {
            [array addObject:[rs stringForColumn:@"name"]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPhotos
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *identification = [rs stringForColumn:@"id"];
            if (identification != nil) {
                [dic setObject:identification forKey:@"identification"];
            }
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longitude = [rs stringForColumn:@"longitude"];
            if (longitude != nil) {
                [dic setObject:longitude forKey:@"longitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (UIImage*)getPhotoFromIdentification:(NSString*)identification
{
    if ([self openDatabase]) {
        FMResultSet *rs = [self.db executeQuery:@"SELECT thumbnail FROM historys WHERE id=?", identification];
        UIImage *thumbnail = nil;
        while ([rs next]) {
            thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
        }
        [rs close];
        [self closeDatabase];
        return thumbnail;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (BOOL)saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        NSData *imagedata = [[NSData alloc] initWithData:UIImageJPEGRepresentation(thumbnail, 0.2f)];
        
        NSDate *now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *nowStr = [formatter stringFromDate:now];
        
        [self.db executeUpdate:@"INSERT INTO historys(thumbnail, url, record_date, latitude, longtitude, title, body, datetime, like_count, tweet_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", imagedata, url, recordDate, [NSNumber numberWithDouble:latitude], [NSNumber numberWithDouble:longtitude], title, detail, nowStr, [NSNumber numberWithInt:INITIAL_LIKE_COUNT], [NSNumber numberWithInt:INITIAL_TWEET_COUNT]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (NSMutableArray*)getAlbumCells
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longitude = [rs stringForColumn:@"longitude"];
            if (longitude != nil) {
                [dic setObject:longitude forKey:@"longitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPlaceCells
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT place, address, detail, place_category_id, point FROM place"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *place = [rs stringForColumn:@"place"];
            if (place != nil) {
                [dic setObject:place forKey:@"place"];
            }
            
            NSString *address = [rs stringForColumn:@"address"];
            if (address != nil) {
                [dic setObject:address forKey:@"address"];
            }
            
            NSString *detail = [rs stringForColumn:@"detail"];
            if (detail != nil) {
                [dic setObject:detail forKey:@"detail"];
            }
            
            int place_category = [rs intForColumn:@"place_category_id"];
            [dic setObject:[NSNumber numberWithInt:place_category] forKey:@"place_category_id"];

            int point = [rs intForColumn:@"point"];
            [dic setObject:[NSNumber numberWithInt:point] forKey:@"point"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
/*
- (NSMutableArray*)getLocationInCategory:(AnnotationCategory)category
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        
        NSString *locationCategory;
        switch (category) {
            case AnnotationCategorySights:
            {
                locationCategory = @"sights";
                break;
            }
            case AnnotationCategoryFood:
            {
                locationCategory = @"food";
                break;
            }
            case AnnotationCategoryShop:
            {
                locationCategory = @"shop";
                break;
            }
            case AnnotationCategoryHotel:
            {
                locationCategory = @"hotel";
                break;
            }
                
            default:
                break;
        }
        
        FMResultSet *rs = [self.db executeQuery:@"SELECT latitude, longtitude, radius FROM place WHERE category=?", locationCategory];
        
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            double latitude = [rs doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longtitude = [rs doubleForColumn:@"longtitude"];
            [dic setObject:[NSNumber numberWithDouble:longtitude] forKey:@"longtitude"];

            double radius = [rs doubleForColumn:@"radius"];
            [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
*/
- (NSMutableArray*)getAllLocation
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM checkInPoint"];
        
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int identifier = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:identifier] forKey:@"identifier"];
            
            NSString *place = [rs stringForColumn:@"name"];
            if (place != nil) {
                [dic setObject:place forKey:@"name"];
            }
            
            NSString *univName = [rs stringForColumn:@"univ_name"];
            if (univName != nil) {
                [dic setObject:univName forKey:@"univ_name"];
            }

            NSString *detail = [rs stringForColumn:@"explanation"];
            if (detail != nil) {
                [dic setObject:detail forKey:@"explanation"];
            }

            int place_category = [rs intForColumn:@"card_category"];
            [dic setObject:[NSNumber numberWithInt:place_category] forKey:@"card_category"];
            
            
            double latitude = [rs doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [rs doubleForColumn:@"longitude"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            double radius = [rs doubleForColumn:@"r"];
            [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableDictionary*)getLocation:(int)identifier
{
    if ([self openDatabase]) {
    
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM checkInPoint WHERE id=?", [NSNumber numberWithInt:identifier]];
        
        while ([rs next]) {
            NSString *place = [rs stringForColumn:@"name"];
            NSLog(@"ここ%@",place);
            if (place != nil) {
                [dic setObject:place forKey:@"name"];
            }
            
//            NSString *file_name = [rs stringForColumn:@"file_name"];
//            if (file_name != nil) {
//                [dic setObject:file_name forKey:@"file_name"];
//            }
            
            NSString *lastDate = [rs stringForColumn:@"last_check_in_date"];
            if (lastDate != nil) {
                [dic setObject:lastDate forKey:@"last_check_in_date"];
            }
            
            double latitude = [rs doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [rs doubleForColumn:@"longitude"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            double radius = [rs doubleForColumn:@"r"];
            [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
            
            int check_in_flag = [rs intForColumn:@"check_in_flag"];
            NSLog(@"checkinflag in getlocation:%d",check_in_flag);
            [dic setObject:[NSNumber numberWithInt:check_in_flag] forKey:@"check_in_flag"];
            
        }
        [rs close];
        [self closeDatabase];
        
        return dic;
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
    return nil;
}

//- (NSMutableArray*)getSPCheckIn:(int)identifier {
//    
//    if ([self openDatabase]) {
//        
//        /*
//         NSMutableArray *array = [NSMutableArray array];
//         FMResultSet *rs = [self.db executeQuery:@"SELECT card_id, rare_level_id FROM card WHERE invalid_flag <> 1;"];
//         while ([rs next]) {
//         // Get the column data for this record and put it into a custom Record object
//         NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//         
//         int card_id = [rs intForColumn:@"card_id"];
//         [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
//         
//         int rare_level_id = [rs intForColumn:@"rare_level_id"];
//         [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
//         
//         [array addObject:dic];
//         }
//         [rs close];
//         [self closeDatabase];
//         return array;
//
//         */
//        NSMutableArray *array = [NSMutableArray array];
//
//        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sp_place WHERE place_id=?", [NSNumber numberWithInt:identifier]];
//        
//        while ([rs next]) {
//            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//
//            int placeID = [rs intForColumn:@"place_id"];
//            [dic setObject:[NSNumber numberWithInt:placeID] forKey:@"place_id"];
//            
//            NSString *date = [rs stringForColumn:@"sp_check_in_date"];
//            if (date != nil) {
//                [dic setObject:date forKey:@"sp_check_in_date"];
//            }
//            
//            int cardID = [rs intForColumn:@"card_id"];
//            [dic setObject:[NSNumber numberWithInt:cardID] forKey:@"card_id"];
//            [array addObject:dic];
//        }
//        [rs close];
//        [self closeDatabase];
//        
//        return array;
//    } else
//    {
//        NSLog(@"DBが開けなかった");
//    }
//    return nil;
//}



- (BOOL)updateLastCheckIn:(NSString*)now withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE checkInPoint SET last_check_in_date=?, check_in_count=check_in_count+1 WHERE id=?", now,[NSNumber numberWithInt:identifier]];


        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

//- (BOOL)updateLastSPCheckIn:(NSString*)now withID:(int)identifier
//{
//    if ([self openDatabase]) {
//        BOOL result = TRUE;
//        
////        [self.db executeUpdate:@"UPDATE place SET last_sp_check_in_date=? WHERE id=?", now, [NSNumber numberWithInt:identifier]];
////        [self.db executeUpdate:@"UPDATE place SET sp_check_in_count=sp_check_in_count+1 WHERE id=?",[NSNumber numberWithInt:identifier]];
//        [self.db executeUpdate:@"UPDATE place SET last_sp_check_in_date=?, sp_check_in_count=sp_check_in_count+1 WHERE id=?", now,[NSNumber numberWithInt:identifier]];
//
//
//        //check
//        if ([self.db hadError]) {
//            result = FALSE;
//            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
//        }
//        return result;
//        
//    } else {
//        NSLog(@"DBが開けなかった");
//    }
//    return NO;
//}

/*うえと併せた
- (BOOL)updateForCheckin:(int)cnt withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE place SET check_in_count=? WHERE id=?", cnt, [NSNumber numberWithInt:identifier]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)updateForSPCheckin:(int)cnt withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE place SET sp_check_in_count=? WHERE id=?", cnt, [NSNumber numberWithInt:identifier]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}
*/



- (BOOL)updateLastNotification:(NSString*)now withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE place SET last_notification_date=? WHERE id=?", now, [NSNumber numberWithInt:identifier]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)updateGetCard:(int)cardID checkInFlag:(int)ownedNum
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //NSLog(@"checkinflag in updateDB:%d",checkInFlag);
        //[self.db executeUpdate:@"UPDATE card SET possession=possession+1 WHERE card_id=?", [NSNumber numberWithInt:cardID]];
        [self.db executeUpdate:@"UPDATE card SET ownedNum=? WHERE id=?",[NSNumber numberWithInt:ownedNum],[NSNumber numberWithInt:cardID]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}




- (int)getAlbumCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getCardCountEachUniv:(NSString*)uivStr
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card where univ = ?;",uivStr];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}


- (int)getGettedCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card WHERE possession > 0"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getGettedAllCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT SUM(possession) AS cnt FROM card"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}


- (int)getGettedCardPossession:(int)cardID
{
    if ([self openDatabase]) {
        
        int pos = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT ownedNum FROM card WHERE id=?", [NSNumber numberWithInt:cardID]];
        while ([rs next]) {
            //            //ここでデータを展開
            pos = [rs intForColumn:@"ownedNum"];
        }
        [rs close];
        [self closeDatabase];
        return pos;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}


- (NSMutableDictionary*)getSelectedCard:(int)cardID
{
    if ([self openDatabase]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM card WHERE id=?", [NSNumber numberWithInt:cardID]];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            
            int card_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"id"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"explanation"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"explanation"];
            }
            
            int possession = [rs intForColumn:@"ownedNum"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"ownedNum"];
        }
        [rs close];
        [self closeDatabase];
        return dic;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}


#pragma mark 未保有のみ取り出し
- (NSMutableArray*)getNotPossessingArray{
    
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
       // FMResultSet *rs = [self.db executeQuery:@"SELECT * WHERE check_in_flag <> 1;"];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM card WHERE check_in_flag = 0 and card_category = 2;"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int card_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"id"];
            
            int checkinflag = [rs intForColumn:@"check_in_flag"];
            [dic setObject:[NSNumber numberWithInt:checkinflag] forKey:@"check_in_flag"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *spotname = [rs stringForColumn:@"name"];
            if (file_name != nil) {
                [dic setObject:spotname forKey:@"name"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;

}

- (NSMutableArray*)getAllCard
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM card;"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];

            int card_id = [rs intForColumn:@"id"];
            //[dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            [dic setValue:[NSNumber numberWithInt:card_id] forKeyPath:@"card_id"];
            
            int ownedNum = [rs intForColumn:@"ownedNum"];
            //[dic setObject:[NSNumber numberWithInt:ownedNum] forKey:@"ownedNum"];
            [dic setValue:[NSNumber numberWithInt:ownedNum] forKeyPath:@"ownedNum"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getCardIdEachUniv:(NSString*)univStr
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id FROM card where univ = ?;",univStr];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            //NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int card_id = [rs intForColumn:@"id"];
              [array addObject:[NSNumber numberWithInt:card_id]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getCardId
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id FROM card;"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            //NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int card_id = [rs intForColumn:@"id"];
            [array addObject:[NSNumber numberWithInt:card_id]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}


- (NSMutableArray*)getCardIdArray
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id FROM card;"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            //NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int card_id = [rs intForColumn:@"id"];
            [array addObject:[NSNumber numberWithInt:card_id]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}




- (void)updateProcess
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults setObject:@"1.0.0" forKey:@"oldVersion"];
    [ud registerDefaults:defaults];
    
    // ホームディレクトリを取得
    NSString* path = [[NSBundle mainBundle] pathForResource:@"update" ofType:@"plist"];
    
    // 読み込みたいplistのパスを作成
    NSMutableArray* array = [NSMutableArray arrayWithContentsOfFile:path];
    
    //********************************************************************************
#pragma mark デバッグ用にチェックポイント１を会社に変更　本番はまとめてコメントアウト！！
    
//    if ([self openDatabase]) {
//        NSLog(@"デバッグ用にupdate!!");
//        NSString *sql=@"UPDATE checkInPoint SET latitude=35.416549,longitude=136.760198,r=50000 WHERE id=1;";
//        [self.db executeUpdate:sql];
//        
//        //check
//        if ([self.db hadError]) {
//            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
//        }
//        
//        [self closeDatabase];
//    } else {
//        
//        NSLog(@"DBが開けなかった");
//    }

    //********************************************************************************
    
    for (NSMutableDictionary *dict in array) {
        
        // アプリバージョン情報
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//        // ビルドバージョン情報
//        NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];

        NSString *oldVersion = [ud stringForKey:@"oldVersion"];

        // plist内のバージョン情報
        NSString *update = [dict objectForKey:@"version"];
        NSLog(@"%@", update);
        
        if ([version compare:update options:NSNumericSearch] >= NSOrderedSame && [oldVersion compare:update options:NSNumericSearch] == NSOrderedAscending) {
            NSLog(@"%@バージョンアップ！", update);
            [ud setObject:update forKey:@"oldVersion"];
            [ud synchronize];
            
            if ([self openDatabase]) {
                for (NSString *sql in [dict objectForKey:@"sql"]) {
                    [self.db executeUpdate:sql];
                }
                
                
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            
                [self closeDatabase];
            } else {
            
            NSLog(@"DBが開けなかった");
            }
        } else {
            
            NSLog(@"バージョンアップしない");
        }
    }
}


@end
