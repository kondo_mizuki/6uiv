//
//  Define.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/07.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#ifndef Marinos_Define_h
#define Marinos_Define_h

#define WIN_SIZE [[UIScreen mainScreen]bounds].size

// Define
// tag
#define SIGHTS_TAG 100
#define FOOD_TAG 101
#define HOTEL_TAG 102
#define SHOP_TAG 103
#define ACCOUNTING_TAG 104
#define DETAIL_TEXTFIELD_TAG 105
#define SENDPHOTO_BACKGROUND_TAG 106
#define SHOT_BTN_TAG 107
#define POST_BTN_TAG 1111
#define TITLE_TEXTFIELD_TAG 108
#define TWITTER_BTN_TAG 109
#define FACEBOOK_BTN_TAG 110
#define STAY_TIME_LABEL_TAG 111
#define ALBUM_COUNT_LABEL_TAG 112
#define GETTED_ANIKICARD_COUNT_LABEL_TAG 113
#define ANIKI_INFO_BACKGROUND_TAG 114
#define ANIKI_COMMENT_LABEL_TAG 115
#define COMPASS_TAG 116

#define POINT_LABEL_TAG 117
#define GETTED_CARD_COUNT_LABEL_TAG 118

#define ANIKI_COMMENT_DETAIL_TAG 119
#define BOTTOM_LABEL_TAG 120


#define CARD_DONT_HAVE 901
#define CARD_HAVE 902

#warning リリース時は100に変更する！！！
#define LOGIN_FIRST 50
//#define LOG_IN_BONUS_SPETIAL 20
#define LOG_IN_BONUS_NORMAL 10

#define TAB_TOP 1001
#define TAB_MAP 1002
#define TAB_CARD 1003
#define TAB_ALBUM 1004
#define TAB_MORE 1999
#define SOUND_TAB 1
#define SOUND_GET_BUTTON 2
#define SOUND_CARD_APPEAR 3
#define SOUND_CARD_BASE 4
#define SOUND_CARD_ZOOM_RARE 5
#define SOUND_CARD_LAST 6
#define SOUND_ALBUM 7
#define SOUND_KIIIN 8
#define SOUND_CHUIU 9
#define SOUND_PYORO 10
#define SOUND_PIRON 11

#define CARD_LEVEL_NORMAL 1
#define CARD_LEVEL_RARE 2
#define CARD_LEVEL_HOMEGAME 3
#define CARD_LEVEL_SPECIAL 4 //これは使わないかも

#define TABBUTTON_HEIGHT 45
#define TOOLBAR_HEIGHT 37
// DB_FILE
#define DB_FILE @"6univ.db"

// dev環境
#define HEROKU_DEV @"http://timecapsule-eagle-dev.herokuapp.com"

// 本番環境
//バッチさん指示で変更その１
#define HEROKU_MASTER @"http://timecapsule-eagle-staging.herokuapp.com"
//#define HEROKU_MASTER @"http://www.timecapsuleinc.info"

#define MY_FONT @"Cochin"

#define INITIAL_LIKE_COUNT 0
#define INITIAL_TWEET_COUNT 0

#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]

//#define NavBarColor [UIColor colorWithRed:0.188 green:0.188 blue:0.188 alpha:1]
#define NavBarColor [UIColor colorWithRed:0.812 green:0.443 blue:0 alpha:1]

//album
#define ALBUM_2015 @"2015"
#define ALBUM_2016 @"2016"

typedef NS_ENUM(NSInteger, AnnotationCategory)
{
    AnnotationCategorySights = 0,
    AnnotationCategoryFood,
    AnnotationCategoryHotel,
    AnnotationCategoryShop
};

typedef NS_ENUM(NSInteger, LogInCategory)
{
    AlreadyGetLogInBonus = 0,
    FirstLogIn,
    NormalLogIn,
    SPLogIn
};

#endif
