//
//  ticketVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "ticketVC.h"

@interface ticketVC (){
    //SKPayment *payment;
}

@end

@implementation ticketVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showImobile];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    
    [TrackingManager sendScreenTracking:@"機能拡張"];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)]; //広告を表示します
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    //self.title=@"チケット購入";
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"機能拡張";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
//        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    }
}


-(void)backAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark　課金
// メソッド名は適当に
//- (BOOL)_checkInAppPurchase
//{
//    if (![SKPaymentQueue canMakePayments]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
//                                                        message:@"アプリ内課金が制限されています。"
//                                                       delegate:nil
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"OK", nil];
//        [alert show];
//        return NO;
//    }
//    //[self _startInAppPurchase];
//    return YES;
//}


// メソッド名は適当に（チェック処理の結果がYESだったらこの処理を呼ぶ）
//- (void)_startInAppPurchase
//{
//    // com.companyname.application.productidは、「1-1. iTunes ConnectでManage In-App Purchasesの追加」で作成したProduct IDを設定します。
//    NSSet *set = [NSSet setWithObjects:@"gameResule0220", nil];
//    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
//    productsRequest.delegate = self;
//    [productsRequest start];
//}

#pragma mark SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    //    // 無効なアイテムがないかチェック
    //    if ([response.invalidProductIdentifiers count] > 0) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
    //                                                        message:@"アイテムIDが不正です。"
    //                                                       delegate:nil
    //                                              cancelButtonTitle:@"OK"
    //                                              otherButtonTitles:nil, nil];
    //        [alert show];
    //        return;
    //    }
    //
    //    if (response == nil) return;
    //
    //    if (!self.isFromRestore) {//購入ボタンから
    //            for (NSString *identifier in response.invalidProductIdentifiers) {
    //                NSLog(@"invalid product identifier: %@", identifier);
    //                return;
    //            }
    //
    //            //確認できなかった場合購入、処理開始
    //        if (1) {
    //            for (SKProduct *product in response.products) {
    //                //SKPayment *payment = [SKPayment paymentWithProduct:product];
    //                payment = [SKPayment paymentWithProduct:product];
    //                [[SKPaymentQueue defaultQueue] addPayment:payment];
    //                return;
    //            }
    //
    //            NSLog(@"購入ボタン押したのに、購入済み");
    //            UIAlertView *alert =
    //            [[UIAlertView alloc] initWithTitle:nil message:@"購入済みです。リストアしてください。"
    //                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            [alert show];
    //
    //        }else{
    //            NSLog(@"購入ボタン押したのに、購入済み");
    //            UIAlertView *alert =
    //            [[UIAlertView alloc] initWithTitle:nil message:@"購入済みです。リストアしてください。"
    //        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            [alert show];
    //        }
    //    }else{//リストアボタンから
    //
    //        for (NSString *identifier in response.invalidProductIdentifiers) {
    //            NSLog(@"invalid product identifier: %@", identifier);
    //            return;
    //        }
    //
    //        if (1) {//確認できた場合、リストア
    //        for (SKProduct *product in response.products ) {
    //            if ([product.productIdentifier isEqualToString:@"gameResule0220"]) {
    //                // payment はヘッダに記述済みとする(SKPayment *payment;)
    //                payment = [SKPayment paymentWithProduct:product];
    //
    //                // 購入履歴チェック
    //                [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    //                [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    //                //restore = YES;
    //                //return;
    //                break;
    //            }
    //        }
    //        }else{
    //            NSLog(@"リストアボタン押したのに未購入");
    //            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"未購入です。購入ボタンを押してください。"
    //                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //            [alert show];
    //        }
    //    }
    
    // 無効なアイテムがないかチェック
    if ([response.invalidProductIdentifiers count] > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
                                                        message:@"アイテムIDが不正です。"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    // 購入処理開始
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    for (SKProduct *product in response.products) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        if (payment) {
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
    }
}

// トランザクション処理
#pragma mark SKPaymentTransactionObserver
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for (SKPaymentTransaction *transaction in transactions) {
        if (transaction.transactionState == SKPaymentTransactionStatePurchasing) {
            NSLog(@"payment purchasing.");
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
            NSLog(@"payment transaction.payment.productIdentifier : %@",transaction.payment.productIdentifier);
            [queue finishTransaction: transaction];
            //このあたりで「課金が終了しましたー」等のアラートを表示することが推奨されていたはずー
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:@"CHARGES_FLAG"];
            [ud synchronize];
            
           // [self.navigationController popToRootViewControllerAnimated:NO];
            
            //さらに、課金が終了したことをなんらかの形でファイル等に記入しておくこと
        } else if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            // Also come here if user canceled.
            [queue finishTransaction:transaction];
            if (!transaction.error.code == SKErrorPaymentCancelled) {
                NSLog(@"payment error : %@", transaction.error.localizedDescription);
            } else {
                NSLog(@"payment transaction is canceled");
            }
        } else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
            // Restore
            NSLog(@"payment transaction.originalTransaction.payment.productIdentifier :%@",transaction.originalTransaction.payment.productIdentifier);
            [queue finishTransaction:transaction];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:@"CHARGES_FLAG"];
            [ud synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"リストアに成功しました"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            [alert show];

            
            //こちらはリストアの処理、こちらもこのあたりで「リストアが終了しましたー」等のアラートを表示
            //することが推奨されていたはずー
            //さらに、リストアが終了したことをなんらかの形でファイル等に記入しておくこと
        }else{
            NSLog(@"else!!!");
        }
    }
}

// リストア処理結果
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"リストア失敗:%@", error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"リストアに失敗しました。"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [alert show];
    // TODO: 失敗のアラート表示等
}

// 購入履歴が確認できた場合
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    BOOL restore = NO;
    
    for (SKPaymentTransaction *transaction in queue.transactions) {
        NSLog(@"購入:%@", transaction);
        restore = YES;
    }
    
    if (restore) {
        NSLog(@"全てのリストア完了");
        //[self.navigationController popToRootViewControllerAnimated:NO];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"購入していません"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

#pragma mark SKPaymentQueue
- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView*)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            //１番目のボタンが押されたときの処理を記述する
            [self.navigationController popToRootViewControllerAnimated:NO];
            break;
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)pushPayBtn:(id)sender {
    
    //    if ([self _checkInAppPurchase]) {
    //        self.isFromRestore=NO;
    //        [self _startInAppPurchase];
    //    }
    //[self _checkInAppPurchase];
    
    //iPhone自体の設定でアプリ内課金の制限がされていないかどうかチェック。制限されていたら
    //アラート表示し、アプリ内課金を中止する。
    if (![SKPaymentQueue canMakePayments]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
                                                        message:@"アプリ内課金が制限されています。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    // 課金部分
    [self requestProductData];
}

- (IBAction)pushRestoreBtn:(id)sender {
    
    //    if ([self _checkInAppPurchase]) {
    //        self.isFromRestore=YES;
    //        [self _startInAppPurchase];
    //    }
    // リストア部分
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)requestProductData
{
    //ituneconnectの「Manage In-App Purchases」で設定したキーを設定する
    //「jp.co.******.noad」の部分を正しいものに置き換える必要があります。
    NSString *kMyFeatureIdentifier = @"gameResule0220";
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kMyFeatureIdentifier]];
    request.delegate = self;
    [request start];
}

@end
