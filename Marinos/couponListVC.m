//
//  couponListVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/24.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "couponListVC.h"

@interface couponListVC ()
@property (nonatomic, retain) NSMutableArray *aryCellIdentifier;
@end

@implementation couponListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showImobile];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)]; //広告を表示します
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = [self returnCouponTitle];
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefaultPrompt];
    }
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSString*)returnCouponTitle{
    NSString *str =nil;
    switch (self.couponNum) {
        case 1:
            str=@"食べるクーポン一覧";
            break;
        case 2:
            str=@"買うクーポン一覧";
            break;
        case 3:
            str=@"遊ぶクーポン一覧";
            break;
        case 4:
            str=@"見るクーポン一覧";
            break;
        default:
            break;
    }
    return str;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return self.aryCellIdentifier.count;
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	// 高さはアプリ出荷時には確定している性質のものなので、動的に算出せずに直書きとする。
	return 110;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//NSString *CellIdentifier = [self.aryCellIdentifier objectAtIndex:indexPath.row];
    NSString *CellIdentifier =@"couponID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    couponDetailVC *cs2VC = (couponDetailVC *)[storyboard instantiateViewControllerWithIdentifier:@"couponDetailVC"];
    [self.navigationController pushViewController:cs2VC animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor lightGrayColor];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
