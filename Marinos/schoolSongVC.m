//
//  schoolSongVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/09.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "schoolSongVC.h"

@interface schoolSongVC (){
    NSDictionary *songDic;
}

@end

@implementation schoolSongVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self initSongData];
    [self initBtn];
    [self initBG];
    [self adjustTab];
    self.title=[self returnSchoolName];
    [self initLinkBtn];
    [self showImobile];
    
    [TrackingManager sendScreenTracking:@"校歌・応援歌画面"];
    [TrackingManager sendEventTracking:@"ToSchoolSong" action:@"tap" label:[self returnSchoolName] value:0 screen:@"top"];
}

-(void)initBG{
    self.view.backgroundColor = [self returnColor];
}

-(UIColor*)returnColor{
    UIColor *color =nil;
    switch (self.schoolNum) {
        case 1:
            color=[UIColor colorWithRed:0.235 green:0.016 blue:0.345 alpha:1];
            break;
        case 2:
            color=[UIColor colorWithRed:0.325 green:0 blue:0.078 alpha:1];
            break;
        case 3:
            color=[UIColor colorWithRed:0.369 green:0.188 blue:0.545 alpha:1];
            break;
        case 4:
            color=[UIColor colorWithRed:0.863 green:0 blue:0.075 alpha:1];
            break;
        case 5:
            color=[UIColor colorWithRed:1 green:0.424 blue:0.157 alpha:1];
            break;
        case 6:
            color=[UIColor colorWithRed:0.275 green:0.475 blue:0.722 alpha:1];
            break;
        default:
            break;
    }
    return color;
}


-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)]; //広告を表示します
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = [self returnSchoolName];
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
//        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    }
}

-(void)backAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)initSongData{
    // plist読み込み
    // バンドル設定
    NSBundle* bundle = [NSBundle mainBundle];
    // 読み込むファイルパスを指定
    NSString* path = [bundle pathForResource:@"songs" ofType:@"plist"];
    NSArray *plist = [NSArray arrayWithContentsOfFile:path];
    songDic = [plist objectAtIndex:self.schoolNum-1];
    self.songTV.text=[songDic valueForKey:@"song1"];
    self.songTV.font=[UIFont fontWithName:@"AppleGothic" size:15];
}

-(void)initBtn{
    [self.tab1 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-nonac.png"] forState:UIControlStateNormal];
    [self.tab2 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-nonac.png"] forState:UIControlStateNormal];
    [self.tab3 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-nonac.png"] forState:UIControlStateNormal];
    
    [self.tab1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.tab2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.tab3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.tab1 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-ac.png"] forState:UIControlStateSelected];
    [self.tab2 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-ac.png"] forState:UIControlStateSelected];
    [self.tab3 setBackgroundImage:[UIImage imageNamed:@"coll_tab2-ac.png"] forState:UIControlStateSelected];
    
    [self.tab1 setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [self.tab2 setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [self.tab3 setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    
    self.tab1.selected=YES;
    self.tab2.selected=NO;
    self.tab3.selected=NO;
}

-(void)adjustTab{
    if (songDic.count==3) {
        self.tab1.frame=CGRectMake(0, 50, 160, 30);
        self.tab2.frame=CGRectMake(160,50,160, 30);
        self.tab3.hidden=YES;
    }
}

-(NSString*)returnSchoolName{
    NSString *str =nil;
    switch (self.schoolNum) {
        case 1:
            str=@"明治大学";
            break;
        case 2:
            str=@"早稲田大学";
            break;
        case 3:
            str=@"立教大学";
            break;
        case 4:
            str=@"慶應義塾大学";
            break;
        case 5:
            str=@"法政大学";
            break;
        case 6:
            str=@"東京大学";
            break;
        default:
            break;
    }
    return str;
}

-(void)initLinkBtn{
    NSString *songStr = [NSString stringWithFormat:@"%@ 野球部",[self returnSchoolName]];
    [self.linkBtn setTitle:songStr forState:UIControlStateNormal];
    [self.linkBtn setTitle:songStr forState:UIControlStateHighlighted];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tab1Push:(id)sender {
    [self allSelectedNo];
    self.tab1.selected=YES;
    self.songTV.text=[songDic valueForKey:@"song1"];
    self.songTV.font=[UIFont fontWithName:@"AppleGothic" size:15];
}

- (IBAction)tab2Push:(id)sender {
    [self allSelectedNo];
    self.tab2.selected=YES;
    self.songTV.text=[songDic valueForKey:@"song2"];
    self.songTV.font=[UIFont fontWithName:@"AppleGothic" size:15];
}

- (IBAction)tab3Push:(id)sender {
    [self allSelectedNo];
    self.tab3.selected=YES;
    self.songTV.text=[songDic valueForKey:@"song3"];
    self.songTV.font=[UIFont fontWithName:@"AppleGothic" size:15];
}

-(void)allSelectedNo{
    self.tab1.selected=NO;
    self.tab2.selected=NO;
    self.tab3.selected=NO;
}
- (IBAction)linkBtnPush:(id)sender {
    //NSURL *linkURL = [NSURL URLWithString:[self returnURL]];
    //[[UIApplication sharedApplication] openURL:url];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    rocksWebVC *grVC = (rocksWebVC *)[storyboard instantiateViewControllerWithIdentifier:@"rocksWebVC"];
    grVC.urlStr = [self returnURL];
    grVC.title = [self returnSchoolName];
    [self.navigationController pushViewController:grVC animated:YES];
}
-(NSString*)returnURL{
    NSString *str =nil;
    switch (self.schoolNum) {
        case 1:
            str=@"http://homepage3.nifty.com/meijibbc";
            break;
        case 2:
            str=@"http://www.wasedabbc.org";
            break;
        case 3:
            str=@"http://rikkio-bbc.com";
            break;
        case 4:
            str=@"http://baseball.sfc.keio.ac.jp";
            break;
        case 5:
            str=@"http://hosei-baseball.jp";
            break;
        case 6:
            str=@"http://www.tokyo-bbc.net";
            break;
        default:
            break;
    }
    return str;
}
@end
