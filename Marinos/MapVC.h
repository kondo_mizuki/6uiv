//
//  MapVC.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKit/MapKit.h"
#import <iAd/iAd.h>
#import "CustomTextView.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "checkInImageVC.h"

@interface MapVC : UIViewController<MKMapViewDelegate,ADBannerViewDelegate,UIScrollViewDelegate>
{
    ADBannerView *adView;    // 広告用の枠
    BOOL bannerIsVisible;    // 広告　表示・非表示用のフラグ
}
@end
