//
//  PremiumVC.h
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PremiumVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *entryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *cautionView;
//@property (weak, nonatomic) IBOutlet UILabel *cautionLbl;
@property (weak, nonatomic) IBOutlet UIImageView *premiumBG;

- (IBAction)entryBtnPush:(id)sender;

@end
