//
//  DatabaseUtility.h
//  BigbrotherMap
//
//  Created by kyo on 2013/07/18.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"

@interface DatabaseUtility : NSObject

+ (DatabaseUtility*)sharedManager;
- (NSMutableArray*)getAlbumCells;
- (NSMutableArray*)getPlaceCells;
- (NSMutableArray*)getAllLocation;
- (NSMutableDictionary*)getLocation:(int)identifier;
- (NSMutableArray*)getSPCheckIn:(int)identifier;
- (BOOL)updateLastCheckIn:(NSString*)now withID:(int)identifier;
- (BOOL)updateLastSPCheckIn:(NSString*)now withID:(int)identifier;
//- (BOOL)updateForCheckin:(int)cnt withID:(int)identifier;
//- (BOOL)updateForSPCheckin:(int)cnt withID:(int)identifier;
- (BOOL)updateLastNotification:(NSString*)now withID:(int)identifier;
- (BOOL)updateGetCard:(int)cardID checkInFlag:(int)checkInFlag;
- (int)getAlbumCount;
- (int)getCardCount;
//- (int)getAllCardCount;
- (int)getGettedCardCount;
- (int)getGettedAllCardCount;
- (int)getGettedCardPossession:(int)cardID;
- (NSMutableDictionary*)getSelectedCard:(int)cardID;
- (NSMutableArray*)getAllCard;
- (void)updateProcess;
- (int)getCardCountEachUniv:(NSString*)uivStr;

- (NSMutableArray*)getNotPossessingArray;
- (NSMutableArray*)getCardIdEachUniv:(NSString*)univStr;
- (NSMutableArray*)getCardIdArray;
- (NSMutableArray*)getCardId;
@end
