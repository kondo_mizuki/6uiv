//
//  AppDelegate.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "AppDelegate.h"

#import "TopVC.h"
#import "Util.h"
#import "DatabaseUtility.h"
#import "LocationUtility.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import <NCMB/NCMB.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    [[Util sharedManager] prepareToPlaySounds];
    [[DatabaseUtility sharedManager] updateProcess];
    [[LocationUtility sharedManager] getLocation];
    
    [self initializeGoogleAnalytics];
    
    UIStoryboard *sb = [[[self window] rootViewController] storyboard];
    self.tvc = (TopVC*)[sb instantiateViewControllerWithIdentifier:@"TopVC"];
    
    // PUSH通知の準備
    [NCMB setApplicationKey:@"bc199941e7410df7088dd0959b7ca42285f72996fa56681c0a00d2997611da9c" clientKey:@"e94bae67d79cfe71a6748239dee4c84d21857db0227c4d6e3497a4277c14c620"];
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1){
        UIUserNotificationType type = UIRemoteNotificationTypeAlert |
        UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound;
        UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:type
                                                                                categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:setting];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert |
          UIRemoteNotificationTypeBadge |
          UIRemoteNotificationTypeSound)];
    }

    
    // リッチPUSHの準備
    [NCMBPush handleRichPush:[launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]];
    
    // Override point for customization after application launch.
    
    [Util sharedManager].heroku = HEROKU_MASTER;
    
    [Fabric with:@[CrashlyticsKit]];
    
    return YES;
}

- (void)initializeGoogleAnalytics
{
    // トラッキングIDを設定
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-44275326-14"];
    
    // 例外を Google Analytics に送る
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    [GAI sharedInstance].dispatchInterval = 20;
    
    [TrackingManager sendScreenTracking:@"トップ画面"];
}

#warning Nifty通知時
// 配信端末情報を登録する
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NCMBInstallation *currentInstallation = [NCMBInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    //    [currentInstallation setObject:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
    [currentInstallation saveInBackgroundWithBlock:^(NSError *error) {
        if(!error){
            //端末情報の登録が成功した場合の処理
        } else {
            //端末情報の登録が失敗した場合の処理
            if (error.code == 409001){
                //失敗した原因がdeviceTokenの重複だった場合
                [self updateExistInstallation:currentInstallation];
            } else {
                //deviceTokenの重複以外のエラーが返ってきた場合
            }
        }    }];
}

//deviceTokenの重複で端末情報の登録に失敗した場合に上書き処理を行う
-(void)updateExistInstallation:(NCMBInstallation*)currentInstallation{
    NCMBQuery *installationQuery = [NCMBInstallation query];
    [installationQuery whereKey:@"deviceToken" equalTo:currentInstallation.deviceToken];
    
    NSError *searchErr = nil;
    NCMBInstallation *searchDevice = [installationQuery getFirstObject:&searchErr];
    
    if (!searchErr){
        //上書き保存する
        currentInstallation.objectId = searchDevice.objectId;
        [currentInstallation saveInBackgroundWithBlock:^(NSError *error) {
            if (!error){
                //端末情報更新に成功したときの処理
            } else {
                //端末情報更新に失敗したときの処理
            }
        }];
    } else {
        //端末情報の検索に失敗した場合の処理
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    if ([error code] == 3010) {
        NSLog(@"Push notifications don't work in the simulator!");
    } else {
        NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

// PUSH時に通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    NSLog(@"push!!");
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:@"NOTIF_SWITCH_2"]==NO) {//設定でoffだったら終わり
        return;
    }
    
    if ([userInfo.allKeys containsObject:@"com.nifty.RichUrl"]){
        if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
            [NCMBPush handleRichPush:userInfo];
        }
    } else if ([userInfo.allKeys containsObject:@"com.nifty.PushId"]){
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            [NCMBPush handlePush:userInfo];
        }
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [self.tvc dayChk];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
