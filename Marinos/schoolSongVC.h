//
//  schoolSongVC.h
//  6uiv
//
//  Created by macbook012 on 2015/02/09.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "rocksWebVC.h"

@interface schoolSongVC : UIViewController{}

@property (nonatomic, assign) int schoolNum;
@property (weak, nonatomic) IBOutlet UITextView *songTV;

@property (weak, nonatomic) IBOutlet UIButton *tab1;
@property (weak, nonatomic) IBOutlet UIButton *tab2;
@property (weak, nonatomic) IBOutlet UIButton *tab3;

- (IBAction)tab1Push:(id)sender;
- (IBAction)tab2Push:(id)sender;
- (IBAction)tab3Push:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *linkBtn;
- (IBAction)linkBtnPush:(id)sender;



@end
