//
//  CardVC.m
//  Marinos
//
//  Created by KEi on 13/08/06.
//  Copyright (c) 2013年 Keishi Kuwabara. All rights reserved.
//


#define TAB_SPACE 60
#define PLAY_POINT 10

#import "CardVC.h"

#import "Util.h"
#import "DatabaseUtility.h"
#import "TabButton.h"

@interface CardVC ()

@end

@implementation CardVC
{
    UIButton *getButton;
    
    TabButton *tb;
    UIImageView *pointBase;
    int cardLevel;
    
    NSString *selectedCardIDString;
    NSString *selectedCardCharaName;
    BOOL isNewCard;
    
    UIImageView *background;
    UILabel *playPointLabel;
}

@synthesize myPoint;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self myPointLoad];
    [self setup];

    //タブボタン生成
    self.view.tag = TAB_CARD;
    [self tabButtonsInit];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

#pragma mark - タブボタン生成
-(void)tabButtonsInit
{
    //float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    tb = [[TabButton alloc]initTabButtons:(int)self.view.tag];
    [tb setDelegate:self];
    tb.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-20);
    [self.view addSubview:tb];
}



#pragma mark - タブボタンどれか押した
- (void)anyButtonPushed:(id)sender
{
    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
    if (vc !=nil)
    {//現在の画面とタブが違う場合のみ遷移
        self.navigationController.navigationBarHidden = YES;
        if ([sender tag] == TAB_TOP)
        {//トップ画面のみポップで遷移
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else
        {//それ以外はプッシュで遷移
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:NO];
    [[Util sharedManager] stopSound:SOUND_CARD_LAST];
}

//-(void)backAction{
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    //[ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)]; //広告を表示します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,0)];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)againButtonAndBG
{
    [[Util sharedManager] allSoundStop];
    [[Util sharedManager] playSound:SOUND_CARD_LAST];
#pragma mark もう一度ボタン
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(0, 0, 200, 40);
    moreButton.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height - 80);
    [moreButton setBackgroundImage:[UIImage imageNamed:@"bn_2000.png"] forState:UIControlStateNormal];
    [moreButton setBackgroundImage:[UIImage imageNamed:@"bn_2000.png"] forState:UIControlStateHighlighted];
    [moreButton setTitle:@"もう一度カードを引く" forState:UIControlStateNormal];
    [moreButton setTitle:@"もう一度カードを引く" forState:UIControlStateHighlighted];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    moreButton.titleLabel.font = [ UIFont fontWithName:@"HiraKakuProN-W6" size:16];
    moreButton.tag = TAB_CARD;
    [moreButton addTarget:self action:@selector(anyButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moreButton];

}

//-(void)moreCard:(id)sender{
//    [self setup];
//}

#pragma mark - GetButton Process
-(void)getButtonPushed:(id)sender
{
    //ポイントを最新にする。
    [self myPointLoad];
    
    if (myPoint < PLAY_POINT) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ポイントが足りません"
                                                        message:@"ポイントを貯めて\nカードをゲットしよう！"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        [self goCallenge];
        //サウンド再生
        [[Util sharedManager] allSoundStop];
        [[Util sharedManager] playSound:SOUND_GET_BUTTON];
        [self ballAnimation];
        [self flashAnimation];
    }
}

-(void)goCallenge{
    //ゲットボタンを押したらタブボタンの操作禁止
    tb.hidden = YES;
    getButton.hidden = YES;
    pointBase.hidden = YES;
    _myPointLabel.hidden = YES;
    playPointLabel.hidden = YES;
    
   [TrackingManager sendScreenTracking:@"ガチャ画面"];

    [self performSelector:@selector(cardChoice) withObject:nil afterDelay:0.8];
    
}



#pragma mark - 獲得するカードを決定する
-(void)cardChoice
{
    NSMutableArray *lotteryArray = [[DatabaseUtility sharedManager] getAllCard];
    
    int randomX = arc4random_uniform((int)[lotteryArray count]);
    //int randomX = 0;
    NSLog(@"lotteryArray count:%d",(int)[lotteryArray count]);
    int b = (int)[lotteryArray count];
    if (b != 0){
        NSLog(@"randomX:%d",randomX);
        NSMutableDictionary *selectedCardDic = [lotteryArray objectAtIndex:randomX];
        
        int ownedNum = [selectedCardDic[@"ownedNum"]intValue];
        int card_id = [selectedCardDic[@"card_id"]intValue];
    
        ownedNum++;
        
        NSLog(@"card_id:%d の checkinflag in cardvc ownedNum:%d",card_id,ownedNum);
        [[DatabaseUtility sharedManager] updateGetCard:card_id checkInFlag:ownedNum];
        
        NSString *file_name = selectedCardDic[@"file_name"];
        
        [TrackingManager sendEventTracking:@"cardGet" action:@"tap" label:[NSString stringWithFormat:@"%d",card_id] value:0 screen:@"Card"];
    
        UIImageView *cardImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:file_name]];
        cardImage.frame = CGRectMake(0, 0, 320, 480);
        
        cardImage.backgroundColor = [UIColor whiteColor];
        cardImage.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 - 30);
        [self.view addSubview:cardImage];
        
        [self cardAppearAnime:cardImage];
        //ポイントの減算と保存
        [self myPointUseAndSave:PLAY_POINT];
        
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"選手カードはコンプリート\n済です。"
                                                        message:@"コンプリートおめでとうございます。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
}


-(void)dbLoadError
{
    UIAlertView *alert;
    alert = [[UIAlertView alloc] init];
    alert.title = @"原因不明のエラー";
    alert.message = @"カード情報の取得に失敗しました。\nエラーが繰り返し発生する場合、アプリの再起動および再インストールをお試しください。\n\n上記対応でも問題を解決できない場合、恐れいりますが開発会社にお問い合わせくださいませ。\n申し訳ありません。";
    //[alert setDelegate:self];
    [alert addButtonWithTitle:@"OK"];
    ((UILabel *)[[alert subviews] objectAtIndex:1]).textAlignment = NSTextAlignmentLeft;
    [alert show];
    //[alert setDelegate:nil];
}



#pragma mark - アニメーション
-(void)cardAppearAnime:(UIImageView*)card
{
    //サウンド再生
    [[Util sharedManager] allSoundStop];
    [[Util sharedManager] playSound:SOUND_CARD_APPEAR];
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                         card.alpha = 0.5f;
                       card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);//-30
                     }
                     completion:^(BOOL finished) {
                         [self cardGetAnime:card];
                     }];
}

-(void)flashAnimation
{

    UIImageView *flashView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 360, 370)];
    flashView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    flashView.alpha = 0.1f;
    flashView.image=[UIImage imageNamed:@"intensive_line.png"];
    [flashView sizeToFit];
    flashView.center = CGPointMake(WIN_SIZE.width/2, 250);
    [self.view addSubview:flashView];
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         flashView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              //flashView.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
                                              flashView.center=CGPointMake(WIN_SIZE.width/2, 350);
                                              flashView.alpha = 0.1f;
                                          }
                                          completion:^(BOOL finished) {
                                              flashView.hidden=YES;
                                               //[self cardAppearAnime:card];
                                          }];
                     }];
}


-(void)ballAnimation
{

    UIImageView *ballView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    ballView.image=[UIImage imageNamed:@"ball_icon.png"];
    ballView.center = CGPointMake(WIN_SIZE.width/2, 450);
    [self.view addSubview:ballView];
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         ballView.transform = CGAffineTransformMakeScale(0.6f, 0.6f);
                         ballView.center=CGPointMake(60,60);
                         ballView.alpha = 0.6f;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              ballView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
                                              ballView.center=CGPointMake(WIN_SIZE.width/2, 260);
                                              ballView.alpha = 0.1f;
                                          }
                                          completion:^(BOOL finished) {
                                          }];
                     }];
}


#pragma mark - カード出現アニメーション
-(void)cardGetAnime:(UIImageView*)card
{
    //パーティクルを表示
    [self getParticle];
    
    pointBase.alpha = 1.0f;
    card.transform = CGAffineTransformMakeScale(0.7, 0.7);
    card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2-58);
    card.alpha = 0.0f;
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.alpha = 1.0f;
                        
                     }
                     completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(againButtonAndBG) withObject:nil afterDelay:1.7];//1.7
                         //TODO:もう一度ボタンも表示する
                         //カード表示後はガチャタブ押したらガチャの最初に戻れるようにタグを別物にする。
                         self.view.tag = TAB_MORE;
                         
#pragma mark アニメーション終了後の処理
                         
                    background.image = [UIImage imageNamed:@"afterAnimationBG.png"];
//                         if (WIN_SIZE.height==568){
//                        background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
//                       background.image = [UIImage imageNamed:@"gacha2_bg5.png"];
//                        }
                         
                         _myPointLabel.text =@"";
                         playPointLabel.text =@"";
                         
                         tb.hidden = NO;
                         
                     }];
}


#pragma mark - パーティクルセットアップ
-(void)getParticle
{
    self.emitterLayer = [CAEmitterLayer layer];
    self.emitterLayer.beginTime = 1.0;
    
    self.emitterLayer.renderMode = kCAEmitterLayerAdditive;
    [self.view.layer addSublayer:self.emitterLayer];
    //-----------
    // パーティクル画像
    UIImage *particleImage = [UIImage imageNamed:@"particle.png"];
    
    /*
     birthRate:１秒間に生成するパーティクルの数。
     lifetime:パーティクルが発生してから消えるまでの時間。単位は秒。
     color:パーティクルの色。
     velocity:パーティクルの秒速。
     emissionRange:パーティクルを発生する角度の範囲。単位はラジアン。
     
     */
    // 花火自体の発生源
    CAEmitterCell *baseCell = [CAEmitterCell emitterCell];
    baseCell.emissionLongitude = -M_PI / 2;
    baseCell.emissionLatitude = 0;
    baseCell.emissionRange = M_PI / 5;
    baseCell.lifetime = 1.0;
    baseCell.scale = 0.6;
    baseCell.birthRate = 1*(cardLevel*1.5);//ノーマル１　レア２だったから使ってみた。
    baseCell.velocity = 400;
    baseCell.velocityRange = 50;
    baseCell.yAcceleration = 300;
    baseCell.color = CGColorCreateCopy([UIColor colorWithRed:0.5
                                                       green:0.5
                                                        blue:0.5
                                                       alpha:0.5].CGColor);
    baseCell.redRange   = 0.5;
    baseCell.greenRange = 0.5;
    baseCell.blueRange  = 0.5;
    baseCell.alphaRange = 0.5;
    baseCell.name = @"fireworks";
    
    // 破裂後に飛散するパーティクルの発生源
    CAEmitterCell *sparkCell = [CAEmitterCell emitterCell];
    sparkCell.contents = (__bridge id)particleImage.CGImage;
    sparkCell.emissionRange = 2 * M_PI;
    sparkCell.birthRate = 600;
    sparkCell.scale = 0.4;
    sparkCell.velocity = 300;
    sparkCell.lifetime = 0.4;
    sparkCell.yAcceleration = 80;
    sparkCell.beginTime = 0.01;//risingCell.lifetime
    sparkCell.duration = 0.1;
    sparkCell.alphaSpeed = -0.2;
    sparkCell.scaleSpeed = -0.6;
    
    // baseCellからrisingCellとsparkCellを発生させる
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {  //iOS以上の場合
        
        // 上昇中のパーティクルの発生源
        CAEmitterCell *risingCell = [CAEmitterCell emitterCell];
        risingCell.contents = (__bridge id)particleImage.CGImage;
        risingCell.emissionLongitude = (4 * M_PI) / 2;
        risingCell.emissionRange = M_PI / 7;
        risingCell.scale = 0.4;
        risingCell.velocity = 100;
        risingCell.birthRate = 50;
        risingCell.lifetime = 1.5;
        risingCell.yAcceleration = 350;
        risingCell.alphaSpeed = -0.7;
        risingCell.scaleSpeed = -0.1;
        risingCell.scaleRange = 0.1;
        risingCell.beginTime = 0.01;
        risingCell.duration = 0.7;
        
        baseCell.emitterCells = [NSArray arrayWithObjects:risingCell, nil];
        risingCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
    }
    // baseCellはemitterLayerから発生させる
    else
    {
        baseCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
    }
    self.emitterLayer.emitterCells = [NSArray arrayWithObjects:baseCell, nil];
    
    //-----------
    CGSize size = self.view.bounds.size;
    self.emitterLayer.emitterPosition = CGPointMake(size.width / 2, size.height * 4 / 5);
}

#pragma mark - マイポイント読込・記録・使用
-(void)myPointLoad
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
    _myPointLabel.text = [NSString stringWithFormat:@"残り%dポイント",myPoint];
}



-(void)myPointSave
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setInteger:myPoint forKey:@"KEY_MYPOINT"];
    _myPointLabel.text = [NSString stringWithFormat:@"残り%dポイント",myPoint];
}

-(void)myPointUseAndSave:(int)usePoint
{
    myPoint = myPoint - usePoint;
    [self myPointSave];
}

#pragma mark - Initialize
-(void)setup
{
    [[Util sharedManager] stopSound:SOUND_CARD_LAST];
    
    [self showImobile];
     tb.hidden = NO;
    
    background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gatya_bk.png"]];
    background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
    background.image = [UIImage imageNamed:@"gatya_bk.png"];
    [self.view addSubview:background];
        
    getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    getButton.frame = CGRectMake(0, 0, 160, 45);
    if (SCREEN_BOUNDS.size.height == 568) {
        getButton.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height - TAB_SPACE - 60);
    }else{
        getButton.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height - TAB_SPACE - 20);
    }
    [getButton setBackgroundImage:[UIImage imageNamed:@"bn_red.png"] forState:UIControlStateNormal];
    [getButton setBackgroundImage:[UIImage imageNamed:@"bn_red.png"] forState:UIControlStateHighlighted];
    
    [getButton setTitle:@"カードを引く" forState:UIControlStateNormal];
    [getButton setTitle:@"カードを引く" forState:UIControlStateHighlighted];
    getButton.titleLabel.font = [ UIFont fontWithName:@"HiraKakuProN-W6" size:23];
    
    [getButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [getButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    [getButton addTarget:self action:@selector(getButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getButton];
    
    pointBase = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pointBase.png"]];
    pointBase.frame = CGRectMake(0, 0,278,84);
    pointBase.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2+45);
    [self.view addSubview:pointBase];
    
    _myPointLabel = [[UILabel alloc]init];
    _myPointLabel.frame=CGRectMake(0, 0, 250, 50);
    _myPointLabel.center = CGPointMake(pointBase.frame.size.width/2, 22);
    _myPointLabel.text = [NSString stringWithFormat:@"残り%dポイント",myPoint];
    _myPointLabel.textColor = [UIColor whiteColor];
    _myPointLabel.backgroundColor = [UIColor clearColor];
    _myPointLabel.textAlignment = NSTextAlignmentCenter;
    _myPointLabel.font = [UIFont fontWithName:@"HiraKakuProN-W6" size:27];
    _myPointLabel.adjustsFontSizeToFitWidth = YES;
    [pointBase addSubview:_myPointLabel];
    
    playPointLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 50)];
    playPointLabel.center = CGPointMake(_myPointLabel.center.x, _myPointLabel.center.y + 101- TAB_SPACE);
    playPointLabel.text = [NSString stringWithFormat:@"1回 %dポイント",PLAY_POINT];
    playPointLabel.textColor = [UIColor whiteColor];
    playPointLabel.backgroundColor = [UIColor clearColor];
    playPointLabel.textAlignment = NSTextAlignmentCenter;
    playPointLabel.font = [UIFont fontWithName:@"HiraKakuProN-W6" size:28];
    [pointBase addSubview:playPointLabel];
    
    
}



#pragma mark - タッチ操作
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    static int count = 0;
    CGPoint p = [[touches anyObject] locationInView:self.view];
    if (p.y < 100 && p.x > 250)
    {
        count++;
        if (count > 30)
        {
            count = 0;
            [self myPointUseAndSave:-300];
        }
    }
    else
    {
        count = 0;
    }
}



#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
