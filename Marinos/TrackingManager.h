//
//  TrackingManager.h
//  FMarinosCard
//
//  Created by eagle014 on 2014/12/01.
//  Copyright (c) 2014年 菊地 拓也. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackingManager : NSObject

// スクリーン名計測用メソッド
+ (void)sendScreenTracking:(NSString *)screenName;

// イベント計測用メソッド
+ (void)sendEventTracking:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value screen:(NSString *)screen;

@end
