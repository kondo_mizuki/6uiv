//
//  checkInImageVC.h
//  6uiv
//
//  Created by macbook012 on 2015/02/27.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util.h"
#import "DatabaseUtility.h"
#import "DetailViewController.h"
#import "CameraViewController.h"
#import "LocationUtility.h"

@interface checkInImageVC : UIViewController<UIWebViewDelegate>

@property(nonatomic, assign)int place_id;
@property(nonatomic, assign)double latitude;
@property(nonatomic, assign)double longtitude;

@property (weak, nonatomic) IBOutlet UILabel *getLbl;
@property (weak, nonatomic) IBOutlet UILabel *checkInLbl;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn;
@property (weak, nonatomic) IBOutlet UIWebView *trainWebView;


- (IBAction)photoBtnPush:(id)sender;

@end
