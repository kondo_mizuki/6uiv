//
//  AlbumCell.m
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/26.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "AlbumCell.h"
#import "DatabaseUtility.h"

@implementation AlbumCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) displayCard:(int)cardID possession:(int)cardPossession
{
    
    //NSString *selectedCardIDString = [NSString stringWithFormat:@"No.%03d",cardID];
    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:cardID];
    NSString *file_name;
    UIImage *img;
    UIImageView *cardImage = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, 72, 111)];//78/117 349/515
    //cardImage.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:cardImage];
    if (selectedCardDic == nil)
    {
        NSLog(@"カードDICが空っぽ　%d番",cardID);
    } else if (selectedCardDic != nil)
    {
        file_name = selectedCardDic[@"file_name"];
        //サムネイル用にファイル名変更(あたまにt_を追加)
        //file_name = [@"thumb_" stringByAppendingString:file_name];
        
        img = [UIImage imageNamed:file_name];
        
#pragma mark デバッグ用本番は残す
        //********************************************************
        if (cardPossession  == 0)
        {
            //カードを保有していないときの処理
            NSString *fileLocation;
            fileLocation = [[NSBundle mainBundle]pathForResource:@"card_ura" ofType:@"png"];
            
            NSData *imageData = [NSData dataWithContentsOfFile:fileLocation];
            img = [UIImage imageWithData:imageData];
        }
        
        //********************************************************
        cardImage.image = img;
        //複数枚カードを所持している場合の処理
        if (cardPossession > 1)
        {
            UIImage *overLapImg = [UIImage imageNamed:[NSString stringWithFormat:@"overlap.png"]];
            UIImageView *overLapIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 119)];
            overLapIV.image = overLapImg;
            [self.contentView addSubview:overLapIV];
            //カードを前面にもってくる。
            [self.contentView bringSubviewToFront:cardImage];
            //枚数表示
            UIImage *batchImg = [UIImage imageNamed:[NSString stringWithFormat:@"batch.png"]];
            UIImageView *batchIV = [[UIImageView alloc] initWithFrame:CGRectMake(58, 0, 27, 27)];
            batchIV.image = batchImg;
            [self.contentView addSubview:batchIV];
            int cardCount = cardPossession;
            
            UILabel *countLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 1, 24, 20)];
            countLabel.text = [NSString stringWithFormat:@"%d",cardCount];
            countLabel.textColor = [UIColor whiteColor];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.textAlignment = NSTextAlignmentCenter;
            countLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:16];
            countLabel.adjustsFontSizeToFitWidth = YES;
            [batchIV addSubview:countLabel];
        }
    }
}
@end
