//
//  AlbumVC.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumDetailViewController.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "Define.h"

@interface AlbumVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,UIActionSheetDelegate>

@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionView *collectionView;

@property (strong, nonatomic) AlbumDetailViewController *detailViewController;

@property (strong, nonatomic) NSMutableArray *possessionArray;
@property (strong, nonatomic) NSMutableArray *meijiPossessionArray;
@property (strong, nonatomic) NSMutableArray *wasedaPossessionArray;
@property (strong, nonatomic) NSMutableArray *rikkyoPossessionArray;
@property (strong, nonatomic) NSMutableArray *keioPossessionArray;
@property (strong, nonatomic) NSMutableArray *hoseiPossessionArray;
@property (strong, nonatomic) NSMutableArray *tokyoPossessionArray;

@property (strong, nonatomic) NSMutableArray *cardIdArray;
@property (strong, nonatomic) NSMutableArray *allCardIdArray;

@end
