//
//  LocationUtility.h
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationUtility : NSObject
<CLLocationManagerDelegate>

@property(nonatomic, assign) double latitude;
@property(nonatomic, assign) double longtitude;
@property(nonatomic, assign) BOOL isLocationEnabled;

+ (LocationUtility*) sharedManager;
- (void) getLocation;
- (void) onPauseLocation;

@end
