//
//  TopVC.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/06.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "TopVC.h"

#import "LocationUtility.h"
#import "Util.h"
#import "Define.h"
#import "UIApplication+UIID.h"
#import "TabButton.h"
#import "MapVC.h"
//#import "CardVC.h"
//#import "AlbumVC.h"


@interface TopVC ()
{
    TabButton *tb;
    UIView *grayView;
    UIImageView *tutorialLeftArrow;
    UIImageView *tutorialRightArrow;
    UIImageView *tutorialSignaldots;

    UIToolbar *myToolbar;
    UIBarButtonItem *backButton;
    UIBarButtonItem *nextButton;
    UIBarButtonItem *refreshButton;
    UIImageView *waitingView;
    UIActivityIndicatorView *indicator;
    int logInCategory;
}
@property (nonatomic, retain) NSMutableArray *aryCellIdentifier;
@end


@implementation TopVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    //日付をチェック
    [self dayChk];
    
    [self showImobile];
    
    //タブボタン生成
    self.view.tag = TAB_TOP;
    [self tabButtonsInit];
    
    
    //[self toolbarSetup];
    [_topWebView setScalesPageToFit:YES];
    
    //コンテンツの存在確認後のセルの並びを保持するもの
	self.aryCellIdentifier = [NSMutableArray arrayWithCapacity:0];
    
    [self.aryCellIdentifier addObject:@"HomeTitleCell"];
    [self.aryCellIdentifier addObject:@"HomeSecondCell"];
    [self.aryCellIdentifier addObject:@"HomeBlogCell"];
    [self.aryCellIdentifier addObject:@"HomeDummyCell"];
    
    
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"];
    [ImobileSdkAds startBySpotID:@"379689"];
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-TABBUTTON_HEIGHT-50)];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationItem setHidesBackButton:NO animated:NO];
    
    [self.tableView reloadData];
}

//-(void)reloadDataWithComplete:(void(^)(void))waitBlock {
//    [self.tableView reloadData];
//    if(waitBlock){
//        waitBlock();
//    }
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.aryCellIdentifier.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	// 高さはアプリ出荷時には確定している性質のものなので、動的に算出せずに直書きとする。
	NSDictionary *dicHeight = @{@"HomeTitleCell":@198,@"HomeSecondCell":@152,@"HomeBlogCell":@271
                                ,@"HomeDummyCell":@100};
	
	NSString *CellIdentifier = [self.aryCellIdentifier objectAtIndex:indexPath.row];
	return [dicHeight[CellIdentifier] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = [self.aryCellIdentifier objectAtIndex:indexPath.row];
    if ([CellIdentifier isEqualToString:@"HomeSecondCell"]) {
        HomeResultCell *resultCell = (HomeResultCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        resultCell.delegate=self;
        [resultCell refreshWV];
        return resultCell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor lightGrayColor];
}



#pragma mark - タブボタン生成
-(void)tabButtonsInit
{
    tb = [[TabButton alloc]initTabButtons:(int)self.view.tag];
    [tb setDelegate:self];
    tb.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-20);
    [self.view addSubview:tb];
}



#pragma mark - タブボタンどれか押した
- (void)anyButtonPushed:(id)sender
{
    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
    if (vc !=nil)
    {//現在の画面とタブが違う場合のみ遷移
        self.navigationController.navigationBarHidden = YES;
        if ([sender tag] == TAB_TOP)
        {//トップ画面のみポップで遷移
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else
        {//それ以外はプッシュで遷移
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}


-(void)dayChk
{
    int myPoint;
    int year;
    int thisMonth;
    int today;
    int beforeMonth;
    int beforeDay;
    //初期値設定
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@"99" forKey:@"KEY_MONTH"];
    [dic setObject:@"99" forKey:@"KEY_DAY"];
    [dic setObject:@"0" forKey:@"KEY_MYPOINT"];
    [ud registerDefaults:dic];
    [ud synchronize];
    
    //保存されたマイポイントと日付をロード
    myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
    beforeMonth = (int)[ud integerForKey:@"KEY_MONTH"];
    beforeDay = (int)[ud integerForKey:@"KEY_DAY"];
    NSLog(@"保存されていた日付は%d月%d日",beforeMonth,beforeDay);

    //日付を取得
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit |
                                   NSMonthCalendarUnit  |
                                   NSDayCalendarUnit    |
                                   NSWeekdayCalendarUnit|
                                   NSHourCalendarUnit   |
                                   NSMinuteCalendarUnit |
                                   NSSecondCalendarUnit
                                              fromDate:date];

    //今日の日付を代入
    year = (int)dateComps.year;
    thisMonth = (int)dateComps.month;
    today = (int)dateComps.day;
   
    NSLog(@"今日は%d月%d日",thisMonth,today);

    //--------------------------
    if (today == beforeDay && thisMonth == beforeMonth)
    {//今日の月日と保存されている月日が一緒だったらリターン
        return;
    }
    //初回　チュートリアルを表示＋初回ボーナス付与
    if (beforeDay == 99)
    {
        //[self performSelector:@selector(tutorialShow) withObject:nil afterDelay:0.0f];
        [self firstloginAlert];
        myPoint = myPoint + LOGIN_FIRST;
        
        [ud setBool:NO forKey:@"CHARGES_FLAG"];//課金のデフォルト設定
    }else{
    //通常のログインボーナス
    myPoint = myPoint + LOG_IN_BONUS_NORMAL;
    [self normalLoginAlert];
    }

    beforeMonth = thisMonth;
    beforeDay=today;
    [ud setInteger:myPoint forKey:@"KEY_MYPOINT"];
    [ud setInteger:year forKey:@"KEY_YEAR"];
    [ud setInteger:beforeMonth forKey:@"KEY_MONTH"];
    [ud setInteger:beforeDay forKey:@"KEY_DAY"];
    [ud synchronize];
    
}



#pragma mark - 初回ログインボーナスのアラートビュー
-(void)firstloginAlert
{
    UIAlertView *alert;
    alert = [[UIAlertView alloc] init];
    
    alert.message = [NSString stringWithFormat:@"おめでとう！\n初回ログインボーナスで%dポイントゲット！！！\n毎日ログインしてポイントを貯めて、選手カードをたくさんゲットしよう！",LOGIN_FIRST];
    
    [alert setDelegate:nil];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

#pragma mark - 通常ログインボーナスのアラートビュー
-(void)normalLoginAlert
{
    UIAlertView *alert;
    alert = [[UIAlertView alloc] init];
    
    alert.message = [NSString stringWithFormat:@"おめでとう！\n1日１回ログインボーナスで%dポイントゲット！！！\n毎日ログインしてポイントを貯めて、選手カードをたくさんゲットしよう！",LOG_IN_BONUS_NORMAL];
    
    [alert setDelegate:nil];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    int number = (_scrollView.contentOffset.x/320)+1;
    tutorialSignaldots.image = [UIImage imageNamed:[NSString stringWithFormat:@"signaldots%d",number]];
    //左矢印管理
    if (_scrollView.contentOffset.x == 0)
    {
        tutorialLeftArrow.image = [UIImage imageNamed:@"left-arrows"];
    }
    else
    {
        tutorialLeftArrow.image = [UIImage imageNamed:@"left-arrows-active"];
    }
    //右矢印管理
    if (_scrollView.contentOffset.x == 320*3) //320 * (i-1)　　 i:チュートリアルの枚数
    {
        tutorialRightArrow.image = [UIImage imageNamed:@"right-arrows"];
        tutorialSignaldots.hidden = YES;
    }
    else
    {
        tutorialRightArrow.image = [UIImage imageNamed:@"right-arrows-active"];
        tutorialSignaldots.hidden = NO;
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dealloc
{
    //_topWebView = nil;
    
}
- (IBAction)meijiPush:(id)sender {
    [self moveSchoolSongVC:1];
}

- (IBAction)wasedaPush:(id)sender {
    [self moveSchoolSongVC:2];
}

- (IBAction)rikkyoPush:(id)sender {
    [self moveSchoolSongVC:3];
}

- (IBAction)keioPush:(id)sender {
    [self moveSchoolSongVC:4];
}

- (IBAction)hoseiPush:(id)sender {
    [self moveSchoolSongVC:5];
}

- (IBAction)tokyoPush:(id)sender {
    [self moveSchoolSongVC:6];
}

#pragma mark　delegate２つ
- (void)gameResultPush{
    [TrackingManager sendEventTracking:@"ToGameResult" action:@"tap" label:@"" value:0 screen:@"top"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    gameResultWebVC *grVC = (gameResultWebVC *)[storyboard instantiateViewControllerWithIdentifier:@"grWebVC"];
    [self.navigationController pushViewController:grVC animated:YES];
}

-(void)showChargeAlert{

    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:@"こちらをご覧頂くにはプレミアムエディションを購入して頂く必要がございます。\n購入の上、再度こちらをタップして下さい。"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"はい"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
         [self presentViewController:alert animated:YES completion:nil];
        } else

    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"こちらをご覧頂くにはプレミアムエディションを購入して頂く必要がございます。\n購入の上、再度こちらをタップして下さい。" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"はい", nil];
        [alert show];
    }
}

- (IBAction)ticketBtnPush:(id)sender {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL chargesFlag = [ud boolForKey:@"CHARGES_FLAG"];
    //chargesFlag = NO;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    if (chargesFlag) {
        PremiumVC *prvc = (PremiumVC *)[storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
        [self.navigationController pushViewController:prvc animated:YES];
    }else{
    ticketVC *tkVC = (ticketVC *)[storyboard instantiateViewControllerWithIdentifier:@"ticketVC"];
    [self.navigationController pushViewController:tkVC animated:YES];
        }
}

- (IBAction)meijiBlogPush:(id)sender {
    [self goBlogURL:@"meiji"];
}

- (IBAction)wasedaBlogPush:(id)sender {
    [self goBlogURL:@"waseda"];
}

- (IBAction)rikkyoBlogPush:(id)sender {
    [self goBlogURL:@"rikkyo"];
}

- (IBAction)keioBlogPush:(id)sender {
    [self goBlogURL:@"keio"];
}

- (IBAction)hoseiBlogPush:(id)sender {
    [self goBlogURL:@"hosei"];
}

- (IBAction)tokyoBlogPush:(id)sender {
    [self goBlogURL:@"tokyo"];
}



-(void)goBlogURL:(NSString*)univStr{
    [TrackingManager sendEventTracking:@"ToBlog" action:@"tap" label:univStr value:0 screen:@"top"];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://tokyorocks2015.wondernotes.jp/%@/",univStr];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    rocksWebVC *grVC = (rocksWebVC *)[storyboard instantiateViewControllerWithIdentifier:@"rocksWebVC"];
    grVC.urlStr = urlStr;
    grVC.title = @"ブログリーグ";
    [self.navigationController pushViewController:grVC animated:YES];
}

-(void)moveSchoolSongVC:(int)num{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    schoolSongVC *vc = (schoolSongVC *)[storyboard instantiateViewControllerWithIdentifier:@"schoolSongVC"];
    vc.schoolNum = num;
     [self.navigationController pushViewController:vc animated:NO];
}
@end
