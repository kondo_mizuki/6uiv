//
//  Util.m
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/29.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "Util.h"
#import "UIApplication+UIID.h"
#import "ConnectionHandler.h"
#import "FMDatabase.h"
#import "Define.h"
#import "DatabaseUtility.h"
#import "TopVC.h"
#import "MapVC.h"
#import "CardVC.h"
#import "AlbumVC.h"
#import "cardCouponSelectVC.h"

@interface Util()

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) FMDatabase *db;

@end

@implementation Util

static Util* sharedUtil = nil;

+ (Util*)sharedManager {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}



- (id)changeVC:(int)buttonTag vcTag:(int)vcTag
{
    
    if (buttonTag != vcTag)
    {
        if (vcTag != TAB_MORE) {//カード画面のもう一度ボタン以外
            //サウンド再生
            [self playSound:SOUND_TAB];
        }


        id vc;
        if (buttonTag == TAB_TOP)
        {
            vc = [[TopVC alloc]init];
        }
        else if(buttonTag == TAB_MAP)
        {
            vc = [[MapVC alloc] init];
            
        }
        else if (buttonTag == TAB_CARD)
        {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//            
//            vc = (cardCouponSelectVC*)[storyboard instantiateViewControllerWithIdentifier:@"ccsVC"];
            
            vc = [[CardVC alloc] init];
            
        }
        else if(buttonTag == TAB_ALBUM)
        {
            vc = [[AlbumVC alloc] init];
        }
        return vc;
    }
    else return nil;
}



-(void)prepareToPlaySounds
{
    // ファイルのパスを作成します。
    NSString *path = [[NSBundle mainBundle] pathForResource:@"choice" ofType:@"mp3"];
    // ファイルのパスを NSURL へ変換します。
    NSURL* url = [NSURL fileURLWithPath:path];
    // ファイルを読み込んで、プレイヤーを作成します。
    _buttonSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_buttonSE prepareToPlay];
    _buttonSE.volume = 0.3;
    
    path = [[NSBundle mainBundle] pathForResource:@"bat_first" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardButtonSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardButtonSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"bat_second" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardButtonSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardButtonSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"ball" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardAppearSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardAppearSE prepareToPlay];
    _cardAppearSE.volume = 0.8;
    
    path = [[NSBundle mainBundle] pathForResource:@"gacha_base" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardBaseSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardBaseSE prepareToPlay];
    _cardBaseSE.volume = 0.5;
    
    path = [[NSBundle mainBundle] pathForResource:@"cardZoomUpSoundRare" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardZoomUpSE_Rare = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardZoomUpSE_Rare prepareToPlay];
    _cardZoomUpSE_Rare.volume = 0.5;
    
    path = [[NSBundle mainBundle] pathForResource:@"cheer_second" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _getCardLast = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_getCardLast prepareToPlay];
    _getCardLast.volume = 0.6;
    
    path = [[NSBundle mainBundle] pathForResource:@"shu" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _albumSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_albumSE prepareToPlay];
    _albumSE.volume = 0.5;
    
    path = [[NSBundle mainBundle] pathForResource:@"kiiiin" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _kiiinSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_kiiinSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"cursor25" ofType:@"wav"];
    url = [NSURL fileURLWithPath:path];
    _chuiuSE= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_chuiuSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"pyoro52" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _pyoroSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_pyoroSE prepareToPlay];
    _pyoroSE.volume = 0.3;
    
    path = [[NSBundle mainBundle] pathForResource:@"Sys03-pop up" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _pironSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_pironSE prepareToPlay];
}



-(void)playSound:(int)soundID
{
    if (soundID == SOUND_TAB)
    {
        if (_buttonSE.isPlaying)
        {
            [_buttonSE stop];
            _buttonSE.currentTime = 0.0;
        }
        [_buttonSE play];
    }
    else if (soundID == SOUND_GET_BUTTON)
    {
        //_cardButtonSE.currentTime = 0.0;
        [_cardButtonSE play];
        [_cardButtonSE2 play];//バットの音２つ重ね
    }
    else if (soundID == SOUND_CARD_APPEAR)
    {
        [_cardAppearSE play];
    }
    else if (soundID == SOUND_CARD_BASE)
    {
        [_cardBaseSE play];
    }
    else if (soundID == SOUND_CARD_ZOOM_RARE)
    {
        [_cardZoomUpSE_Rare play];
    }
    else if (soundID == SOUND_CARD_LAST)
    {
        [_getCardLast play];
    }
    else if (soundID == SOUND_ALBUM)
    {
        [_albumSE play];
    }
    else if (soundID == SOUND_KIIIN)
    {
        [_kiiinSE play];
    }
    else if (soundID == SOUND_CHUIU)
    {
        [_chuiuSE play];
    }
    else if (soundID == SOUND_PYORO)
    {
        [_pyoroSE play];
    }
    else if (soundID == SOUND_PIRON)
    {
        [_pironSE play];
    }
}

-(void)allSoundStop{
    
    [_buttonSE stop];
    [_cardButtonSE stop];
    [_cardButtonSE2 stop];
    [_cardAppearSE stop];
    [_cardBaseSE stop];
    [_cardZoomUpSE_Rare stop];
    [_getCardLast stop];
    
    [_albumSE stop];
    [_kiiinSE stop];
    [_chuiuSE stop];
    [_pyoroSE stop];
    [_pironSE stop];
    
    _cardAppearSE.currentTime = 0.0;
    _cardButtonSE.currentTime= 0.0;
    _cardButtonSE2.currentTime= 0.0;
    _cardAppearSE.currentTime= 0.0;
    _getCardLast.currentTime= 0.0;

}

-(void)stopSound:(int)soundID
{
    if (soundID == SOUND_TAB)
    {
        [_buttonSE stop];
    }
    else if (soundID == SOUND_GET_BUTTON)
    {
        //_cardButtonSE.currentTime = 0.0;
        [_cardButtonSE stop];
        [_cardButtonSE2 stop];//バットの音２つ重ね
    }
    else if (soundID == SOUND_CARD_APPEAR)
    {
        [_cardAppearSE stop];
    }
    else if (soundID == SOUND_CARD_BASE)
    {
        [_cardBaseSE stop];
    }
    else if (soundID == SOUND_CARD_ZOOM_RARE)
    {
        [_cardZoomUpSE_Rare stop];
    }
    else if (soundID == SOUND_CARD_LAST)
    {
        [_getCardLast stop];
    }
    else if (soundID == SOUND_ALBUM)
    {
        [_albumSE stop];
    }
    else if (soundID == SOUND_KIIIN)
    {
        [_kiiinSE stop];
    }
    else if (soundID == SOUND_CHUIU)
    {
        [_chuiuSE stop];
    }
    else if (soundID == SOUND_PYORO)
    {
        [_pyoroSE stop];
    }
    else if (soundID == SOUND_PIRON)
    {
        [_pironSE stop];
    }
}




- (id)copyWithZone:(NSZone*)zone {
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

#pragma mark - データベース操作のためのopen,closeメソッド
- (BOOL)openDatabase {
    //DBファイルへのパスを取得
    //パスは~/Documents/配下に格納される。
    NSString *dbPath = nil;
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得データ数を確認
    if ([documentsPath count] >= 1) {
        //固定で0番目を取得でOK
        dbPath = [documentsPath objectAtIndex:0];
        //パスの最後にファイル名をアペンドし、DBファイルへのフルパスを生成。
        dbPath = [dbPath stringByAppendingPathComponent:DB_FILE];
    } else {
        //error
        NSLog(@"search Document path error. database file open error.");
        return false;
    }
    
    //DBファイルがDocument配下に存在するか判定
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dbPath]) {
        //存在しない
        //デフォルトのDBファイルをコピー(初回のみ)
        //ファイルはアプリケーションディレクトリ配下に格納されている。
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *orgPath = [bundle bundlePath];
        //初期ファイルのパス。(~/XXX.app/sample.db)
        orgPath = [orgPath stringByAppendingPathComponent:DB_FILE];
        
        //デフォルトのDBファイルをDocument配下へコピー
        if (![fileManager copyItemAtPath:orgPath toPath:dbPath error:nil]) {
            //error
            NSLog(@"db file copy error. : %@ to %@.", orgPath, dbPath);
            return false;
        }
    }
    
    //open database with FMDB.
    self.db = [FMDatabase databaseWithPath:dbPath];
    return [self.db open];
}

- (void)closeDatabase {
    if (self.db) {
        [self.db close];
    }
}

#pragma mark - データベース操作
- (NSMutableArray*) showTables {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sqlite_master WHERE type='table'"];
        while ([rs next]) {
            [array addObject:[rs stringForColumn:@"name"]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (int)getPhotosSum {
    if ([self openDatabase]) {
        [self.db setShouldCacheStatements:YES];
        
        int photosCount = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM historys"];
        while ([rs next]) {
            //            //ここでデータを展開
            photosCount = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return photosCount;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (NSMutableArray*)getAlbumCells {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPhotos {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *identification = [rs stringForColumn:@"id"];
            if (identification != nil) {
                [dic setObject:identification forKey:@"identification"];
            }
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (UIImage*)getPhotoFromIdentification:(NSString*)identification {
    if ([self openDatabase]) {
        FMResultSet *rs = [self.db executeQuery:@"SELECT thumbnail FROM historys WHERE id=?", identification];
        UIImage *thumbnail = nil;
        while ([rs next]) {
            thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
        }
        [rs close];
        [self closeDatabase];
        return thumbnail;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (BOOL)saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        NSData *imagedata = [[NSData alloc] initWithData:UIImageJPEGRepresentation(thumbnail, 0.2f)];
        
        NSDate *now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *nowStr = [formatter stringFromDate:now];
        
        [self.db executeUpdate:@"INSERT INTO historys(thumbnail, url, record_date, latitude, longtitude, title, body, datetime, like_count, tweet_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", imagedata, url, recordDate, [NSNumber numberWithDouble:latitude], [NSNumber numberWithDouble:longtitude], title, detail, nowStr, [NSNumber numberWithInt:INITIAL_LIKE_COUNT], [NSNumber numberWithInt:INITIAL_TWEET_COUNT]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)saveCheckin:(double)latitude with:(double)longtitude {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        [self.db executeUpdate:@"INSERT INTO checkins(latitude, longtitude) VALUES(?, ?)",[NSNumber numberWithDouble:[self changeDigitTo3:latitude]], [NSNumber numberWithDouble:[self changeDigitTo3:longtitude]]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

// テスト用メソッド
- (BOOL)showAllRows {
    if ([self openDatabase]) {
        //        [self.db setShouldCacheStatements:YES];
        
        int count = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM checkins"];
        while ([rs next]) {
            //            //ここでデータを展開
            NSLog(@"%d, %f, %f", [rs intForColumnIndex:0], [rs doubleForColumnIndex:1], [rs doubleForColumnIndex:2]);
        }
        [rs close];
        [self closeDatabase];
        return count;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (NSMutableArray*)getCheckIns {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT latitude, longtitude FROM checkins"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (int)hasCheckin:(double)latitude with:(double)longtitude {
    if ([self openDatabase]) {
        //        [self.db setShouldCacheStatements:YES];
        
        int count = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM checkins WHERE latitude=? AND longtitude=?", [NSNumber numberWithDouble:[self changeDigitTo3:latitude]], [NSNumber numberWithDouble:[self changeDigitTo3:longtitude]]];
        while ([rs next]) {
            //            //ここでデータを展開
            count = [rs intForColumn:@"cnt"];
        }
        
        [rs close];
        [self closeDatabase];
        return count;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getCheckinSum {
    if ([self openDatabase]) {
        [self.db setShouldCacheStatements:YES];
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM checkins"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (void)checkinCheck:(double)latitude with:(double)longtitude {
    if ([self hasCheckin:latitude with:longtitude] == 0) {
        [self saveCheckin:latitude with:longtitude];
    }
}

- (double)changeDigitTo3:(double)value {
    return floor(value*10*10*10)/1000;
}



-(float)getSatusbarHeight
{
    float sbh = 20.0f;
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
    {  //iOS7未満の場合
        sbh = 0.0f;
    }
    return sbh;
}


@end

@implementation UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
{
    UIGraphicsBeginImageContext(size);
    // draw scaled image into thumbnail context
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
        NSLog(@"could not scale image");
    return newThumbnail;
}

@end