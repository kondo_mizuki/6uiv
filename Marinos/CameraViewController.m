//
//  CameraViewController.m
//  TimeCapsule
//
//  Created by kyo on 2013/02/19.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "CameraViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ConnectionHandler.h"
#import "Constants.h"
#import "UIPlaceHolderTextView.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "Util.h"
#import "LocationUtility.h"

@interface CameraViewController ()

@property(nonatomic, strong) UITextField *titletf;
@property(nonatomic, strong) UITextField *detailtf;
@property(nonatomic, strong) NSString *titleStr;
@property(nonatomic, strong) NSString *detailStr;
@property(nonatomic, assign) BOOL isCameraRoll;
@property(nonatomic, strong) NSString *img_path;
@property(nonatomic, strong) NSString *html_path;
@property(nonatomic, strong) NSString *post_id;
@property(nonatomic, strong) ConnectionHandler *connectionHandler;
@property(nonatomic, strong) NSTimer *timeout;
@property(nonatomic, strong) NSURLConnection *tmpImageConnection;
@property(nonatomic, assign) CallingS3APIStatus s3status;
@property(nonatomic, strong) NSURLConnection *commitImageConnection;
@property(nonatomic, assign) double postLatitude;
@property(nonatomic, assign) double postLongtitude;

@end

@implementation CameraViewController

- (void) dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)timeout:(NSTimer *)timer
{
    if (self.tmpImageConnection) {
        [self.tmpImageConnection cancel];
        [self.timeout invalidate];
        self.timeout = nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please retry!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [SVProgressHUD dismiss];
        [alert show];
    } else if (self.s3status == calling) {
        self.s3status = callFailure;
        [self.timeout invalidate];
        self.timeout = nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please retry!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [SVProgressHUD dismiss];
        [alert show];
    } else if (self.commitImageConnection) {
        [self.commitImageConnection cancel];
        [self.timeout invalidate];
        self.timeout = nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please retry!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [SVProgressHUD dismiss];
        [alert show];
    } else {
        [self.timeout invalidate];
        self.timeout = nil;
    }
}

#pragma mark - Herokuサーバに送信
- (BOOL)sendPhotoToServer {
    [SVProgressHUD showWithStatus:@"Creating URL..." maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    self.receivedData = [[NSMutableData alloc] init];
    
    self.s3 = nil;
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = @"https://s3-ap-northeast-1.amazonaws.com";
    
    // 1分待っても投稿が終わらない場合は切る
    //きくちくん修正　９０秒にしてみる！
    self.timeout = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(timeout:) userInfo:nil repeats:NO];
    
    NSString *uuid;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    //    if ([NSStringFromClass([self class]) isEqualToString:@"ActivationViewController"]) {
    //        uuid = [[Util sharedManager] createUIID];
    //
    //        [ud setObject:uuid forKey:@"uuid"];
    //        [ud setInteger:0 forKey:@"first_shot"];
    //        [ud synchronize];
    //
    //        NSLog(@"サーバにUUID送るよ！");
    //
    //        [[Util sharedManager] sendUiidToServer:uuid];
    //    } else {
    //        if ([ud stringForKey:@"uuid"] != nil) {
    //            uuid = [ud stringForKey:@"uuid"];
    //        } else {
    uuid = @"NotFoundUUID";
    //        }
    //        NSLog(@"CameraVCだよ！");
    //    }
    
    // 送信したいURLを作成する
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat: @"%@/%@", [Util sharedManager].heroku, @"/tmpimage"]];
    NSLog(@"tmp:%@",url);
    
    // Mutableなインスタンスを作成し、インスタンスの内容を変更できるようにする
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    // MethodにPOSTを指定する。
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *settedDate = [formatter dateFromString:self.dateTextField.text];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowString = [formatter stringFromDate:settedDate];
    NSLog(@"nowString is %@", nowString);
    
    //本文テキストフィールド
    UITextField *detailtf = (UITextField*)[self.view viewWithTag:DETAIL_TEXTFIELD_TAG];
    self.detailStr = detailtf.text;
    if (self.detailStr == nil || [self.detailStr isEqualToString:@""] || [self.detailStr isEqualToString:@" "]) {
        self.detailStr = @"";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          uuid, @"uuid",
                          [NSDictionary dictionaryWithObjectsAndKeys:
                           // 緯度と経度は小数点第三位で渡す
                           self.postLatitudeStr, @"latitude",
                           self.postLongtitudeStr, @"longtitude",
                           nowString, @"img_datetime",
                           self.titleStr, @"title",
                           self.detailStr, @"body",
                           @"1", @"appli_id",//バッチさん指示で変更その２（追記）
                           nil], @"image",
                          nil];
    
    // 送信がうまくいかなかった時のために、userdefaultsに下書きを保存しておく
    [ud setObject:dict forKey:@"draft_dict"];
    [ud setObject:UIImagePNGRepresentation(self.capturedImage) forKey:@"draft_photo"];
    [ud synchronize];
    
    NSError *error = nil;
    NSData *data = nil;
    if([NSJSONSerialization isValidJSONObject:dict]){
        data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        NSLog(@"data to string: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
    
    request.HTTPBody = data;
    
    //    ConnectionHandler *connectionHandler = [[ConnectionHandler alloc] init];
    //    connectionHandler.api = SaveUUID;
    self.tmpImageConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (self.tmpImageConnection) {
        NSLog(@"Success!!");
    } else {
        NSLog(@"Error!!");
    }
    return YES;
}

#pragma mark - サーバアクセス時のレスポンスの受け取り
// サーバからレスポンスヘッダを受け取ったときに呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // 送信されるデータの文字コードを取得
    NSString *encodingName = [response textEncodingName];
    
    NSLog(@"受信文字コード: %@", encodingName);
    
    if ([encodingName isEqualToString: @"euc-jp"]) {
        self.receivedDataEncoding = NSJapaneseEUCStringEncoding;
    } else {
        self.receivedDataEncoding = NSUTF8StringEncoding;
    }
}

// サーバからデータを受け取るたびに呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // 取得したデータをreceivedDataへ格納する
    NSLog(@"受信データ（バイト数）: %lu", (unsigned long)[data length]);
    [self.receivedData appendData:data];
}



// データの取得が終了したときに呼び出される
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.tmpImageConnection = nil;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingAllowFragments error:nil];
    self.img_path = [dic objectForKey:@"img_path"];
    
    if (self.img_path == nil) {
        // herokuとの通信失敗
        [SVProgressHUD dismiss];
        [self showAlertMessage:@"Please retry!!" withTitle:@"Upload Error"];
    } else {
        // herokuとの通信成功
        self.html_path = [dic objectForKey:@"html_path"];
        self.post_id = [dic objectForKey:@"id"];
        
        BOOL isFirstShot = [self.img_path hasSuffix:@"n0.jpg"];
        if (isFirstShot) {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            int first_shot = (int)[ud integerForKey:@"first_shot"];
            [ud setInteger:++first_shot forKey:@"first_shot"];
            [ud synchronize];
        }
        
        NSString *imgName;
        if ([[Util sharedManager].heroku isEqualToString:HEROKU_DEV]) {
            imgName = [self.img_path substringFromIndex:34];
        } else if ([[Util sharedManager].heroku isEqualToString:HEROKU_MASTER]) {
            //バッチさん指示変更その３
            // imgName = [self.img_path substringFromIndex:60];
            imgName = [self.img_path substringFromIndex:30];
        } else {
            imgName = [self.img_path substringFromIndex:34];
        }
        // 以下の行を上記の行に書き換え
        // NSString *imgName = [imgpath substringFromIndex:30];
        
        NSLog(@"イメージの投稿URL: %@", self.img_path);
        NSLog(@"イメージのファイル名: %@", imgName);
        
        [self dispatchUploadImgS3:imgName];
    }
}

#pragma mark - S3への画像の送信
- (void)dispatchUploadImgS3:(NSString*)imageName
{
    [SVProgressHUD showWithStatus:@"Sending Image..." maskType:SVProgressHUDMaskTypeBlack];
    self.s3status = willCall;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        // Upload image data.  Remember to set the content type.
        S3PutObjectRequest *por;
        if ([[Util sharedManager].heroku isEqualToString:HEROKU_DEV]) {
            por = [[S3PutObjectRequest alloc] initWithKey:imageName inBucket:[Constants pictureBucket]];
            
        } else if ([[Util sharedManager].heroku isEqualToString:HEROKU_MASTER]) {
            //バッチさん指示変更その４
            por = [[S3PutObjectRequest alloc] initWithKey:imageName inBucket:@"www.where.eagle-inc.jp"];
            
        } else {
            por = [[S3PutObjectRequest alloc] initWithKey:imageName inBucket:[Constants pictureBucket]];
        }
        // 以下の行を上記の行に書き換え
        //        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageName inBucket:[Constants pictureBucket]];
        
        por.contentType = @"image/jpeg";
        por.data        = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.capturedImage, 0.9)];
        
        // 画像の解像度を1500から600に変更
        NSData* data = por.data;
        UIImage* aImage = [[UIImage alloc] initWithData:data];
        CGSize resizedSize = CGSizeMake(600, 600);
        UIGraphicsBeginImageContext(resizedSize);
        [aImage drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
        UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData* jpgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(resizedImage, 1.0)];
        por.data = jpgData;
        
        por.cannedACL = [S3CannedACL publicRead];
        
        self.s3status = calling;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(putObjectResponse.error != nil)
            {
                if (self.s3status != called || self.s3status != callFailure) {
                    [self.timeout invalidate];
                    self.timeout = nil;
                    NSLog(@"Error: %@", putObjectResponse.error);
                    [self showAlertMessage:[putObjectResponse.error.userInfo objectForKey:@"message"] withTitle:@"Upload Error"];
                }
            }
            else
            {
                if (self.s3status == calling) {
                    NSLog(@"in self.s3status == calling");
                    self.s3status = called;
                    [self sendUploadSuccessToHeroku];
                }
            }
        });
    });
}

- (void)appearSendDetail {
    UIButton *postBtn = (UIButton*)[self.view viewWithTag:POST_BTN_TAG];
    //postBtn.enabled = NO;
    postBtn.hidden = YES;
    
    [self showImobile];
    
    [SVProgressHUD showWithStatus:@"Now Completed." maskType:SVProgressHUDMaskTypeBlack];
    
#pragma mark カメラロールに保存で追加
    //2014/5/15 カメラロールに保存で追加
    UIImageWriteToSavedPhotosAlbum(self.capturedImage, self, @selector(onCompleteCapture:didFinishSavingWithError:contextInfo:), NULL);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"draft_dict"];
    [ud removeObjectForKey:@"draft_photo"];
    
    self.url = [NSURL URLWithString:self.connectionHandler.html_path];
    
    self.from = FromCameraViewController;
    [self setUpDetailPageParts];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"];    [ImobileSdkAds startBySpotID:@"379689"];
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)];
}


//画像保存完了時のセレクタ ＊2014/5/15 カメラロールに保存で追加

- (void)onCompleteCapture:(UIImage *)screenImage didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    NSString *message = @"カメラロールへの保存に失敗しました";
    if (!error) {
        
        message = @"画像をカメラロールに保存しました";
        
    }
    
    
}

- (void)sendUploadSuccessToHeroku {
    [SVProgressHUD showWithStatus:@"End Processing." maskType:SVProgressHUDMaskTypeBlack];
    // 送信したいURLを作成する
    //    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat: @"%@/%@", [Util sharedManager].heroku, @"/commitimage"]];
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat: @"%@/%@",[Util sharedManager].heroku, @"/commitimage"]];
    NSLog(@"commit:%@",url);
    
    // Mutableなインスタンスを作成し、インスタンスの内容を変更できるようにする
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    // MethodにPOSTを指定する。
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *uuid;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud stringForKey:@"uuid"] != nil) {
        uuid = [ud stringForKey:@"uuid"];
    } else {
        uuid = @"NotFoundUUID";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          uuid, @"uuid",
                          self.html_path, @"html_path",
                          self.img_path, @"img_path",
                          self.post_id, @"id",
                          nil];
    
    NSError *error = nil;
    NSData *data = nil;
    if([NSJSONSerialization isValidJSONObject:dict]){
        data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        NSLog(@"data to string: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
    
    request.HTTPBody = data;
    
    self.connectionHandler = [[ConnectionHandler alloc] init];
    self.connectionHandler.api = CommitImage;
    
    // commitimage完了後に詳細画面をロード
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(loadWebViewAfterCommitImage) name:@"commitimageComplated" object:self.connectionHandler];
    
    self.commitImageConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self.connectionHandler];
    
    if (self.commitImageConnection) {
        NSLog(@"Success!!");
    } else {
        NSLog(@"Error!!");
    }
}

- (void)loadWebViewAfterCommitImage {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"commitimageComplated" object:self.connectionHandler];
    
    self.commitImageConnection = nil;
    [self.timeout invalidate];
    self.timeout = nil;
    
    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    NSString *nowStr = [formatter stringFromDate:now];
    
    [[Util sharedManager] saveHistory:self.capturedImage url:self.connectionHandler.html_path recordDate:nowStr latitude:self.postLatitude longtitude:self.postLongtitude title:self.titleStr detail:self.detailStr];
    
    [self appearSendDetail];
}

#pragma mark - 投稿画面のボタンなどの処理
// キャンセルボタンを押した場合の処理
- (void)cancelShootingPhoto:(id)sender {
    /*
     [self.imageEditer startEffectToStillCamera:self.stillCamera GPUImageView:self.gpuImageView];
     
     UIView *sendPhotoBackground = [self.view viewWithTag:SENDPHOTO_BACKGROUND_TAG];
     [sendPhotoBackground removeFromSuperview];
     
     UIButton *shotbtn = (UIButton*)[self.view viewWithTag:SHOT_BTN_TAG];
     shotbtn.enabled = YES;
     */
}

#pragma mark - 投稿画面の生成
- (void)setUpPostPageParts {
    // 投稿画面のベースに背景view
    // float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    
    UIView *sendPhotoBackground = [[UIView alloc] initWithFrame:CGRectMake(0,30, SCREEN_BOUNDS.size.width, SCREEN_BOUNDS.size.height)];
    sendPhotoBackground.backgroundColor = [UIColor whiteColor];
    sendPhotoBackground.tag = SENDPHOTO_BACKGROUND_TAG;
    [self.view addSubview:sendPhotoBackground];
    
    // 投稿画面の背景画像
    UIImageView *sendPhotoBackgroundImage = [[UIImageView alloc] init];
    
    sendPhotoBackgroundImage.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
    sendPhotoBackgroundImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    [sendPhotoBackground addSubview:sendPhotoBackgroundImage];
    
    
    // 投稿画面のヘッダー画像
    //    UIImageView *headerSendPhoto = [[UIImageView alloc] init];
    //    headerSendPhoto.backgroundColor = NavBarColor;
    //    headerSendPhoto.frame = CGRectMake(0, 0, WIN_SIZE.width, 47);
    //    [sendPhotoBackground addSubview:headerSendPhoto];
    //
    //    UIButton *closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    closebtn.frame = CGRectMake(7, 13, 23, 23);
    //    [closebtn setImage:[UIImage imageNamed:@"reverse-arrow.png"] forState:UIControlStateNormal];
    //    [closebtn setImage:[UIImage imageNamed:@"reverse-arrow.png"] forState:UIControlStateHighlighted];
    //    [closebtn addTarget:self action:@selector(cancelShootingPhoto:) forControlEvents:UIControlEventTouchUpInside];
    //    [sendPhotoBackground addSubview:closebtn];
    
    // datebtnに表示される現在時刻。NSDateの時点ではグリニッジ時間だが、DateFormatterで変換する際にデフォルトでシステムのlocateに変更される
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0.0f];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *nowString = [formatter stringFromDate:now];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(SCREEN_BOUNDS.size.width/2-30, 49+10, 170, 20)];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque; // スタイルを設定
    [toolBar sizeToFit];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeKeyboard:)];
    NSArray *items = [NSArray arrayWithObjects:spacer, done, nil];
    [toolBar setItems:items animated:YES];
    
    //日付テキストの下地
    UIImageView *tfBase = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"date-panel.png"]];
    tfBase.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-22, 49+7, 164, 31);
    [sendPhotoBackground addSubview:tfBase];
    
    
    self.dateTextField = [[UITextField alloc] initWithFrame:CGRectMake(SCREEN_BOUNDS.size.width/2-20, 49+10, 160, 25)];
    self.dateTextField.borderStyle = UITextBorderStyleRoundedRect;
    
    if (!self.isCameraRoll) {
        self.dateTextField.text = nowString;
    } else {
        self.dateTextField.text = NSLocalizedString(@"inputDate", @"投稿画面の日付入力");
    }
    
    self.dateTextField.textAlignment = NSTextAlignmentCenter;
    self.dateTextField.inputView = self.datePicker;
    self.dateTextField.inputAccessoryView = toolBar;
    [sendPhotoBackground addSubview:self.dateTextField];
    
    //タイトル入力部分の下地
    UIImageView *ttBase = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-panel.png"]];
    ttBase.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-142,46+50+20-20-6 , 284, 41);
    [sendPhotoBackground addSubview:ttBase];
    
    // タイトル入力部分
    UITextField *titletf = [[UITextField alloc] initWithFrame:CGRectMake(SCREEN_BOUNDS.size.width/2-140+2, 46+50+20-20-1, 276, 30)];
    titletf.placeholder = NSLocalizedString(@"writeTitle", @"投稿画面のタイトル入力");
    titletf.borderStyle = UITextBorderStyleRoundedRect;
    titletf.returnKeyType = UIReturnKeyDone;
    titletf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    titletf.tag = TITLE_TEXTFIELD_TAG;
    titletf.delegate = self;
    [titletf addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [sendPhotoBackground addSubview:titletf];
    
    //本文入力部分の下地
    UIImageView *mBase = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"text-panel.png"]];
    mBase.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-140-2, 46+50+20+30+10-22-1 , 284, 82);
    [sendPhotoBackground addSubview:mBase];
    
    // 本文入力部分
    UIImageView *bodyBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"textfield"]];
    bodyBackground.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-140, 46+50+20+30+10-22, 280, 80);
    [sendPhotoBackground addSubview:bodyBackground];
    
    UIPlaceHolderTextView *detailtv = [[UIPlaceHolderTextView alloc] initWithFrame:CGRectMake(SCREEN_BOUNDS.size.width/2-140, 46+50+20+30+10-22, 280, 80)];
    detailtv.placeholder = NSLocalizedString(@"writeEpisode", @"投稿画面の本文入力");
    detailtv.backgroundColor = [UIColor clearColor];
    detailtv.delegate = self;
    detailtv.tag = DETAIL_TEXTFIELD_TAG;
    [sendPhotoBackground addSubview:detailtv];
    
    
    //画像部分の下地
    UIImageView *pBase = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"picture-panel.png"]];
    pBase.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-75-11, (46+50+20+30+10-22+40+(SCREEN_BOUNDS.size.height-20-(46+50+20+30+10-22)-150-50)/2)-11, 172, 172);
    [sendPhotoBackground addSubview:pBase];
    
    // 送信予定の画像
    UIImageView *sendPhoto = [[UIImageView alloc] initWithImage:self.capturedImage];
    sendPhoto.frame = CGRectMake(SCREEN_BOUNDS.size.width/2-75, 46+50+20+30+10-22+40+(SCREEN_BOUNDS.size.height-20-(46+50+20+30+10-22)-150-50)/2, 150, 150);
    [sendPhotoBackground addSubview:sendPhoto];
    
    // 投稿ボタン
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendBtn setImage:[UIImage imageNamed:@"post_btn.png"] forState:UIControlStateNormal];
    [sendBtn setImage:[UIImage imageNamed:@"post_btn.png"] forState:UIControlStateHighlighted];
    //[sendBtn setBackgroundColor:[UIColor blueColor]];
    [sendBtn setTitle:@"POST" forState:UIControlStateNormal];
    [sendBtn setTitle:@"POST" forState:UIControlStateHighlighted];
    sendBtn.frame = CGRectMake(0,0,220,52);
    sendBtn.center = CGPointMake(WIN_SIZE.width/2, 500);
    sendBtn.tag = POST_BTN_TAG;
    [sendBtn addTarget:self action:@selector(showConfirmationOnlyCameraRoll:) forControlEvents:UIControlEventTouchUpInside];
    [sendPhotoBackground addSubview:sendBtn];
}

-(BOOL)showConfirmationOnlyCameraRoll:(id)sender
{
    //UIButton *b = sender;
    //b.enabled = NO;
    
    if ([self.dateTextField.text isEqualToString:NSLocalizedString(@"inputDate", @"投稿画面の日付入力")]) {
        UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"demandInputDate", @"日付が入力されていなかった場合のアラートメッセージ") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    UITextField *titletf = (UITextField*)[self.view viewWithTag:TITLE_TEXTFIELD_TAG];
    self.titleStr = titletf.text;
    if (self.titleStr == nil || [self.titleStr isEqualToString:@""] || [self.titleStr isEqualToString:@" "]) {
        UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"demandInputTitle", @"タイトルが入力されていなかった場合のアラートメッセージ") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    if (self.isCameraRoll) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"confirm", @"確認")
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"confirmPost", @"カメラロールから投稿した場合の確認アラート"), self.dateTextField.text]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", @"キャンセル")
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    } else {
        [self sendPhotoToServer];
    }
    return NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self sendPhotoToServer];
    }
}

- (void)alertViewCancel:(UIAlertView*)alertView {
    NSLog(@"cancel");
}

-(void)closeKeyboard:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    self.dateTextField.text = [df stringFromDate:self.datePicker.date];
    [self.dateTextField resignFirstResponder];
}

/*
 #pragma mark - 撮影ボタンを押した時に呼ばれるメソッド
 - (void)pressSaveBtn:(id)sender {
 if ([LocationUtility sharedManager].isLocationEnabled) {
 UIButton *shotbtn = (UIButton*)[self.view viewWithTag:SHOT_BTN_TAG];
 shotbtn.enabled = NO;
 
 self.postLatitude = [LocationUtility sharedManager].latitude;
 self.postLongtitude = [LocationUtility sharedManager].longtitude;
 self.postLatitudeStr = [NSString stringWithFormat:@"%.3f", self.postLatitude];
 self.postLongtitudeStr = [NSString stringWithFormat:@"%.3f", self.postLongtitude];
 
 self.isCameraRoll = NO;
 // 指定したFilterがかかった、画像が取得できる。そのために、プロパティでfilterをもたせている。
 
 [self.stillCamera capturePhotoAsImageProcessedUpToFilter:self.imageEditer.filterGroup withCompletionHandler: ^(UIImage *processedImage, NSError *error){
 
 // S3に画像を送信する際に再度使うのでインスタンス変数に代入しておく
 self.capturedImage = processedImage;
 
 // 投稿画面の生成
 [self setUpPostPageParts];
 
 // カメラ機能のストップ
 [self.stillCamera stopCameraCapture];
 }];
 
 } else {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
 [alert show];
 }
 }
 */
#pragma mark - カメラ起動メソッド
- (void) wakeCamera {
    [self makeCameraViewParts];
    
    // キャンセルボタン
    //    float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    //
    //    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    cancelBtn.frame = CGRectMake(0, 0, 45, 54);
    //    cancelBtn.center = CGPointMake(8+22.5f, SCREEN_BOUNDS.size.height-20-27+statusbarHeight);
    //    [cancelBtn setImage:[UIImage imageNamed:@"camera_cancel_btn"] forState:UIControlStateNormal];
    //    [cancelBtn setImage:[UIImage imageNamed:@"camera_cancel_btn_active"] forState:UIControlStateHighlighted];
    //    [cancelBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:cancelBtn];
    
}

- (void)makeCameraViewParts {
    float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    
    UIImageView *cameraBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, statusbarHeight, 320, 548)];
    cameraBackground.image = [UIImage imageNamed:@"background.png"];
    [self.view addSubview:cameraBackground];
    
    /*
     if (SCREEN_BOUNDS.size.height == 568) {
     self.gpuImageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 87-10, 320, 320)];
     } else {
     self.gpuImageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 43-5, 320, 320)];
     }
     */
    /*
     self.gpuImageView.layer.shadowOpacity = 0.2; // 濃さを指定
     self.gpuImageView.layer.shadowOffset = CGSizeMake(0.0, 10.0); // 影までの距離を指定
     [cameraBackground addSubview:self.gpuImageView];
     */
    UIImageView *cameraBottomBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, SCREEN_BOUNDS.size.height-20-58, 320, 58)];
    cameraBottomBar.image = [UIImage imageNamed:@"camera_bottom"];
    [cameraBackground addSubview:cameraBottomBar];
    
    //    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn.frame = CGRectMake(0, 0, 100, 54);
    //    btn.tag = SHOT_BTN_TAG;
    //    btn.center = CGPointMake(SCREEN_BOUNDS.size.width / 2, SCREEN_BOUNDS.size.height-20-27+statusbarHeight);
    //    [btn setImage:[UIImage imageNamed:@"camera_shot_btn"] forState:UIControlStateNormal];
    //    [btn setImage:[UIImage imageNamed:@"camera_shot_btn_active"] forState:UIControlStateHighlighted];
    //    [btn addTarget:self action:@selector(pressSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:btn];
    
    // フォトライブラリからの画像の取得
    //    UIButton *photoLibraryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    photoLibraryBtn.frame = CGRectMake(0, 0, 45, 54);
    //    photoLibraryBtn.center = CGPointMake(SCREEN_BOUNDS.size.width-8-22.5f, SCREEN_BOUNDS.size.height-20-27+statusbarHeight);
    //    [photoLibraryBtn setImage:[UIImage imageNamed:@"camera_album_btn"] forState:UIControlStateNormal];
    //    [photoLibraryBtn setImage:[UIImage imageNamed:@"camera_album_btn_active"] forState:UIControlStateHighlighted];
    //    [photoLibraryBtn addTarget:self action:@selector(appearPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:photoLibraryBtn];
}

/*
 - (void)setUpStillCamera {
 // setting for stillCamera
 self.stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionBack];
 // 保存される画像はPortrait
 self.stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
 // カメラへのエフェクト開始
 self.imageEditer = [[ImageEditer alloc] init];
 [self.imageEditer startEffectToStillCamera:self.stillCamera GPUImageView:self.gpuImageView];
 }
 */
#pragma mark - 写真をS3に送信完了後に表示するアラート画面（現状では送信失敗時だけ表示）
- (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
    [alertView show];
}

//#pragma mark - フォトライブラリの表示
//- (void) appearPhotoLibrary {
//    if ([LocationUtility sharedManager].isLocationEnabled) {
//        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        if([UIImagePickerController isSourceTypeAvailable:sourceType]) {
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.sourceType = sourceType;
//            picker.allowsEditing = YES;
//            [self presentViewController:picker animated:YES completion:nil];
//        }
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}

#pragma mark - delegate methods
// テキストフィールドでReturnを押した時の挙動制御
-(BOOL)textFieldShouldReturn:(UITextField*)tf{
    [tf resignFirstResponder];
    return YES;
}

/**
 * テキストが編集されたとき
 * @param textField イベントが発生したテキストフィールド
 * @param range 文字列が置き換わる範囲(入力された範囲)
 * @param string 置き換わる文字列(入力された文字列)
 * @retval YES 入力を許可する場合
 * @retval NO 許可しない場合
 */
- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // すでに入力されているテキストを取得
    NSMutableString *text = [textField.text mutableCopy];
    
    // すでに入力されているテキストに今回編集されたテキストをマージ
    [text replaceCharactersInRange:range withString:string];
    
    // 結果が文字数をオーバーしていないならYES，オーバーしている場合はNO
    return ([text length] <= 33);
}

// textViewで文字が入力された際に呼ばれる。改行 (つまり\n) がタッチされた場合にキーボードをしまう処理
// 96文字を超えていた際に入力を許可しない処理
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }
    
    NSMutableString *bodyText = [textView.text mutableCopy];
    [bodyText replaceCharactersInRange:range withString:text];
    if ([bodyText length] > 96) {
        return FALSE;
    }
    
    // For any other character return TRUE so that the text gets added to the view
    return TRUE;
}

// イメージピッカーから画像を取ってきた際に呼ばれる
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    // S3に画像を送信する際に再度使うのでインスタンス変数に代入しておく
    self.capturedImage = image;
    
    //self.isCameraRoll = YES;
    
    self.postLatitude = [LocationUtility sharedManager].latitude;
    self.postLongtitude = [LocationUtility sharedManager].longtitude;
    self.postLatitudeStr = [NSString stringWithFormat:@"%.3f", self.postLatitude];
    self.postLongtitudeStr = [NSString stringWithFormat:@"%.3f", self.postLongtitude];
    
    // 投稿画面の生成
    [self setUpPostPageParts];
    
    [(UIView *)[self.view viewWithTag:999999] removeFromSuperview];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:^{
            //
        }];
    }];
}

-(void)webViewDidFinishLoad:(UIWebView*)webView {
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    } else {
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void) showCamera {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        [imagePickerController setDelegate:self];
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePickerController setAllowsEditing:YES];
        self.isCameraRoll = NO;
        [self presentViewController:imagePickerController animated:NO completion:^{
            
        }];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Camera not available"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}
//#pragma mark - setUpParts
//- (void)setUpCameraPageParts {
//    if ([LocationUtility sharedManager].isLocationEnabled) {
//        [self wakeCamera];
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//
//    //    self.receivedData = [[NSMutableData alloc] init];
//    //
//    //    if(![ACCESS_KEY_ID isEqualToString:@"CHANGE ME"] && self.s3 == nil) {
//    //        // Initial the S3 Client.
//    //        self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
//    //    }
//    //
//    //    self.s3.endpoint = @"https://s3-ap-northeast-1.amazonaws.com";
//}
//
//- (void)setUpParts {
//    [self setUpCameraPageParts];
//    self.isCameraRoll = NO;
//}

#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *concealView = [[UIView alloc] initWithFrame:self.view.bounds];
    [concealView setTag:999999];
    [concealView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:concealView];
    
    //親クラス (DetailViewController) でsetUpPartsが呼ばれているのでここに書く必要なし
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
