//
//  ConnectionHandler.m
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/25.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "ConnectionHandler.h"

@implementation ConnectionHandler

- (id)init {
    self = [super init];
    if (self) {
        receivedData = [[NSMutableData alloc] init];
    }
    return self;
}

// サーバからレスポンスヘッダを受け取ったときに呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // 送信されるデータの文字コードを取得
    NSString *encodingName = [response textEncodingName];
    
    NSLog(@"受信文字コード: %@", encodingName);
    
    if ([encodingName isEqualToString: @"euc-jp"]) {
        receivedDataEncoding = NSJapaneseEUCStringEncoding;
    } else {
        receivedDataEncoding = NSUTF8StringEncoding;
    }
}

// サーバからデータを受け取るたびに呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // 取得したデータをreceivedDataへ格納する
    NSLog(@"受信データ（バイト数）: %d", [data length]);
    [receivedData appendData:data];
}

// データの取得が終了したときに呼び出される
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    switch (self.api) {
        case SaveUUID:
        {
            NSString *result = [[NSString alloc] initWithData:receivedData encoding:receivedDataEncoding];
            NSLog(@"(in ConnectionHandler) データの受信完了: %@", result);
        }
            break;
            
        case TmpImage:
            break;
            
        case CommitImage:
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"(in ConnectionHandler) html_path is %@", [dic objectForKey:@"html_path"]);
            self.html_path = [dic objectForKey:@"html_path"];
            
            NSNotification *n = [NSNotification notificationWithName:@"commitimageComplated" object:self];
            [[NSNotificationCenter defaultCenter] postNotification:n];
        }
            break;
            
        case getMapCount:
        {
            NSLog(@"getMapCount in connectionHandler");
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"getMapCount in connectionHandler : %@", dic);
            
            if ([[dic objectForKey:@"status"] intValue] == 0) {
                NSNotification *n = [NSNotification notificationWithName:@"squaresDataLoaded" object:self userInfo:dic];
                [[NSNotificationCenter defaultCenter] postNotification:n];
                NSLog(@"getMapCount in connectionHandlerの status が 0");
            } else {
                //エラーアラート
                NSLog(@"getMapCount in connectionHandlerの status が 0以外");
            }
            break;
        }
            
        case getAreaNew:
        {
            NSLog(@"getAreaNew in connectionHandler");
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"areaNew in connectionHandler : %@", dic);
            
            NSNotification *n = [NSNotification notificationWithName:@"getAreaNewComplated" object:self userInfo:dic];
            [[NSNotificationCenter defaultCenter] postNotification:n];
            
            break;
        }
            
        default:
            NSLog(@"default in connectionHandler");
            break;
    }
}

@end
