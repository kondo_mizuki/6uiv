//
//  ViewController.h
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKit/MapKit.h"

@interface ViewController : UIViewController
<MKMapViewDelegate>

@end
