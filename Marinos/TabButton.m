//
//  TabButton.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/14.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "TabButton.h"
#import "TopVC.h"
#import "MapVC.h"
#import "CardVC.h"
#import "AlbumVC.h"


@implementation TabButton

-(id)initTabButtons:(int)senderTag
{
    if ((self = [super init]))
    {
        [self setUpButtons:senderTag];
    }return self;
}

#pragma mark - ボタン４つ作成
-(void)setUpButtons:(int)senderTag
{
    //背景作成
    //不要かな？
    self.frame = CGRectMake(0, 0, WIN_SIZE.width, 50);
    //self.backgroundColor =[UIColor blackColor];
    
    UIImageView *tabBG = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"under_bar_bg.png"]];
    tabBG.frame = self.frame;
    [self addSubview:tabBG];
    
    //ボタン４つ作成
    
    UIButton *topButton = [UIButton buttonWithType:UIButtonTypeCustom];
    topButton.frame = CGRectMake(0,0,80,50);
    topButton.tag = TAB_TOP;
    [topButton setImage:[UIImage imageNamed:@"top_off.png"] forState:UIControlStateNormal];
    [topButton setImage:[UIImage imageNamed:@"top_on.png"] forState:UIControlStateHighlighted];
    [topButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:topButton];

    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mapButton.frame = CGRectMake(WIN_SIZE.width/4,0,WIN_SIZE.width/4,50);
    mapButton.tag = TAB_MAP;
    [mapButton setImage:[UIImage imageNamed:@"map_off.png"] forState:UIControlStateNormal];
    [mapButton setImage:[UIImage imageNamed:@"map_on.png"] forState:UIControlStateHighlighted];
    [mapButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:mapButton];

    UIButton *cardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cardButton.frame = CGRectMake(WIN_SIZE.width/2,0,WIN_SIZE.width/4,50);//(160,-5,80,55)
    cardButton.tag = TAB_CARD;
    [cardButton setImage:[UIImage imageNamed:@"point_off.png"] forState:UIControlStateNormal];
    [cardButton setImage:[UIImage imageNamed:@"point_on.png"] forState:UIControlStateHighlighted];
    [cardButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cardButton];
    
    
    UIButton *albumButton = [UIButton buttonWithType:UIButtonTypeCustom];
    albumButton.frame = CGRectMake(WIN_SIZE.width/4*3,0,WIN_SIZE.width/4,50);
    albumButton.tag = TAB_ALBUM;
    [albumButton setImage:[UIImage imageNamed:@"album_off.png"] forState:UIControlStateNormal];
    [albumButton setImage:[UIImage imageNamed:@"album_on.png"] forState:UIControlStateHighlighted];
    [albumButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:albumButton];
    

    
    if(senderTag == TAB_TOP)
    {
        [topButton setImage:[UIImage imageNamed:@"top_on.png"] forState:UIControlStateNormal];
        
    }
    else if (senderTag == TAB_MAP)
    {
        [mapButton setImage:[UIImage imageNamed:@"map_on.png"] forState:UIControlStateNormal];
        
    }
    else if (senderTag == TAB_CARD)
    {
        [cardButton setImage:[UIImage imageNamed:@"point_on.png"] forState:UIControlStateNormal];

    }
    else if (senderTag == TAB_ALBUM)
    {
        [albumButton setImage:[UIImage imageNamed:@"album_on.png"] forState:UIControlStateNormal];

    }
}



-(void)buttonPushed:(id)sender
{
    self.tag = [sender tag];
    [_delegate anyButtonPushed:self];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
