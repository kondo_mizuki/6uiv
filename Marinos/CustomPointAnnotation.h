//
//  CustomPointAnnotation.h
//  BigbrotherMap
//
//  Created by kyo on 2013/07/25.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Define.h"

@interface CustomPointAnnotation : MKPointAnnotation

@property (assign, nonatomic) int placeID;
@property (assign, nonatomic) double radius;
@property (strong, nonatomic) NSString *detail;
@property (strong, nonatomic) NSString *univName;

@end
