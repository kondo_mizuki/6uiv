//
//  rocksWebVC.m
//  6uiv
//
//  Created by macbook012 on 2015/03/25.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "rocksWebVC.h"

@interface rocksWebVC ()

@end

@implementation rocksWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.WV loadRequest:req];
    
    //self.WV.scrollView.delegate = self;
    //[self.WV.scrollView setMaximumZoomScale: 5.0];
    
    self.WV.scalesPageToFit = YES;

    [self showImobile];
}

// ズームイベントの実装
//- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    return scrollView;
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = self.title;
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
//        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefaultPrompt];
    }
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"];
    [ImobileSdkAds startBySpotID:@"379689"];
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)];
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
