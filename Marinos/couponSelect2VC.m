//
//  couponSelect2VC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/24.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "couponSelect2VC.h"

@interface couponSelect2VC ()

@end

@implementation couponSelect2VC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showImobile];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-50)]; //広告を表示します
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"クーポン";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    if([[self.navigationController viewControllers] count] > 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
        [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage new] forState:UIControlStateNormal barMetrics:UIBarMetricsDefaultPrompt];
    }
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)coupon1Push:(id)sender {
    [self pushBtn:1];
}

- (IBAction)coupon2Push:(id)sender {
    [self pushBtn:2];
}

- (IBAction)coupon3Push:(id)sender {
    [self pushBtn:3];
}

- (IBAction)coupon4Push:(id)sender {
    [self pushBtn:4];
}

-(void)pushBtn:(int)couponNum{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    couponListVC *cs2VC = (couponListVC *)[storyboard instantiateViewControllerWithIdentifier:@"couponListVC"];
    cs2VC.couponNum=couponNum;
    [self.navigationController pushViewController:cs2VC animated:YES];
}
@end
