//
//  AlbumDetailViewController.h
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/16.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextView.h"

@interface AlbumDetailViewController : UIViewController
{
    UIView *infoView;
    UIImageView *cardImage;
}
@property (strong, nonatomic) id detailItem;
@property (nonatomic,assign) UIImageView *imageView;
@property (nonatomic,assign) int cardID;

@end
