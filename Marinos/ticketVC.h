//
//  ticketVC.h
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImobileSdkAds/ImobileSdkAds.h"
#import <StoreKit/StoreKit.h>
#import "PremiumVC.h"

@interface ticketVC : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver,UIAlertViewDelegate>
- (IBAction)pushPayBtn:(id)sender;
- (IBAction)pushRestoreBtn:(id)sender;

@property (nonatomic, assign) BOOL isFromRestore;

@end
