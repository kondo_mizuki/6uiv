//
//  AlbumCell.h
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/26.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumCell : UICollectionViewCell

- (void) displayCard:(int)cardID possession:(int)cardPossession;

@end
