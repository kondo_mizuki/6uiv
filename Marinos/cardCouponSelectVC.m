//
//  cardCouponSelectVC.m
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import "cardCouponSelectVC.h"

@interface cardCouponSelectVC (){
    TabButton *tb;
    int myPoint;
}

@end

@implementation cardCouponSelectVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.tag = TAB_CARD;
    [self tabButtonsInit];
    [self showImobile];
    [self myPointLoad];
}

#pragma mark - マイポイント読込・記録・使用
-(void)myPointLoad
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
    self.pointLbl.text = [NSString stringWithFormat:@"%d",myPoint];
}

-(void)showImobile{
    [ImobileSdkAds registerWithPublisherID:@"33379" MediaID:@"146912" SpotID:@"379689"]; //広告の取得に必要な情報を設定します
    [ImobileSdkAds startBySpotID:@"379689"]; //広告の取得を開始します
    [ImobileSdkAds showBySpotID:@"379689" ViewController:self Position:CGPointMake(0,WIN_SIZE.height-TABBUTTON_HEIGHT-50)]; //広告を表示します
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.navigationController.navigationBar.barTintColor = NavBarColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    //self.title=@"チケット購入";
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont boldSystemFontOfSize:16.0];
    title.textColor = [UIColor whiteColor];
    title.text = @"所持ポイント";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
}

#pragma mark - タブボタン生成
-(void)tabButtonsInit
{
    //float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
    tb = [[TabButton alloc]initTabButtons:(int)self.view.tag];
    [tb setDelegate:self];
    tb.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-20);
    [self.view addSubview:tb];
}

#pragma mark - タブボタンどれか押した
- (void)anyButtonPushed:(id)sender
{
    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
    if (vc !=nil)
    {//現在の画面とタブが違う場合のみ遷移
        self.navigationController.navigationBarHidden = YES;
        if ([sender tag] == TAB_TOP)
        {//トップ画面のみポップで遷移
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else
        {//それ以外はプッシュで遷移
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)couponBtnPush:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    couponSelect2VC *cs2VC = (couponSelect2VC *)[storyboard instantiateViewControllerWithIdentifier:@"CS2VC"];
    [self.navigationController pushViewController:cs2VC animated:YES];
}

- (IBAction)playerCardBtnPush:(id)sender {
    CardVC *cardVC = [[CardVC alloc] init];
    [self.navigationController pushViewController:cardVC animated:YES];
}
@end
