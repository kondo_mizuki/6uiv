//
//  HomeResultCell.h
//  6uiv
//
//  Created by macbook012 on 2015/03/09.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeResultCellDelegate <NSObject>

-(void)gameResultPush;
-(void)showChargeAlert;
@end

@interface HomeResultCell : UITableViewCell<UIWebViewDelegate,UIGestureRecognizerDelegate>{
   // id<HomeResultCellDelegate> delegate;
}

@property (strong, nonatomic) IBOutlet UIWebView *resultWebView;
- (IBAction)noChargePush:(id)sender;

-(void)refreshWV;

@property (nonatomic,retain) id<HomeResultCellDelegate> delegate;

@end
