//
//  cardCouponSelectVC.h
//  6uiv
//
//  Created by macbook012 on 2015/02/23.
//  Copyright (c) 2015年 近藤瑞紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabButton.h"
#import "Util.h"
#import "CardVC.h"
#import "couponSelect2VC.h"

@interface cardCouponSelectVC : UIViewController
- (IBAction)couponBtnPush:(id)sender;
- (IBAction)playerCardBtnPush:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *pointLbl;
@end
