//
//  AlbumDetailViewController.m
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/16.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "AlbumDetailViewController.h"
#import "AlbumVC.h"
#import "Util.h"
#import "DatabaseUtility.h"
#import "TabButton.h"

@interface AlbumDetailViewController (){
    UITextView *exTv;
}

@end

@implementation AlbumDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void)viewWillDisappear:(BOOL)animated
{
    if (![self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setUpParts];
   // [self performSelector:@selector(infoViewAnimation:) withObject:nil afterDelay:1.5];
    infoView.alpha = 0.0f;
    //タグ付け
    //self.view.tag = TAB_ALBUM_DETAIL;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    [TrackingManager sendScreenTracking:@"アルバム詳細画面"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}




-(void)setUpParts
{
    UIImageView *background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"afterAnimationBG.png"]];
//    if (WIN_SIZE.height==568)
//    {
        background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
//        background.image = [UIImage imageNamed:@"card_bg5.png"];
//    }
    background.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    [self.view addSubview:background];

    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:_cardID];//なんかエラー出てもとりあえず1番返そうw
    
    //カードID、カード画像ファイル名、カードに関する情報（現在精査中）をintやNSStringで保持。
    NSString *file_name = selectedCardDic[@"file_name"];
    //file_name = [@"pic" stringByAppendingString:file_name];
    if (file_name == nil)
    {
        NSLog(@"ファイル名が空っぽって噂");
    }
    //カード情報を格納
//    UIImage *img =  [UIImage imageNamed:[NSString stringWithFormat:@"card-base.png"]];
//    UIImageView *baseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 349, 515)];//78/117 349/515
//    baseView.contentMode = UIViewContentModeScaleToFill;
//    baseView.image = img;
    cardImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    //ステータスバーの分それぞれ２０足した2013/9/25
    cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    if (WIN_SIZE.height == 568)
    {
        cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);    }
    
    //TODO:_detailItemをもとにこのimgをDBから取得する！
    UIImage *img = [UIImage imageNamed:file_name];
    cardImage.image = img;
    //[baseView addSubview:cardImage];
    
   // cardImage.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [self.view addSubview:cardImage];
    
    
    
    //カード情報表示
//    infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIN_SIZE.width, 90)];
//    infoView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//    [self.view addSubview:infoView];
    //カードID
//    NSString *selectedCardIDString = [NSString stringWithFormat:@"No.%03d",_cardID];
//    UILabel *cardIDLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width, 40)];
//    cardIDLabel.center = CGPointMake(WIN_SIZE.width/2,22);//ここで位置調整
//    if (_cardID >= 31) {
//        cardIDLabel.center = CGPointMake(WIN_SIZE.width/2,50);
//        if (WIN_SIZE.height == 568) {
//            cardIDLabel.center = CGPointMake(WIN_SIZE.width/2,22);
//        }
//    }
//    cardIDLabel.text = selectedCardIDString;
//    cardIDLabel.textColor = [UIColor whiteColor];
//    cardIDLabel.backgroundColor = [UIColor clearColor];
//    cardIDLabel.textAlignment = NSTextAlignmentCenter;
//    if (WIN_SIZE.height == 568) {
//        cardIDLabel.font = [UIFont fontWithName:MY_FONT size:47];
//    }else{
//        cardIDLabel.font = [UIFont fontWithName:MY_FONT size:30];
//    }
//    
//    //cardIDLabel.adjustsFontSizeToFitWidth = YES;
//   // [infoView addSubview:cardIDLabel];
//    [self.view addSubview:cardIDLabel];

    
    /*
    UITextView *cardCharaText = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, WIN_SIZE.width,40)];
    cardCharaText.center = CGPointMake(WIN_SIZE.width/2,45);
    cardCharaText.text = selectedCardCharaName;
    cardCharaText.textColor = [UIColor whiteColor];
    cardCharaText.backgroundColor = [UIColor clearColor];
    cardCharaText.textAlignment = NSTextAlignmentLeft;
    cardCharaText.font = [UIFont fontWithName:@"Cochin" size:28];
    [infoView addSubview:cardCharaText];
     */
//    NSString *spotName = selectedCardDic[@"name"];
//    UILabel *spotNameLabel;
//    if (WIN_SIZE.height==568) {
//        spotNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width-20, 25)];
//        spotNameLabel.center = CGPointMake(WIN_SIZE.width/2,70);
//    }else{
//        spotNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width-20, 25)];
//        spotNameLabel.center = CGPointMake(WIN_SIZE.width/2,50);
//    }
//
//    spotNameLabel.text = spotName;
//    spotNameLabel.textColor = [UIColor whiteColor];
//    spotNameLabel.backgroundColor = [UIColor clearColor];
//    spotNameLabel.textAlignment = NSTextAlignmentCenter;
//    spotNameLabel.font = [UIFont fontWithName:MY_FONT size:18];
//    spotNameLabel.adjustsFontSizeToFitWidth = YES;
//    if (_cardID<31) {
//        [self.view addSubview:spotNameLabel];
//    }
//    
//    //下部の長い説明
//    NSString *selectedCardCharaDetailName = selectedCardDic[@"explanation"];
//    NSLog(@"説明文:%@", selectedCardCharaDetailName);
    
    
   //***********************************************
//    UILabel *cardCharaDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width-20, 60)];
//    if (WIN_SIZE.height == 568){
//        cardCharaDetailLabel.center = CGPointMake(WIN_SIZE.width/2,450);
//    }else{
//        cardCharaDetailLabel.center = CGPointMake(WIN_SIZE.width/2,410);
//    }
//    cardCharaDetailLabel.text = selectedCardCharaDetailName;
//    cardCharaDetailLabel.textColor = [UIColor whiteColor];
//    cardCharaDetailLabel.numberOfLines=6;
//    cardCharaDetailLabel.backgroundColor = [UIColor clearColor];
//    cardCharaDetailLabel.textAlignment = NSTextAlignmentLeft;
//    cardCharaDetailLabel.font = [UIFont fontWithName:MY_FONT size:10];
//    cardCharaDetailLabel.adjustsFontSizeToFitWidth = YES;
//    [self.view addSubview:cardCharaDetailLabel];
    //***********************************************
    
    //textviewに変更してみる
//    exTv=[[CustomTextView alloc]init];
//    exTv.frame=CGRectMake(0, 0, WIN_SIZE.width-30, 70);
//        if (WIN_SIZE.height == 568){
//            exTv.center = CGPointMake(WIN_SIZE.width/2,450);
//        }else{
//            exTv.center = CGPointMake(WIN_SIZE.width/2,410);
//        }
//    exTv.font =[UIFont systemFontOfSize:[UIFont systemFontSize]];
//    exTv.textColor=[UIColor whiteColor];
//    exTv.backgroundColor=[UIColor clearColor];
//    exTv.editable=NO;
//    exTv.text=selectedCardCharaDetailName;
//    [self.view addSubview:exTv];
//    [self.view setUserInteractionEnabled:YES];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    //backButton.center = CGPointMake(20,WIN_SIZE.height-20);
    backButton.center = CGPointMake(20,20);
    [backButton setBackgroundImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(viewBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}



-(void)viewBack:(id)sender
{
    [[Util sharedManager] playSound:SOUND_ALBUM];
    [self.navigationController popViewControllerAnimated:NO];
}



//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if (infoView.alpha == 0.0f)
//    {
//        [self infoViewAnimation:YES];
//    }
//    else 
//    {
//        [self infoViewAnimation:NO];
//    }
//}



-(void)infoViewAnimation:(BOOL)appear
{
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         if (appear == YES)
                         {
                             infoView.alpha = 1.0f;
                             if (WIN_SIZE.height ==568)
                             {
                                 cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);//WIN_SIZE.height/2-33)
                             }
                         }
                         else
                         {
                             infoView.alpha = 0.0f;
                             if (WIN_SIZE.height ==568)
                             {
                                 //cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2-33);//WIN_SIZE.height/2-33)
                             }
                         }
                     }
                     completion:^(BOOL finished) {
                         //なにかあれば書く
                     }];
}



//#pragma mark - タブボタンどれか押した
//- (void)anyButtonPushed:(id)sender
//{
//    id vc = [[Util sharedManager] changeVC:(int)[sender tag] vcTag:(int)self.view.tag];
//    if (vc !=nil)
//    {//現在の画面とタブが違う場合のみ遷移
//        self.navigationController.navigationBarHidden = YES;
//        if ([sender tag] == TAB_TOP)
//        {//トップ画面のみポップで遷移
//            [self.navigationController popToRootViewControllerAnimated:NO];
//        }
//        else
//        {//それ以外はプッシュで遷移
//            [self.navigationController pushViewController:vc animated:NO];
//        }
//    }
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
