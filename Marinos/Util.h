//
//  Util.h
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/29.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import "Define.h"

@interface Util : NSObject
<CLLocationManagerDelegate>
{
    
}

@property(nonatomic, strong) NSString *transUrl;
@property(nonatomic, assign) BOOL isLaunching;
@property(nonatomic, strong) NSString *heroku;
@property(nonatomic, assign) BOOL isLocationEnabled;
@property(nonatomic, assign) BOOL isMapVCAppeared;

@property(nonatomic,retain)  AVAudioPlayer *buttonSE;
@property(nonatomic,retain)  AVAudioPlayer *cardButtonSE;
@property(nonatomic,retain)  AVAudioPlayer *cardButtonSE2;
@property(nonatomic,retain)  AVAudioPlayer *cardAppearSE;
@property(nonatomic,retain)  AVAudioPlayer *cardBaseSE;
@property(nonatomic,retain)  AVAudioPlayer *cardZoomUpSE_Rare;
@property(nonatomic,retain)  AVAudioPlayer *getCardLast;
@property(nonatomic,retain)  AVAudioPlayer *albumSE;
@property(nonatomic,retain)  AVAudioPlayer *kiiinSE;
@property(nonatomic,retain)  AVAudioPlayer *chuiuSE;
@property(nonatomic,retain)  AVAudioPlayer *pyoroSE;
@property(nonatomic,retain)  AVAudioPlayer *pironSE;

@property(nonatomic, assign) BOOL isMAP_TAB_INITED;
@property(nonatomic, assign) BOOL isCARD_TAB_INITED;
@property(nonatomic, assign) BOOL isALBUM_TAB_INITED;


+ (Util*) sharedManager;
- (void) sendUiidToServer:(NSString*)uiid;
- (NSString*) createUIID;
- (int) getPhotosSum;
- (NSMutableArray*) getAlbumCells;
- (BOOL) saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail;
- (int) getCheckinSum;
- (void) checkinCheck:(double)latitude with:(double)longtitude;
- (BOOL) showAllRows;
- (NSMutableArray*) getPhotos;
- (NSMutableArray*) getCheckIns;
- (UIImage*) getPhotoFromIdentification:(NSString*)identification;
- (NSMutableArray*) showTables;

- (id)changeVC:(int)buttonTag vcTag:(int)vcTag;
-(void)prepareToPlaySounds;
-(void)playSound:(int)soundID;

-(float)getSatusbarHeight;

-(void)stopSound:(int)soundID;
-(void)allSoundStop;
-(NSString*)returnSchoolName;
@end

@interface UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end
